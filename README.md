[Click here to lend your support and make a donation at pledgie.com !](https://pledgie.com/campaigns/33786)

Page Menu

[TOC]

# Boilerplate for a Character & Combat System

Quickly implement your character system with abilities, items... and be ready for combat in no time!

B4 Character System removes the burden of creating all the "standard" scripts for your characters.
Instead you can concentrate on the specific features you want for your characters.
Stats, Abilities, Weapons, Armors, Consumables and much more are supported out of the box.

* Works for both RPG and FPS type games.
* Built on ScriptableObjects (templates) to allow easy editing even during play mode, and better version control support!
* Multiplayer-Ready (requires you to implement the necessary features according to your infrastructure).
* Extensive customization.
* Zero-To-Minimal coding requirements if you don't require more specific features.
* Fully documented code.
* Extensive Wiki to help you start out.
* Issue tracker to quickly identify and crush the bugs!

See it in action: [https://www.youtube.com/watch?v=ncSvr6q_nxU](https://www.youtube.com/watch?v=ncSvr6q_nxU)

# Wiki Menu

* [How To](https://bitbucket.org/FabienB4/b4charactersystem/wiki/How%20To)
* [Tips & Tricks](https://bitbucket.org/FabienB4/b4charactersystem/wiki/Tips%20&%20Tricks)
* [Roadmap](https://bitbucket.org/FabienB4/b4charactersystem/wiki/Roadmap)

# Built-In Features

* 9 Stats
* 5 Armor parts (Head, Torso, Hands, Legs, Feet)
* 2 Weapons (primary, secondary)
* 4 Ability behaviours (Self, Directional, Positional, Projectile)
* 2 Ability types (Active, Passive)
* 41 Conditions for Abilities, Effects, Equippables and Consumables (using almost every aspects of B4 Character System)
* 28 Effects (Damage, Heal, Teleport, Summon, Buff, Cleanse, Resist, Resurrect and more)
* 4 Consumable types (Health regen, Energy regen, Ability, Effect)
* Unlimited Character templates possibilities (Player/NPC/Summon)
* Unlimited Item templates possibilities (Weapon/Armor Part/Consumable)
* Unlimited Ability templates possibilities
* Unlimited Effect templates possibilities
* Player/NPC/Summon support (with basic behaviour for NPC/Summon)
* Character movements (based on Ethan), casting, and status.
* Attackable, respawnable NPCs
* Ability casting (with animation support)
* Ability positioning (Guild Wars 2 like)
* Item Container support (Equipment/Inventory/Storage, just link them to your GUI and make your changes)
* Storable, destroyable, stackable or NPC-restricted items
* Can attach stats and abilities to Equippables
* Set, add, subtract, divide, multiply Stat Modifiers (with order support)
* GameManager & SpawnManager to handle automatic on-start spawns and respawns
* Extensive configuration (Characters, Abilities, Items and more)
* Notification system (just link it to your GUI)

# Roadmap

*  i18n of the Notification System
* Extensions
* And [more](https://bitbucket.org/FabienB4/b4charactersystem/wiki/Roadmap)

# Video/Screenshots Credits (Not included in the package)

* Magic Spells & Particles: [Link](https://www.assetstore.unity3d.com/en/#!/content/14330)
* Magic Pack: [Link](https://www.assetstore.unity3d.com/en/#!/content/36269)

# Help

* [Wiki](https://bitbucket.org/FabienB4/b4charactersystem/wiki/Home)
* [Issue Tracker](https://bitbucket.org/FabienB4/b4charactersystem/issues)