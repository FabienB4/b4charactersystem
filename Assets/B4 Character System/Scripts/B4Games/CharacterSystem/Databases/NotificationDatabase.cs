using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Notifications.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public class NotificationDatabase : ScriptableObjectDatabase<NotificationTemplate>
    {
        public override string resourcesPath { get { return Config.Database().notificationDbPath; } }

        private static NotificationDatabase m_Instance;
        public static NotificationDatabase instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = CreateInstance<NotificationDatabase>();

                    Reload();
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Unable to create NotificationDatabase");
#endif
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Reload database items.
        /// </summary>
        public static void Reload()
        {
            NotificationDatabase.instance.m_Items = new List<NotificationTemplate>(
                Resources.LoadAll<NotificationTemplate>(NotificationDatabase.instance.resourcesPath)
            );
        }

        /// <summary>
        /// Find a NotificationTemplate using its ID.
        /// </summary>
        /// <param name="id">The ID to find.</param>
        /// <returns>If found, the NotificationTemplate, else null.</returns>
        public static NotificationTemplate FindById(string id)
        {
            return NotificationDatabase.instance.Find((nt) => nt.id == id);
        }
    }
}