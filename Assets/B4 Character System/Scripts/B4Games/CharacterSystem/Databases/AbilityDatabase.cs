using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public class AbilityDatabase : ScriptableObjectDatabase<AbilityTemplate>
    {
        public override string resourcesPath { get { return Config.Database().abilityDbPath; } }

        private static AbilityDatabase m_Instance;
        public static AbilityDatabase instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = CreateInstance<AbilityDatabase>();

                    Reload();
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Unable to create AbilityDatabase");
#endif
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Reload database items.
        /// </summary>
        public static void Reload()
        {
            AbilityDatabase.instance.m_Items = new List<AbilityTemplate>(
                Resources.LoadAll<AbilityTemplate>(AbilityDatabase.instance.resourcesPath)
            );
        }
    }
}