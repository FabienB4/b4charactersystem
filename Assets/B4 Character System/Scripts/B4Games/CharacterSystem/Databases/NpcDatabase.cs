using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public class NpcDatabase : ScriptableObjectDatabase<NpcTemplate>
    {
        public override string resourcesPath { get { return Config.Database().npcDbPath; } }

        private static NpcDatabase m_Instance;
        public static NpcDatabase instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = CreateInstance<NpcDatabase>();

                    Reload();
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Unable to create NpcDatabase");
#endif
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Reload database items.
        /// </summary>
        public static void Reload()
        {
            NpcDatabase.instance.m_Items = new List<NpcTemplate>(
                Resources.LoadAll<NpcTemplate>(NpcDatabase.instance.resourcesPath)
            );
        }
    }
}