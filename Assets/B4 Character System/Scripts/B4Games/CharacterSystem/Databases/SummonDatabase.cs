using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public class SummonDatabase : ScriptableObjectDatabase<NpcTemplate>
    {
        public override string resourcesPath { get { return Config.Database().summonDbPath; } }

        private static SummonDatabase m_Instance;
        public static SummonDatabase instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = CreateInstance<SummonDatabase>();

                    Reload();
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Unable to create SummonDatabase");
#endif
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Reload database items.
        /// </summary>
        public static void Reload()
        {
            SummonDatabase.instance.m_Items = new List<NpcTemplate>(
                Resources.LoadAll<NpcTemplate>(SummonDatabase.instance.resourcesPath)
            );
        }
    }
}