using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public abstract class ScriptableObjectDatabase<T> : ScriptableObject where T : ScriptableObject
    {
        public abstract string resourcesPath { get; }

        protected List<T> m_Items = new List<T>();
        public ReadOnlyCollection<T> items { get { return m_Items.AsReadOnly(); } }

#region LIST_SHORTHANDS
        public bool Contains(T item)
        {
            return m_Items.Contains(item);
        }

        public bool Exists(Predicate<T> match)
        {
            return m_Items.Exists(match);
        }

        public T Find(Predicate<T> match)
        {
            return m_Items.Find(match);
        }

        public List<T> FindAll(Predicate<T> match)
        {
            return m_Items.FindAll(match);
        }
        public int FindIndex(int startIndex, int count, Predicate<T> match)
        {
            return m_Items.FindIndex(startIndex, count, match);
        }

        public int FindIndex(int startIndex, Predicate<T> match)
        {
            return m_Items.FindIndex(startIndex, match);
        }

        public int FindIndex(Predicate<T> match)
        {
            return m_Items.FindIndex(match);
        }

        public T FindLast(Predicate<T> match)
        {
            return m_Items.FindLast(match);
        }

        public int FindLastIndex(int startIndex, int count, Predicate<T> match)
        {
            return m_Items.FindLastIndex(startIndex, count, match);
        }

        public int FindLastIndex(int startIndex, Predicate<T> match)
        {
            return m_Items.FindLastIndex(startIndex, match);
        }

        public int FindLastIndex(Predicate<T> match)
        {
            return m_Items.FindLastIndex(match);
        }

        public void ForEach(Action<T> action)
        {
            m_Items.ForEach(action);
        }

        public List<T> GetRange(int index, int count)
        {
            return m_Items.GetRange(index, count);
        }

        public int IndexOf(T item)
        {
            return m_Items.IndexOf(item);
        }

        public int IndexOf(T item, int index)
        {
            return m_Items.IndexOf(item, index);
        }

        public int IndexOf(T item, int index, int count)
        {
            return m_Items.IndexOf(item, index, count);
        }

        public int LastIndexOf(T item)
        {
            return m_Items.LastIndexOf(item);
        }

        public int LastIndexOf(T item, int index)
        {
            return m_Items.LastIndexOf(item, index);
        }

        public int LastIndexOf(T item, int index, int count)
        {
            return m_Items.LastIndexOf(item, index, count);
        }

        public bool TrueForAll(Predicate<T> match)
        {
            return m_Items.TrueForAll(match);
        }
#endregion
    }
}