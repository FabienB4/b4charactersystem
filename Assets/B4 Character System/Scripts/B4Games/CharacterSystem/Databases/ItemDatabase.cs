using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public class ItemDatabase : ScriptableObjectDatabase<ItemTemplate>
    {
        public override string resourcesPath { get { return Config.Database().itemDbPath; } }

        private static ItemDatabase m_Instance;
        public static ItemDatabase instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = CreateInstance<ItemDatabase>();

                    Reload();
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Unable to create ItemDatabase");
#endif
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Reload database items.
        /// </summary>
        public static void Reload()
        {
            ItemDatabase.instance.m_Items = new List<ItemTemplate>(
                Resources.LoadAll<ItemTemplate>(ItemDatabase.instance.resourcesPath)
            );
        }
    }
}