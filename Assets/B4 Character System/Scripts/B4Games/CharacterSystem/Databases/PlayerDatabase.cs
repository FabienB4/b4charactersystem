using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Databases
{
    public class PlayerDatabase : ScriptableObjectDatabase<PlayerTemplate>
    {
        public override string resourcesPath { get { return Config.Database().playerDbPath; } }

        private static PlayerDatabase m_Instance;
        public static PlayerDatabase instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = CreateInstance<PlayerDatabase>();

                    Reload();
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Unable to create PlayerDatabase");
#endif
                }

                return m_Instance;
            }
        }

        /// <summary>
        /// Reload database items.
        /// </summary>
        public static void Reload()
        {
            PlayerDatabase.instance.m_Items = new List<PlayerTemplate>(
                Resources.LoadAll<PlayerTemplate>(PlayerDatabase.instance.resourcesPath)
            );
        }
    }
}