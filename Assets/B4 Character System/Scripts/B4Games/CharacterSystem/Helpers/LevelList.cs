using System.Collections.Generic;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Effects.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Helpers
{
    /// <summary>
    /// Util class for level list. Retrieve specific levels in a level list.
    /// </summary>
    public static class LevelList
    {
        public static int GetIntLevelValue(List<int> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(int) : list[count - 1]);
        }

        public static float GetFloatLevelValue(List<float> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(float) : list[count - 1]);
        }

        public static string GetStringLevelValue(List<string> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(string) : list[count - 1]);
        }

        public static bool GetBoolLevelValue(List<bool> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(bool) : list[count - 1]);
        }

        public static Vector3 GetVector3LevelValue(List<Vector3> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(Vector3) : list[count - 1]);
        }

        public static EffectTemplate.BuffStat GetBuffStatLevelValue(List<EffectTemplate.BuffStat> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(EffectTemplate.BuffStat) : list[count - 1]);
        }

        public static EffectType GetEffectTypeLevelValue(List<EffectType> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(EffectType) : list[count - 1]);
        }

        public static SummonTemplateLevel GetSummonTemplateLevelLevelValue(List<SummonTemplateLevel> list, int level = 1)
        {
            int count = list.Count;

            return (level > 0 && level <= count) ? list[level - 1] : (count - 1 < 0 ? default(SummonTemplateLevel) : list[count - 1]);
        }
    }
}