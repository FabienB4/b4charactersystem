namespace B4Games.CharacterSystem.Enums
{
    public enum ZoneType
    {
        /// <summary>
        /// Characters cannot attack
        /// </summary>
        Pacific,
        /// <summary>
        /// Characters can only attack hostile Npcs
        /// </summary>
        Neutral, 
        /// <summary>
        /// Characters can attack other Characters
        /// </summary>
        War
    }
}