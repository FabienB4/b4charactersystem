namespace B4Games.CharacterSystem.Enums
{
    public enum ItemLocation
    {
        None,
        Inventory,
        Equipment,
        Storage
    }
}