namespace B4Games.CharacterSystem.Enums
{
    public enum EffectType
    {
        // Damagers
        Damage,
        DrainHealth,
        DrainEnergy,

        // Restorers
        RestoreHealth,
        RestoreEnergy,

        // Stat Changers
        Buff,
        Debuff,

        // Cleansers
        CleanseAll,
        CleanseDebuffs,
        CleanseDisablers,
        CleanseEffectType,

        // Disablers
        Aggression,
        Fear,
        Paralyze,
        Sleep,
        Snare,
        Stun,

        // Resists
        ResistAggression,
        ResistFear,
        ResistParalyze,
        ResistSleep,
        ResistSnare,
        ResistStun,

        // Others
        Resurrection,
        TeleportPosition,
        TeleportDirection,
        Summon,
        SummonCharacter,
    }
}