namespace B4Games.CharacterSystem.Enums
{
    public enum CharacterStatus
    {
        Idle,
        Dead,
        Disabled,
    }
}