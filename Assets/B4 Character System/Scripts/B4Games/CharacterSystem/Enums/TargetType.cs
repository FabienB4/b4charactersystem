namespace B4Games.CharacterSystem.Enums
{
    public enum TargetType
    {
        None,
        Self,
        Any,
        NotSelf,
    }
}