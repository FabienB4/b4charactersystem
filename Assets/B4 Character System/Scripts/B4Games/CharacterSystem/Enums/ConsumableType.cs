namespace B4Games.CharacterSystem.Enums
{
    public enum ConsumableType
    {
        RegenerateHealth,
        RegenerateEnergy,
        AbilityTemplate,
        EffectTemplate,
    }
}