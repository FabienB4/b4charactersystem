namespace B4Games.CharacterSystem.Enums
{
    public enum Stat
    {
        // Pools
        MaxHealth,
        MaxEnergy,
        HealthRegeneration,
        EnergyRegeneration,

        // Combat
        Attack,
        Defense,
        Speed,
        HealPower,
        RegenerationPower,
    }
}