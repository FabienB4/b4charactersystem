namespace B4Games.CharacterSystem.Enums
{
    public enum AbilityType
    {
        Active,
        Passive
    }
}