namespace B4Games.CharacterSystem.Enums
{
    public enum StatModifierType
    {
        Set,
        Add,
        Subtract,
        Multiply,
        Divide,
    }
}