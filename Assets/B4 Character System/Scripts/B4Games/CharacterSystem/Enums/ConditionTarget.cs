namespace B4Games.CharacterSystem.Enums
{
    public enum ConditionTarget
    {
        Actor,
        Target
    }
}