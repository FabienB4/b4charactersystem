namespace B4Games.CharacterSystem.Enums
{
    public enum ArmorPartSlot
    {
        Head,
        Torso,
        Hands,
        Legs,
        Feet
    }
}