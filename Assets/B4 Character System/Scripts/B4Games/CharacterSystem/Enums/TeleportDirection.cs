namespace B4Games.CharacterSystem.Enums
{
    public enum TeleportDirection
    {
        Forward,
        Backward
    }
}