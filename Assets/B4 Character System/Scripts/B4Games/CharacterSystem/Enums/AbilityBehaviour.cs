namespace B4Games.CharacterSystem.Enums
{
    public enum AbilityBehaviour
    {
        Self,
        Projectile,
        Directional,
        Positional
    }
}