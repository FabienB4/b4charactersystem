using UnityEngine;

namespace B4Games.CharacterSystem.Enums
{
    public enum TeleportForceMode
    {
        /// <summary>
        /// Add an instant force impulse to the rigidbody, using its mass.
        /// </summary>
        Impulse = ForceMode.Impulse,
        /// <summary>
        /// Add an instant velocity change to the rigidbody, ignoring its mass.
        /// </summary>
        VelocityChange = ForceMode.VelocityChange
    }
}