namespace B4Games.CharacterSystem.Enums
{
    public enum WeaponSlot
    {
        Primary,
        Secondary
    }
}