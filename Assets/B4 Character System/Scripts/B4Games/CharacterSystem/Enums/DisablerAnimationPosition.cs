namespace B4Games.CharacterSystem.Enums
{
    public enum DisablerAnimationPosition
    {
        Feet,
        Middle,
        Head
    }
}