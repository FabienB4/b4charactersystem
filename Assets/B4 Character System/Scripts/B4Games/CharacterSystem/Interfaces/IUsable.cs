namespace B4Games.CharacterSystem.Interfaces
{
    public interface IUsable
    {
        bool IsUsable();
        void Use();
        void CancelUse();
    }
}