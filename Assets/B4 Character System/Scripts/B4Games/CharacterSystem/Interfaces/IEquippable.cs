using B4Games.CharacterSystem.Models.Actors;

namespace B4Games.CharacterSystem.Interfaces
{
    public interface IEquippable
    {
        bool IsEquippable();
        bool Equip(Character actor);
        bool UnEquip(Character actor);
    }
}