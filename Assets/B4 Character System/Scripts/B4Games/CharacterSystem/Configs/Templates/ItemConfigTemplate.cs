using UnityEngine;

namespace B4Games.CharacterSystem.Configs.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Configs/Item Config Template")]
    public class ItemConfigTemplate : ConfigTemplate
    {
        [Header("Stacks")]
        public long maxStackQuantity = long.MaxValue;
    }
}