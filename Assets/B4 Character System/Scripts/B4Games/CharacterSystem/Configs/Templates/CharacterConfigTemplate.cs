using UnityEngine;

namespace B4Games.CharacterSystem.Configs.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Configs/Character Config Template")]
    public class CharacterConfigTemplate : ConfigTemplate
    {
        [Header("Stats")]
        [Tooltip("Multiplier used to display stats to players. Allow to use smaller or bigger numbers without modifying the code.")]
        public int statDisplayMultiplier = 100;
        [Tooltip("The number of times per second the health regeneration is activated while in effect.")]
        [Range(0.1f, 10.0f)]
        public float healthRegeneratonTicksPerSecond = 1.0f;
        [Tooltip("The number of times per second the energy regeneration is activated while in effect.")]
        [Range(0.1f, 10.0f)]
        public float energyRegeneratonTicksPerSecond = 1.0f;
        [Range(0, 100)]
        public int healthPercentOnRevive = 0;
        [Range(0, 100)]
        public int energyPercentOnRevive = 0;

        [Header("Stat Caps (0 = no cap)")]
        public int maxHealthCap = 0;
        public int maxEnergyCap = 0;
        public int healthRegenerationCap = 0;
        public int energyRegenerationCap = 0;
        public int attackCap = 0;
        public int defenseCap = 0;
        public int speedCap = 0;
        public int healPowerCap = 0;
        public int regenerationPowerCap = 0;

        [Header("Movements")]
        public float walkingSpeedMultiplier = 0.5f;
        public float movingTurnSpeed = 1000.0f;
        public float stationaryTurnSpeed = 500.0f;
        public float jumpPower = 6.0f;
        [Range(1.0f, 4.0f)]
        public float jumpGravityMultiplier = 2.0f;
        public float groundCheckDistance = 0.2f;

        [Header("Combat")]
        public float enemyStatusDuration = 120.0f;
        public float friendlyStatusDuration = 120.0f;

        [Header("Summons")]
        [Tooltip("If true, the first summon will be replaced by the new one when the max summon count is reached.")]
        public bool replaceSummonOnMaxCount = false;
        public int maxSummonDistanceFromOwner = 15;
    }
}