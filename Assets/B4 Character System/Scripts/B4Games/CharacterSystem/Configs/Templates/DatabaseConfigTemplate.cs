using UnityEngine;

namespace B4Games.CharacterSystem.Configs.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Configs/Database Config Template")]
    public class DatabaseConfigTemplate : ConfigTemplate
    {
        [Header("Paths")]
        public string abilityDbPath = "Templates/Abilities/";
        public string itemDbPath = "Templates/Items/";
        public string notificationDbPath = "Templates/Notifications/";
        public string npcDbPath = "Templates/Characters/Npcs/";
        public string playerDbPath = "Templates/Characters/Players/";
        public string summonDbPath = "Templates/Characters/Summons/";
    }
}