using UnityEngine;

namespace B4Games.CharacterSystem.Configs.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Configs/Player Config Template")]
    public class PlayerConfigTemplate : ConfigTemplate
    {
        [Header("Abilities")]
        public int abilityBarLength = 10;

        [Header("Respawn")]
        public float respawnDelay = 10.0f;
    }
}