using B4Games.CharacterSystem.Enums;
using UnityEngine;

namespace B4Games.CharacterSystem.Configs.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Configs/Ability Config Template")]
    public class AbilityConfigTemplate : ConfigTemplate
    {
        [Header("Positioning Indicators")]
        public GameObject directionalIndicator;
        public Color directionalIndicatorColor = Color.blue;
        public GameObject positionalIndicator;
        public Color positionalIndicatorInRangeColor = Color.green;
        public Color positionalIndicatorOutOfRangeColor = Color.red;
        public string ignoreIndicatorCollisionLayerName = "IgnoreAbilityIndicatorCollision";

        [Header("Physics")]
        public string ignoreCollisionLayerName = "IgnoreAbilityCollision";

        [Header("Disablers")]
        public GameObject aggressionAnimation;
        public DisablerAnimationPosition aggressionAnimationPosition;
        public GameObject fearAnimation;
        public DisablerAnimationPosition fearAnimationPosition;
        public GameObject paralyzeAnimation;
        public DisablerAnimationPosition paralyzeAnimationPosition;
        public GameObject sleepAnimation;
        public DisablerAnimationPosition sleepAnimationPosition;
        public GameObject snareAnimation;
        public DisablerAnimationPosition snareAnimationPosition;
        public GameObject stunAnimation;
        public DisablerAnimationPosition stunAnimationPosition;
    }
}