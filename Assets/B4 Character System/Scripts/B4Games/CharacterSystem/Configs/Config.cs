using B4Games.CharacterSystem.Configs.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Configs
{
    public class Config
    {
        private static Config m_Instance;
        public static Config instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = Config.Create(Resources.LoadAll<ConfigTemplate>("Templates/Configs"));
                }

                if (m_Instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Invalid Config.");
#endif
                }

                return m_Instance;
            }
        }

        private DatabaseConfigTemplate m_DatabaseConfigTemplate;
        private AbilityConfigTemplate m_AbilityConfigTemplate;
        private CharacterConfigTemplate m_CharacterConfigTemplate;
        private PlayerConfigTemplate m_PlayerConfigTemplate;
        private ItemConfigTemplate m_ItemConfigTemplate;

        protected Config(ConfigTemplate[] templates)
        {
            foreach (ConfigTemplate template in templates)
            {
                switch (template.GetType().Name)
                {
                    case "DatabaseConfigTemplate":
                        m_DatabaseConfigTemplate = (DatabaseConfigTemplate)template;
                        break;
                    case "AbilityConfigTemplate":
                        m_AbilityConfigTemplate = (AbilityConfigTemplate)template;
                        break;
                    case "CharacterConfigTemplate":
                        m_CharacterConfigTemplate = (CharacterConfigTemplate)template;
                        break;
                    case "PlayerConfigTemplate":
                        m_PlayerConfigTemplate = (PlayerConfigTemplate)template;
                        break;
                    case "ItemConfigTemplate":
                        m_ItemConfigTemplate = (ItemConfigTemplate)template;
                        break;
                }
            }
        }

        /// <summary>
        /// Factory for Config.
        /// </summary>
        /// <param name="templates">The config templates.</param>
        /// <returns>A new Config instance.</returns>
        public static Config Create(ConfigTemplate[] templates)
        {
            Config config = new Config(templates);
            bool error = false;

            if (config.m_DatabaseConfigTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Missing DatabaseConfigTemplate.");
#endif

                error |= true;
            }

            if (config.m_AbilityConfigTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Missing AbilityConfigTemplate.");
#endif

                error |= true;
            }

            if (config.m_CharacterConfigTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Missing CharacterConfigTemplate.");
#endif

                error |= true;
            }

            if (config.m_PlayerConfigTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Missing PlayerConfigTemplate.");
#endif

                error |= true;
            }

            if (config.m_ItemConfigTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Missing ItemConfigTemplate.");
#endif

                error |= true;
            }

            if (error)
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
            }

            return config;
        }

        /// <summary>
        /// Database config.
        /// </summary>
        /// <returns>The database config.</returns>
        public static DatabaseConfigTemplate Database()
        {
            return instance.m_DatabaseConfigTemplate;
        }

        /// <summary>
        /// Ability config.
        /// </summary>
        /// <returns>The ability config.</returns>
        public static AbilityConfigTemplate Ability()
        {
            return instance.m_AbilityConfigTemplate;
        }

        /// <summary>
        /// Character config.
        /// </summary>
        /// <returns>The character config.</returns>
        public static CharacterConfigTemplate Character()
        {
            return instance.m_CharacterConfigTemplate;
        }

        /// <summary>
        /// Player config.
        /// </summary>
        /// <returns>The player config.</returns>
        public static PlayerConfigTemplate Player()
        {
            return instance.m_PlayerConfigTemplate;
        }

        /// <summary>
        /// Item config.
        /// </summary>
        /// <returns>The item config.</returns>
        public static ItemConfigTemplate Item()
        {
            return instance.m_ItemConfigTemplate;
        }
    }
}