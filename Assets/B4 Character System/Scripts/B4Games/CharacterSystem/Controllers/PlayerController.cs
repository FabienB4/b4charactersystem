using System.Collections.Generic;
using UnityEngine;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using System.Collections.ObjectModel;

namespace B4Games.CharacterSystem.Controllers
{
    [RequireComponent(typeof(Player))]
    public class PlayerController : CharController
    {
        public new Player character { get { return (Player)m_Character; } }

        // A reference to the main camera in the scenes transform
        private Transform m_Cam;
        private Vector3 m_CamForward;

        private List<Ability> m_AbilityBar;
        public ReadOnlyCollection<Ability> abilityBar { get { return m_AbilityBar.AsReadOnly(); } }

        private GameObject m_AbilityIndicator;
        private Renderer[] m_AbilityIndicatorRenderers;
        private int m_RaycastLayerMask;
        private bool m_IgnoreMovementInputs = false;

        protected override void Awake()
        {
            base.Awake();

            if (!(m_Character is Player))
            {
#if UNITY_EDITOR
                Debug.LogError("Created PlayerController for non Player character. Controller deactivated", this);
#endif

                gameObject.SetActive(false);
            }

            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
#endif
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // bit shift and invert to get layer mask
            m_RaycastLayerMask = ~(1 << LayerMask.NameToLayer(Config.Ability().ignoreIndicatorCollisionLayerName));

            // initialize ability bar
            m_AbilityBar = new List<Ability>(Config.Player().abilityBarLength);

            for (int i = 0; i < Config.Player().abilityBarLength; i++)
            {
                m_AbilityBar.Add(null);
            }
        }

        protected override void Update()
        {
            base.Update();

            if (!m_IgnoreMovementInputs)
            {
                if (!m_Jump)
                {
                    m_Jump = Input.GetButtonDown("Jump");
                }

                if (Input.GetButtonDown("Crouch"))
                {
                    m_Crouch = !m_Crouch;
                }
            }

            // Add a template for ability keys and link to Player
            if (Input.GetButtonDown("Ability One"))
            {
                ChangeActiveAbility(0);
            }
            else if (Input.GetButtonDown("Ability Two"))
            {
                ChangeActiveAbility(1);
            }
            else if (Input.GetButtonDown("Ability Three"))
            {
                ChangeActiveAbility(2);
            }
            else if (Input.GetButtonDown("Ability Four"))
            {
                ChangeActiveAbility(3);
            }
            else if (Input.GetButtonDown("Ability Five"))
            {
                ChangeActiveAbility(4);
            }
            else if (Input.GetButtonDown("Ability Six"))
            {
                ChangeActiveAbility(5);
            }
            else if (Input.GetButtonDown("Ability Seven"))
            {
                ChangeActiveAbility(6);
            }
            else if (Input.GetButtonDown("Ability Eight"))
            {
                ChangeActiveAbility(7);
            }
            else if (Input.GetButtonDown("Ability Nine"))
            {
                ChangeActiveAbility(8);
            }
            else if (Input.GetButtonDown("Ability Ten"))
            {
                ChangeActiveAbility(9);
            }

            if (m_ActiveAbility != null)
            {
                if (m_AbilityIndicator == null)
                {
                    // cancel the ability use on request or movement
                    if (Input.GetButtonDown("Cancel") || m_Move != Vector3.zero || m_Jump || m_Crouch)
                    {
                        CancelUseActiveAbility();
                    }
                }
                else
                {
                    // cancel the indicator on request
                    if (Input.GetButtonDown("Cancel"))
                    {
                        CancelUseActiveAbility();
                    }

                    // if indicator in valid range
                    if (CheckCastRange())
                    {
                        // left mouse click triggered
                        if (Input.GetButtonDown("Fire1"))
                        {
                            UseActiveAbility();
                        }
                    }
                }
            }
        }

        protected override void LateUpdate()
        {
            base.LateUpdate();

            if (m_AbilityIndicator != null)
            {
                UpdateIndicatorPosition();
            }
        }

        protected override void FixedUpdate()
        {
            if (!m_IgnoreMovementInputs)
            {
                // read inputs
                float h = Input.GetAxis("Horizontal");
                float v = Input.GetAxis("Vertical");

                // calculate move direction to pass to character
                if (m_Cam != null)
                {
                    // calculate camera relative direction to move:
                    m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                    m_Move = v * m_CamForward + h * m_Cam.right;
                }
                else
                {
                    // we use world-relative directions in the case of no main camera
                    m_Move = v * Vector3.forward + h * Vector3.right;
                }

#if !MOBILE_INPUT
                // walking
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    m_Walk = true;
                }
#endif
            }

            // called after inputs to process walking properly
            base.FixedUpdate();

            m_IgnoreMovementInputs = false;
        }

        /// <summary>
        /// Set the movements for Player.
        /// </summary>
        /// <param name="move">The movement.</param>
        /// <param name="walk">Should the Player walk?</param>
        /// <param name="crouch">Should the Player crouch?</param>
        /// <param name="jump">Should the Player jump?</param>
        /// <param name="jump">Is the Character being forced to move?</param>
        public override void SetMovement(Vector3 move, bool walk = false, bool crouch = false, bool jump = false, bool forced = false)
        {
            base.SetMovement(move, walk, crouch, jump, forced);

            m_IgnoreMovementInputs = true;
        }

        /// <summary>
        /// Add ability to ability bar, if possible.
        /// </summary>
        /// <param name="ability">The ability to add.</param>
        /// <param name="index">[Optional] The slot. If not specified, use empty slot, if any.</param>
        public virtual void TryAddToAbilityBar(Ability ability, int index = -1)
        {
            if (index >= 0 && index < m_AbilityBar.Count)
            {
                m_AbilityBar[index] = ability;
            }
            else
            {
                for (int i = 0; i < m_AbilityBar.Count; i++)
                {
                    if (m_AbilityBar[i] == null)
                    {
                        m_AbilityBar[i] = ability;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Remove ability from ability bar, if present.
        /// </summary>
        /// <param name="ability">The ability to remove.</param>
        public virtual void TryRemoveFromAbilityBar(Ability ability)
        {
            if (m_AbilityBar.Contains(ability))
            {
                m_AbilityBar[m_AbilityBar.IndexOf(ability)] = null;
            }
        }

        /// <summary>
        /// Check if slot is used.
        /// </summary>
        /// <param name="index">The index to check.</param>
        /// <returns>True if slod is used.</returns>
        public virtual bool HasAbilityInSlot(int index)
        {
            return index < m_AbilityBar.Count && m_AbilityBar[index] != null;
        }

        /// <summary>
        /// Change active ability to the one at the specified ability bar index.
        /// </summary>
        /// <param name="index">The index.</param>
        private void ChangeActiveAbility(int index)
        {
            if (index >= m_AbilityBar.Count)
            {
                return;
            }

            HideAbilityIndicator();
            SetActiveAbility(m_AbilityBar[index]);
        }

        /// <summary>
        /// Position the active ability.
        /// </summary>
        protected override void PositionActiveAbility()
        {
            ShowAbilityIndicator();
        }

        /// <summary>
        /// Use the active ability.
        /// </summary>
        protected override void UseActiveAbility()
        {
            base.UseActiveAbility();

            HideAbilityIndicator();
        }

        /// <summary>
        /// Cancel the use of the active ability.
        /// </summary>
        protected override void CancelUseActiveAbility()
        {
            base.CancelUseActiveAbility();

            if (m_AbilityIndicator != null)
            {
                HideAbilityIndicator();
            }
        }

        /// <summary>
        /// Show the ability indicator.
        /// </summary>
        private void ShowAbilityIndicator()
        {
            if (m_AbilityIndicator != null)
            {
                return;
            }

            if (m_ActiveAbility == null)
            {
#if UNITY_EDITOR
                Debug.LogError("No active ability", this);
#endif

                return;
            }

            m_AbilityIndicator = new GameObject("Ability Indicator");
            m_AbilityIndicator.transform.position = this.character.transform.position;
            m_AbilityIndicator.transform.rotation = this.character.transform.rotation;

            if (m_ActiveAbility.IsDirectional())
            {
                if (Config.Ability().directionalIndicator == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("AbilityConfig: DirectionalIndicator not assigned", this);
#endif

                    return;
                }

                GameObject prefab = Instantiate(Config.Ability().directionalIndicator);

                prefab.transform.SetParent(m_AbilityIndicator.transform, false);
            }
            else if (m_ActiveAbility.IsPositional())
            {
                if (Config.Ability().positionalIndicator == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("AbilityConfig: PositionalIndicator not assigned", this);
#endif

                    return;
                }

                GameObject prefab = Instantiate(Config.Ability().positionalIndicator);

                prefab.transform.SetParent(m_AbilityIndicator.transform, false);
            }
        }

        /// <summary>
        /// Hide the ability indicator.
        /// </summary>
        private void HideAbilityIndicator()
        {
            if (m_AbilityIndicator == null)
            {
                return;
            }

            Destroy(m_AbilityIndicator);
            m_AbilityIndicator = null;
            m_AbilityIndicatorRenderers = null;
        }

        /// <summary>
        /// Update the indicator position.
        /// </summary>
        private void UpdateIndicatorPosition()
        {
            if (m_AbilityIndicator == null || m_ActiveAbility == null)
            {
                return;
            }

            RaycastHit mousePositionHit;

            // raycast the current mouse position to get the ground position
            // if raycast didn't hit, the position remains unchanged
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out mousePositionHit, Mathf.Infinity, m_RaycastLayerMask))
            {
                m_ActiveAbilityPosition = mousePositionHit.point;
            }

            if (m_ActiveAbility.IsDirectional())
            {
                // indicator's position is always linked to Character's position
                m_AbilityIndicator.transform.position = this.character.transform.position;
                m_AbilityIndicator.transform.forward = this.character.transform.forward;

                // handle rotation towards mouse position
                m_AbilityIndicator.transform.LookAt(m_ActiveAbilityPosition);
            }
            else if (m_ActiveAbility.IsPositional())
            {
                // follow mouse position
                m_AbilityIndicator.transform.position = m_ActiveAbilityPosition;
            }
        }

        /// <summary>
        /// Check the range of the indicator against the valid range of the ability.
        /// </summary>
        /// <returns>True if in range.</returns>
        protected override bool CheckCastRange()
        {
            if (m_AbilityIndicator == null)
            {
                return false;
            }

            if (m_ActiveAbility.IsDirectional())
            {
                SetIndicatorColor(Config.Ability().directionalIndicatorColor);

                return true;
            }

            // non-positional ignore cast range, avoid useless color changes
            if (!m_ActiveAbility.IsPositional())
            {
                return true;
            }

            if (base.CheckCastRange())
            {
                SetIndicatorColor(Config.Ability().positionalIndicatorInRangeColor);

                return true;
            }
            else
            {
                SetIndicatorColor(Config.Ability().positionalIndicatorOutOfRangeColor);

                return false;
            }
        }

        /// <summary>
        /// Set the color of the indicator prefab.
        /// </summary>
        /// <param name="color">The color to set.</param>
        private void SetIndicatorColor(Color color)
        {
            if (m_AbilityIndicatorRenderers == null)
            {
                m_AbilityIndicatorRenderers = m_AbilityIndicator.GetComponentsInChildren<Renderer>();
            }

            for (int i = 0; i < m_AbilityIndicatorRenderers.Length; i++)
            {
                Renderer renderer = m_AbilityIndicatorRenderers[i];

                for (int j = 0; j < renderer.materials.Length; j++)
                {
                    Material material = renderer.materials[j];

                    material.color = color;
                }
            }
        }
    }
}