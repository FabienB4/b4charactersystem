using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Controllers
{
    [RequireComponent(typeof(Character))]
    public abstract class CharController : MonoBehaviour
    {
        protected Character m_Character;
        public Character character { get { return m_Character; } }

        protected float m_MoveHorizontal;
        public float moveH { get { return m_MoveHorizontal; } }

        protected float m_MoveVertical;
        public float moveV { get { return m_MoveVertical; } }

        protected Vector3 m_Move;
        public Vector3 move { get { return m_Move; } }

        protected bool m_Walk = false;
        public bool walk { get { return m_Walk; } }

        protected bool m_Crouch;
        public bool crouch { get { return m_Crouch; } }

        protected bool m_Jump;
        public bool jump { get { return m_Jump; } }

        protected bool m_Forced;
        public bool forced { get { return m_Forced; } }

        protected Ability m_ActiveAbility;
        public Ability activeAbility { get { return m_ActiveAbility; } }

        protected Vector3 m_ActiveAbilityPosition;
        public Vector3 activeAbilityPosition { get { return m_ActiveAbilityPosition; } }

        protected virtual void Awake()
        {
            m_Character = GetComponent<Character>();
        }

        protected virtual void Update()
        {

        }

        protected virtual void FixedUpdate()
        {
            if (m_Walk)
            {
                m_Move *= Config.Character().walkingSpeedMultiplier;
            }

            // pass all parameters to the character control script
            m_Character.Move(m_Move, m_Crouch, m_Jump, m_Forced);
            m_Walk = false;
            m_Jump = false;
        }

        protected virtual void LateUpdate()
        {

        }

        /// <summary>
        /// Set the movements for Character.
        /// </summary>
        /// <param name="move">The movement.</param>
        /// <param name="walk">Should the Character walk?</param>
        /// <param name="crouch">Should the Character crouch?</param>
        /// <param name="jump">Should the Character jump?</param>
        /// <param name="jump">Is the Character being forced to move?</param>
        public virtual void SetMovement(Vector3 move, bool walk = false, bool crouch = false, bool jump = false, bool forced = false)
        {
            m_Move = move;
            m_Walk = walk;
            m_Crouch = crouch;
            m_Jump = jump;
            m_Forced = forced;
        }

        /// <summary>
        /// Shorthand for SetMovement(move, false, false, false, true).
        /// </summary>
        /// <param name="move">The movement.</param>
        public virtual void SetForcedMovement(Vector3 move)
        {
            SetMovement(move, false, false, false, true);
        }

        /// <summary>
        /// Shorthand for SetMovement(Vector3.zero).
        /// </summary>
        public virtual void StopMovement()
        {
            SetMovement(Vector3.zero);
        }

        /// <summary>
        /// Set active ability.
        /// </summary>
        /// <param name="ability">The ability to set as active.</param>
        public virtual void SetActiveAbility(Ability ability)
        {
            if (ability == null)
            {
                return;
            }

            // can't use abilities when dead or disabled
            if (m_Character.isDead || m_Character.isDisabled)
            {
                return;
            }

            // cancel currently in use if any
            if (m_ActiveAbility != null && m_ActiveAbility.endCastDuration != 0)
            {
                CancelUseActiveAbility();
            }

            if (ability.endReuseDurationTime > 0)
            {
                m_Character.Notify(Notification.Create("ABILITY_IN_COOLDOWN"));
                return;
            }

            m_ActiveAbility = ability;

            OnSetActiveAbility();
        }

        /// <summary>
        /// Triggers when active ability is set.
        /// </summary>
        protected virtual void OnSetActiveAbility()
        {
            if (m_ActiveAbility == null)
            {
                return;
            }

            if (m_ActiveAbility.template.instantBehaviour)
            {
                if (m_ActiveAbility.IsPositional())
                {
                    m_ActiveAbilityPosition = m_Character.transform.position + (m_Character.transform.forward * m_ActiveAbility.castRange);
                }
                else
                {
                    m_ActiveAbilityPosition = m_Character.transform.position + m_Character.transform.forward;
                }

                UseActiveAbility();
            }
            else if (m_ActiveAbility.RequirePositioning())
            {
                PositionActiveAbility();
            }
            else
            {
                UseActiveAbility();
            }
        }

        /// <summary>
        /// Position the active ability.
        /// </summary>
        protected abstract void PositionActiveAbility();

        /// <summary>
        /// Use the active ability.
        /// </summary>
        protected virtual void UseActiveAbility()
        {
            if (!CheckActiveAbilityUseConditions())
            {
                return;
            }

            m_ActiveAbility.Use(m_ActiveAbilityPosition);
        }

        /// <summary>
        /// Cancel the use of the active ability.
        /// </summary>
        protected virtual void CancelUseActiveAbility()
        {
            if (m_ActiveAbility == null)
            {
                return;
            }

            m_ActiveAbility.CancelUse();

            // reset active ability
            m_ActiveAbility = null;
        }

        /// <summary>
        /// Check use conditions for an ability.
        /// </summary>
        /// <returns>True if the conditions are met.</returns>
        protected virtual bool CheckActiveAbilityUseConditions()
        {
            if (m_ActiveAbility == null)
            {
#if UNITY_EDITOR
                Debug.LogError("No active ability");
#endif

                return false;
            }

            if (!m_ActiveAbility.IsUsable())
            {
                m_Character.Notify(Notification.Create("NOT_USABLE"));
                return false;
            }

            if (!m_Character.CanAct())
            {
                m_Character.Notify(Notification.Create("CANNOT_USE_STATE"));
                return false;
            }

            if (m_ActiveAbility.castDuration > 0 && (m_Move != Vector3.zero || m_Jump || m_Crouch))
            {
                m_Character.Notify(Notification.Create("CANNOT_USE_MOVING_JUMPING_CROUCHING"));
                return false;
            }

            if (m_ActiveAbility.healthConsumption >= m_Character.health)
            {
                m_Character.Notify(Notification.Create("NOT_ENOUGH_HEALTH"));
                return false;
            }

            if (m_ActiveAbility.energyConsumption > m_Character.energy)
            {
                m_Character.Notify(Notification.Create("NOT_ENOUGH_ENERGY"));
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check the cast range for the active ability.
        /// </summary>
        /// <returns>True if the ability position is in range.</returns>
        protected virtual bool CheckCastRange()
        {
            if (m_ActiveAbility == null)
            {
                return false;
            }

            // non-positional ignore cast range, avoid vector calculations
            if (!m_ActiveAbility.IsPositional())
            {
                return true;
            }

            // check the distance between the caster and the mouse position against the ability's cast range
            return Vector3.Distance(this.character.transform.position, m_ActiveAbilityPosition) <= m_ActiveAbility.castRange;
        }
    }
}