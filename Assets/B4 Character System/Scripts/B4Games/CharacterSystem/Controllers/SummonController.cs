using UnityEngine;
using B4Games.CharacterSystem.Models.Actors;

namespace B4Games.CharacterSystem.Controllers
{
    public class SummonController : NpcController
    {
        public new Summon character { get { return (Summon)m_Character; } }

        protected override void Awake()
        {
            base.Awake();

            if (!(m_Character is Summon))
            {
#if UNITY_EDITOR
                Debug.LogError("Created SummonController for non Summon character. Controller deactivated", this);
#endif

                gameObject.SetActive(false);
            }
        }

        protected override void Update()
        {
            base.Update();
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            // forces the summon to only move when the owner is moving away from the summon
            character.transform.LookAt(character.owner.transform);

            character.FollowOwner();
        }
    }
}