using UnityEngine;
using B4Games.CharacterSystem.Models.Actors;

namespace B4Games.CharacterSystem.Controllers
{
    public class NpcController : CharController
    {
        public new Npc character { get { return (Npc)m_Character; } }

        private Character m_IntendedTarget;
        public Character intendedTarget { get { return m_IntendedTarget; } }

        protected override void Awake()
        {
            base.Awake();

            if (!(m_Character is Npc))
            {
#if UNITY_EDITOR
                Debug.LogError("Created NpcController for non Npc character. Controller deactivated", this);
#endif

                gameObject.SetActive(false);
            }
        }

        protected override void Update()
        {
            base.Update();
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
        }

        protected override void LateUpdate()
        {
            base.LateUpdate();
        }

        /// <summary>
        /// Set the intended target of the Npc.
        /// </summary>
        /// <param name="target">The intended target.</param>
        public virtual void SetIntendedTarget(Character target)
        {
            m_IntendedTarget = target;
        }

        /// <summary>
        /// Position the active ability and use it at the position.
        /// </summary>
        protected override void PositionActiveAbility()
        {
            if (m_ActiveAbility == null)
            {
#if UNITY_EDITOR
                Debug.LogError("No active ability", this);
#endif

                return;
            }

            if (m_IntendedTarget != null)
            {
                // this can easily be customized to say (1m in front of target) or (3m behind target)
                m_ActiveAbilityPosition = m_IntendedTarget.transform.position;
            }
            else
            {
                // use instant-behaviour like positioning
                m_ActiveAbilityPosition = m_Character.transform.position + (m_Character.transform.forward * m_ActiveAbility.castRange);
            }

            UseActiveAbility();
        }
    }
}