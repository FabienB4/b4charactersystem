using System.Collections.Generic;
using B4Games.CharacterSystem.Databases;
using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Managers
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager m_Instance;
        public static GameManager instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new GameObject("Game Manager").AddComponent<GameManager>();
                }

                return m_Instance;
            }
        }

        protected GameManager()
        {
        }

        void OnEnable()
        {
            m_Instance = this;
        }

        void OnDisable()
        {
            m_Instance = null;
        }

        void Start()
        {
            List<NpcTemplate> autoSpawnNpcs = NpcDatabase.instance.FindAll((npc) => npc.spawnOnStart);

            for (int i = 0; i < autoSpawnNpcs.Count; i++)
            {
                NpcTemplate template = autoSpawnNpcs[i];

                SpawnManager.AddSpawn(new SpawnManager.SpawnInfo(template, template.level, template.spawnPosition, Quaternion.identity), 0);
            }
        }
    }
}