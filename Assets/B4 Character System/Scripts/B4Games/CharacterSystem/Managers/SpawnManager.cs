using System.Collections;
using System.Collections.Generic;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Managers
{
    public class SpawnManager : MonoBehaviour
    {
        public struct SpawnInfo
        {
            public NpcTemplate template;
            public int level;
            public Vector3 position;
            public Quaternion rotation;

            public SpawnInfo(NpcTemplate template, int level, Vector3 position, Quaternion rotation)
            {
                if (template == null || template is SummonTemplate)
                {
#if UNITY_EDITOR
                    Debug.LogError("Invalid template");
#endif
                }

                this.template = template;
                this.level = level > 0 ? level : 1;
                this.position = position;
                this.rotation = rotation;
            }
        }

        private static SpawnManager m_Instance;
        public static SpawnManager instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new GameObject("Spawn Manager").AddComponent<SpawnManager>();
                }

                return m_Instance;
            }
        }

        private Dictionary<SpawnInfo, Coroutine> m_SpawnCoroutines = new Dictionary<SpawnInfo, Coroutine>();

        protected SpawnManager()
        {
        }

        /// <summary>
        /// Add a spawn.
        /// </summary>
        /// <param name="spawnInfo">The spawn info.</param>
        /// <param name="delay">[Optional] Delay before the spawn. Default: 0.</param>
        public static void AddSpawn(SpawnInfo spawnInfo, float delay = 0)
        {
            SpawnManager.instance.StartSpawnTimer(spawnInfo, delay);
        }

        /// <summary>
        /// Cancel a spawn.
        /// </summary>
        /// <param name="spawnInfo">The spawn info.</param>
        public static void CancelSpawn(SpawnInfo spawnInfo)
        {
            SpawnManager.instance.StopSpawnTimer(spawnInfo);
        }

        /// <summary>
        /// Spawn task.
        /// </summary>
        /// <param name="spawnInfo">The spawn info.</param>
        /// <param name="endTime">The time at which to spawn.</param>
        private IEnumerator SpawnTimer(SpawnInfo spawnInfo, float endTime)
        {
            while (Time.time < endTime)
            {
                yield return null;
            }

            OnSpawnTimerEnd(spawnInfo);
        }

        /// <summary>
        /// Start a spawn task.
        /// </summary>
        /// <param name="spawnInfo">The spawn info.</param>
        /// <param name="delay">The delay before the spawn.</param>
        private void StartSpawnTimer(SpawnInfo spawnInfo, float delay)
        {
            float endTime = Time.time + delay;

            m_SpawnCoroutines.Add(spawnInfo, StartCoroutine(SpawnTimer(spawnInfo, endTime)));
        }

        /// <summary>
        /// Stop a spawn task.
        /// </summary>
        /// <param name="spawnInfo">The spawn info.</param>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopSpawnTimer(SpawnInfo spawnInfo, bool end = true)
        {
            Coroutine coroutine;

            if (m_SpawnCoroutines.TryGetValue(spawnInfo, out coroutine))
            {
                StopCoroutine(coroutine);
                OnSpawnTimerEnd(spawnInfo, end);
            }
        }

        /// <summary>
        /// Triggered when the spawn task ends or is stopped.
        /// </summary>
        /// <param name="spawnInfo">The spawn info.</param>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnSpawnTimerEnd(SpawnInfo spawnInfo, bool end = true)
        {
            if (end)
            {
                // specified classes first
                if (spawnInfo.template is SummonTemplate)
                {
                    Summon.Create(spawnInfo.template, spawnInfo.level, spawnInfo.position, spawnInfo.rotation);
                }
                else
                {
                    Npc.Create(spawnInfo.template, spawnInfo.level, spawnInfo.position, spawnInfo.rotation);
                }
            }

            m_SpawnCoroutines.Remove(spawnInfo);
        }
    }
}