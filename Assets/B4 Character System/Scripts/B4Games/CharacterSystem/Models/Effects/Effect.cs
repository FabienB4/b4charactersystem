using System;
using System.Collections;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Helpers;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Conditions;
using B4Games.CharacterSystem.Models.Effects.Templates;
using B4Games.CharacterSystem.Models.Notifications;
using B4Games.CharacterSystem.Models.Stats;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Effects
{
    public class Effect
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        private EffectTemplate m_Template;
        public EffectTemplate template { get { return m_Template; } }

        private int m_Level;
        public int level { get { return m_Level; } }

        private float m_EndDurationTime;
        public float endDurationTime { get { return m_EndDurationTime; } }

        private Character m_Actor;
        public Character actor { get { return m_Actor; } }

        public float duration { get { return LevelList.GetFloatLevelValue(m_Template.duration, m_Level); } }
        public int tickCount { get { return LevelList.GetIntLevelValue(m_Template.tickCount, m_Level); } }
        public float power { get { return LevelList.GetFloatLevelValue(m_Template.power, m_Level); } }
        public EffectTemplate.BuffStat buffStat { get { return LevelList.GetBuffStatLevelValue(m_Template.buffStat, m_Level); } }
        public EffectType cleanseEffectType { get { return LevelList.GetEffectTypeLevelValue(m_Template.cleanseEffectType, m_Level); } }
        public int resurrectionHealth { get { return LevelList.GetIntLevelValue(m_Template.resurrectionHealth, m_Level); } }
        public int resurrectionEnergy { get { return LevelList.GetIntLevelValue(m_Template.resurrectionEnergy, m_Level); } }
        public Vector3 teleportPosition { get { return LevelList.GetVector3LevelValue(m_Template.teleportPosition, m_Level); } }
        public float teleportDirectionForce { get { return LevelList.GetFloatLevelValue(m_Template.teleportDirectionForce, m_Level); } }
        public SummonTemplateLevel summonTemplate { get { return LevelList.GetSummonTemplateLevelLevelValue(m_Template.summonTemplate, m_Level); } }

        private Character m_Affected;
        private Coroutine m_DurationTimerCoroutine = null;

        /// <summary>
        /// Constructor for Effect.
        /// </summary>
        /// <param name="template">The effect template.</param>
        /// <param name="level">The level of the effect.</param>
        protected Effect(EffectTemplate template, int level = 1)
        {
            m_Id = Guid.NewGuid();
            m_Template = template;
            m_Level = level >= 1 ? level : 1;
            m_EndDurationTime = 0;
        }

        /// <summary>
        /// Factory for Effect.
        /// </summary>
        /// <param name="template">The effect template to use.</param>
        /// <param name="level">The level of the effect.</param>
        /// <returns>A new Effect instance.</returns>
        public static Effect Create(EffectTemplate template, int level = 1)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid effect template.");
#endif

                return null;
            }

            return new Effect(template, level);
        }

        /// <summary>
        /// Factory for Effect using an EffectTemplateLevel.
        /// </summary>
        /// <param name="template">The effect level template.</param>
        /// <returns>A new Effect instance.</returns>
        public static Effect Create(EffectTemplateLevel template)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid effect level template.");
#endif

                return null;
            }

            return Effect.Create(template.template, template.level);
        }

        /// <summary>
        /// Apply effect to target.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <returns>True if effect applie successfully.</returns>
        public bool Apply(Character actor, Character target)
        {
            if (actor == null || target == null)
            {
                return false;
            }

            m_Actor = actor;
            m_Affected = target;

            if (IsHostile() && (!target.IsAttackable() || target.zoneType == ZoneType.Pacific))
            {
                actor.Notify(Notification.Create("NOT_ATTACKABLE", target.characterName));
                return false;
            }

            // can't affect dead targets unless it's a resurrection effect
            if (target.isDead && m_Template.type != EffectType.Resurrection)
            {
                actor.Notify(Notification.Create("DEAD"));
                return false;
            }

            if (!CheckConditions(m_Actor, m_Affected))
            {
                return false;
            }

            if (IsResistable() && Formulas.CalculateResistChance(m_Actor, m_Affected, this))
            {
                actor.Notify(Notification.Create("RESISTED_EFFECT", target.characterName));
                return false;
            }

            StartDurationTimer();

            return true;
        }

        /// <summary>
        /// Check the conditions attached to the item.
        /// </summary>
        /// <param name="actor">The Character acting.</param>
        /// <param name="target">The target Character.</param>
        /// <returns>True if all conditions passed.</returns>
        public bool CheckConditions(Character actor, Character target)
        {
            if (target == null)
            {
                return false;
            }

            // no condition
            if (!HasConditions())
            {
                return true;
            }

            for (int i = 0; i < m_Template.conditions.Count; i++)
            {
                Condition condition = Condition.Create(m_Template.conditions[i]);

                if (condition == null)
                {
                    continue;
                }

                if (!condition.Test(actor, target, this))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check if effect has conditions attached.
        /// </summary>
        /// <returns>True if conditions attached.</returns>
        public bool HasConditions()
        {
            return m_Template.HasConditions();
        }

        /// <summary>
        /// Get the resistance type for the specified effect type.
        /// </summary>
        /// <param name="type">The type to get the resistance for.</param>
        /// <returns>The resistance type, or null if no resistance associated.</returns>
        public EffectType? GetResistanceType(EffectType type)
        {
            return m_Template.GetResistanceType(type);
        }

        /// <summary>
        /// Check if effect is hostile.
        /// </summary>
        /// <returns>True if hostile.</returns>
        public bool IsHostile()
        {
            return m_Template.IsHostile();
        }

        /// <summary>
        /// Check if effect is damager.
        /// </summary>
        /// <returns>True if damager.</returns>
        public bool IsDamager()
        {
            return m_Template.IsDamager();
        }

        /// <summary>
        /// Check if effect is restorer.
        /// </summary>
        /// <returns>True if restorer.</returns>
        public bool IsRestorer()
        {
            return m_Template.IsRestorer();
        }

        /// <summary>
        /// Check if effect is stat changer.
        /// </summary>
        /// <returns>True if stat changer.</returns>
        public bool IsStatChanger()
        {
            return m_Template.IsStatChanger();
        }

        /// <summary>
        /// Check if effect is cleanser.
        /// </summary>
        /// <returns>True if cleanser.</returns>
        public bool IsCleanser()
        {
            return m_Template.IsCleanser();
        }

        /// <summary>
        /// Check if effect is disabler.
        /// </summary>
        /// <returns>True if disabler.</returns>
        public bool IsDisabler()
        {
            return m_Template.IsDisabler();
        }

        /// <summary>
        /// Check if effect is resist.
        /// </summary>
        /// <returns>True if resist</returns>
        public bool IsResist()
        {
            return m_Template.IsResist();
        }

        /// <summary>
        /// Check if effect can be resisted.
        /// </summary>
        /// <returns>True if can be resisted</returns>
        public bool IsResistable()
        {
            return m_Template.IsResistable();
        }

        /// <summary>
        /// Check if effect is lasting (stays for a given duration or indefinitely after being applied).
        /// Considered lasting:
        ///   - StatChanger
        ///   - Resist
        ///   - duration for level > 0
        /// </summary>
        /// <returns>True if lasting</returns>
        public bool IsLasting()
        {
            return m_Template.IsLasting(m_Level);
        }

        /// <summary>
        /// Duration timer task.
        /// </summary>
        private IEnumerator DurationTimer()
        {
            while (Time.time < m_EndDurationTime)
            {
                yield return null;
            }

            OnDurationTimerEnd();
        }

        /// <summary>
        /// Start the duration timer task.
        /// </summary>
        private void StartDurationTimer()
        {
            if (m_DurationTimerCoroutine != null)
            {
                return;
            }

            m_EndDurationTime = this.duration > 0 ? Time.time + this.duration : 0;

            if (m_EndDurationTime > 0)
            {
                m_DurationTimerCoroutine = m_Affected.StartCoroutine(DurationTimer());
            }
        }

        /// <summary>
        /// Stop the duration timer task.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopDurationTimer(bool end = true)
        {
            if (m_DurationTimerCoroutine == null)
            {
                return;
            }

            m_Affected.StopCoroutine(m_DurationTimerCoroutine);
            OnDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered when the duration timer task ends or is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnDurationTimerEnd(bool end = true)
        {
            m_DurationTimerCoroutine = null;
            m_EndDurationTime = 0;

            if (end)
            {
                m_Affected.RemoveEffect(this);
            }
        }
    }
}