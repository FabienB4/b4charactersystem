using System;

namespace B4Games.CharacterSystem.Models.Effects.Templates
{
    [Serializable]
    public class EffectTemplateLevel
    {
        public EffectTemplate template;
        public int level = 1;
    }
}