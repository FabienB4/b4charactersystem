using System;
using System.Collections.Generic;
using UnityEngine;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Helpers;

namespace B4Games.CharacterSystem.Models.Effects.Templates
{
    /// <summary>
    /// Effect template used by abilities.
    /// </summary>
    [CreateAssetMenu(menuName = "B4 Character System/Effect Template")]
    public class EffectTemplate : ScriptableObject
    {
        [Serializable]
        public struct BuffStat
        {
            public Stat stat;
            public StatModifierType modifierType;
            [Tooltip("Low orders are applied first, high orders are applied after.")]
            [Range(0, 100)]
            public int order;
        }

        [Header("Display")]
        public string effectName;
        public string description;
        public Sprite icon;

        [Header("Behaviour")]
        [Tooltip("The type of targets affected by the effect.")]
        public TargetType targetType;
        public EffectType type;

        [Space]

        [Tooltip("The power of the effect.\nBehaviour depends on the effect type.")]
        public List<float> power;

        [Space]

        [Tooltip("The duration of the effect in seconds.\nFor damage, heal, and related, if > 0, the effect happens over time.")]
        public List<float> duration;
        [Tooltip("Is the effect repeating over time? (Requires duration > 1 and tickCount > 1)")]
        public bool repeatOverTime;
        [Tooltip("The number of times the effect is activated while in effect.")]
        public List<int> tickCount;

        [Space]

        public List<ConditionTemplate> conditions;

        // Buff/Debuff Specific
        public List<BuffStat> buffStat;

        // Cleanse Specific
        public List<EffectType> cleanseEffectType;

        // Resurrection Specific
        [Range(0, 100)]
        public List<int> resurrectionHealth;
        [Range(0, 100)]
        public List<int> resurrectionEnergy;

        // Teleport Specific
        public List<Vector3> teleportPosition;
        [Tooltip("The force to apply to teleport the target.")]
        public TeleportDirection teleportDirection;
        public List<float> teleportDirectionForce;
        [Tooltip("Impulse: Add an instant force impulse to the rigidbody, using its mass.\nVelocityChange: Add an instant velocity change to the rigidbody, ignoring its mass.\nIn simple circumstances, when using VelocityChange, a 1 meter leap, requires 50 of force.")]
        public TeleportForceMode teleportDirectionForceMode;

        // Summon Specific
        public List<SummonTemplateLevel> summonTemplate;

        /// <summary>
        /// Get the resistance type for the specified effect type.
        /// </summary>
        /// <param name="type">The type to get the resistance for.</param>
        /// <returns>The resistance type, or null if no resistance associated.</returns>
        public EffectType? GetResistanceType(EffectType type)
        {
            switch (type)
            {
                case EffectType.Fear:
                    return EffectType.ResistFear;
                case EffectType.Paralyze:
                    return EffectType.ResistParalyze;
                case EffectType.Sleep:
                    return EffectType.ResistSleep;
                case EffectType.Snare:
                    return EffectType.ResistSnare;
                case EffectType.Stun:
                    return EffectType.ResistStun;
                case EffectType.Aggression:
                    return EffectType.ResistAggression;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Check if effect template is hostile.
        /// </summary>
        /// <returns>True if hostile.</returns>
        public bool IsHostile()
        {
            return type == EffectType.Damage || type == EffectType.DrainHealth || type == EffectType.DrainEnergy ||
                    type == EffectType.Debuff || type == EffectType.Aggression || type == EffectType.Fear || type == EffectType.Paralyze ||
                    type == EffectType.Sleep || type == EffectType.Snare || type == EffectType.Stun;
        }

        /// <summary>
        /// Check if effect template is damager.
        /// </summary>
        /// <returns>True if damager.</returns>
        public bool IsDamager()
        {
            return type == EffectType.Damage || type == EffectType.DrainHealth || type == EffectType.DrainEnergy;
        }

        /// <summary>
        /// Check if effect template is restorer.
        /// </summary>
        /// <returns>True if restorer.</returns>
        public bool IsRestorer()
        {
            return type == EffectType.RestoreHealth || type == EffectType.RestoreEnergy;
        }

        /// <summary>
        /// Check if effect template is stat changer.
        /// </summary>
        /// <returns>True if stat changer.</returns>
        public bool IsStatChanger()
        {
            return type == EffectType.Buff || type == EffectType.Debuff;
        }

        /// <summary>
        /// Check if effect template is cleanser.
        /// </summary>
        /// <returns>True if cleanser.</returns>
        public bool IsCleanser()
        {
            return type == EffectType.CleanseAll || type == EffectType.CleanseDebuffs || type == EffectType.CleanseDisablers || type == EffectType.CleanseEffectType;
        }

        /// <summary>
        /// Check if effect template is disabler.
        /// </summary>
        /// <returns>True if disabler.</returns>
        public bool IsDisabler()
        {
            return type == EffectType.Fear || type == EffectType.Paralyze || type == EffectType.Sleep || type == EffectType.Snare ||
                    type == EffectType.Stun || type == EffectType.Aggression;
        }

        /// <summary>
        /// Check if effect template is resist.
        /// </summary>
        /// <returns>True if resist</returns>
        public bool IsResist()
        {
            return type == EffectType.ResistAggression || type == EffectType.ResistFear || type == EffectType.ResistParalyze ||
                    type == EffectType.ResistSleep || type == EffectType.ResistSnare || type == EffectType.ResistStun;
        }

        /// <summary>
        /// Check if effect template is lasting (stays for a given duration or indefinitely after being applied).
        /// Considered lasting:
        ///   - StatChanger
        ///   - Resist
        ///   - duration[level] > 0
        /// </summary>
        /// <param name="level">The level to test the duration for.</param>
        /// <returns>True if lasting</returns>
        public bool IsLasting(int level = 1)
        {
            return IsStatChanger() || IsResist() || (LevelList.GetFloatLevelValue(duration, level) > 0);
        }

        /// <summary>
        /// Check if effect template can be resisted.
        /// </summary>
        /// <returns>True if can be resisted</returns>
        public bool IsResistable()
        {
            return IsDisabler();
        }

        /// <summary>
        /// Check if effect template has conditions attached.
        /// </summary>
        /// <returns>True if conditions attached.</returns>
        public bool HasConditions()
        {
            return conditions != null && conditions.Count > 0;
        }
    }
}