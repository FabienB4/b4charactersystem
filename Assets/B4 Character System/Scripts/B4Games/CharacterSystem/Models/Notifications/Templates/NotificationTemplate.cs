using UnityEngine;

namespace B4Games.CharacterSystem.Models.Notifications.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Notification Template")]
    public class NotificationTemplate : ScriptableObject
    {
        public string id = "UNKNOWN";
        public string message = "Unknown";
    }
}