using System;
using B4Games.CharacterSystem.Databases;
using B4Games.CharacterSystem.Models.Notifications.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Notifications
{
    public class Notification
    {
        private NotificationTemplate m_Template;
        private object[] m_Params;

        /// <summary>
        /// Formatted Notification message.
        /// </summary>
        /// <returns>The formatted message.</returns>
        public string message
        {
            get
            {
                if (m_Params.Length == 0)
                {
                    return m_Template.message;
                }

                try
                {
                    return string.Format(m_Template.message, m_Params);
                }
                catch (Exception)
                {
#if UNITY_EDITOR
                    Debug.LogError("Notification has invalid parameters");
#endif
                }

                return m_Template.message;
            }
        }

        protected Notification(NotificationTemplate template, params object[] list)
        {
            m_Template = template;
            m_Params = list;
        }

        /// <summary>
        /// Factory for Notification.
        /// </summary>
        /// <param name="template">The notification template to use.</param>
        /// <param name="list">A list of variable for the notification.</param>
        /// <returns>A new Notification instance.</returns>
        public static Notification Create(NotificationTemplate template, params object[] list)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid notification template");
#endif
                return null;
            }

            return new Notification(template, list);
        }

        /// <summary>
        /// Shorthand for Notification.Create(NotificationDatabase.FindById(notificationId), list).
        /// </summary>
        /// <param name="notificationId">The ID of the notification template.</param>
        /// <param name="list">A list of variable for the notification.</param>
        /// <returns>A new Notification instance.</returns>
        public static Notification Create(string notificationId, params object[] list)
        {
            return Notification.Create(NotificationDatabase.FindById(notificationId), list);
        }

        public void AddParams(params object[] list)
        {
            m_Params = list;
        }
    }
}