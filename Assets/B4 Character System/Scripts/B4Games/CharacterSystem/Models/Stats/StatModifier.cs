using System;
using System.Collections;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using UnityEngine;

namespace B4Games.CharacterSystem.Modifiers.Stats
{
    public class StatModifier : IComparable<StatModifier>
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        private Guid m_OriginId;
        public Guid originId { get { return m_OriginId; } }

        private Stat m_Stat;
        public Stat stat { get { return m_Stat; } }

        private StatModifierType m_Type;
        public StatModifierType type { get { return m_Type; } }

        private int m_Order;
        public int order { get { return m_Order; } }

        private float m_Power;
        public float power { get { return m_Power; } }

        private float m_Duration;
        public float duration { get { return m_Duration; } }

        private float m_EndDurationTime;
        public float endDurationTime { get { return m_EndDurationTime; } }

        private Coroutine m_DurationTimerCoroutine = null;

        private Character m_Affected;

        /// <summary>
        /// Constructor for StatModifier.
        /// </summary>
        /// <param name="originId">The Id of object responsible for creating the StatModifier.</param>
        /// <param name="stat">The stat of the modifier.</param>
        /// <param name="type">The type of the modifier.</param>
        /// <param name="order">The processing order of the modifier.</param>
        /// <param name="power">The power of the modifier.</param>
        /// <param name="duration">[Optional] The duration of the modifier. Default: 0.</param>
        protected StatModifier(Guid originId, Stat stat, StatModifierType type, int order, float power, float duration = 0)
        {
            m_Id = Guid.NewGuid();
            m_OriginId = originId;
            m_Stat = stat;
            m_Type = type;
            m_Order = order;
            m_Power = power;
            m_Duration = duration;

            m_EndDurationTime = 0;
        }

        /// <summary>
        /// Factory for StatModifier.
        /// </summary>
        /// <param name="originId">The Id of object responsible for creating the StatModifier.</param>
        /// <param name="stat">The stat of the modifier.</param>
        /// <param name="type">The type of the modifier.</param>
        /// <param name="order">The processing order of the modifier.</param>
        /// <param name="power">The power of the modifier.</param>
        /// <param name="duration">[Optional] The duration of the modifier. Default: 0.</param>
        /// <returns>A new StatModifier instance.</returns>
        public static StatModifier Create(Guid originId, Stat stat, StatModifierType type, int order, float power, float duration = 0)
        {
            if (duration < 0)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid stat modifier duration.");
#endif

                return null;
            }

            return new StatModifier(originId, stat, type, order, power, duration);
        }

        /// <summary>
        /// Comparer for StatModifier. Look at order first (lowest -> highest), then type (Add/Subtract -> Multiply/Divide -> Set).
        /// </summary>
        /// <param name="modifier">The modifier to compare to.</param>
        /// <returns>0 = equal, 1 = after, -1 = before.</returns>
        public int CompareTo(StatModifier modifier)
        {
            if (modifier == null)
            {
                return 1;
            }

            if (this == modifier)
            {
                return 0;
            }

            if (m_Order == modifier.order)
            {
                if (m_Type == modifier.type)
                {
                    return 0;
                }
                else
                {
                    switch (m_Type)
                    {
                        case StatModifierType.Set:
                            switch (modifier.type)
                            {
                                case StatModifierType.Add:
                                case StatModifierType.Subtract:
                                case StatModifierType.Multiply:
                                case StatModifierType.Divide:
                                    return 1;
                                case StatModifierType.Set:
                                default:
                                    return 0;
                            }
                        case StatModifierType.Add:
                        case StatModifierType.Subtract:
                            switch (modifier.type)
                            {
                                case StatModifierType.Set:
                                case StatModifierType.Multiply:
                                case StatModifierType.Divide:
                                    return -1;
                                case StatModifierType.Add:
                                case StatModifierType.Subtract:
                                default:
                                    return 0;
                            }
                        case StatModifierType.Multiply:
                        case StatModifierType.Divide:
                            switch (modifier.type)
                            {
                                case StatModifierType.Set:
                                    return -1;
                                case StatModifierType.Add:
                                case StatModifierType.Subtract:
                                    return 1;
                                case StatModifierType.Multiply:
                                case StatModifierType.Divide:
                                default:
                                    return 0;
                            }
                        default:
                            return 0;
                    }
                }
            }
            else
            {
                return m_Order < modifier.order ? -1 : 1;
            }
        }

        /// <summary>
        /// Apply a modifier to the specified Character.
        /// </summary>
        /// <param name="character">The Character the modifier is being applied to.</param>
        /// <returns>The final value of the stat.</returns>
        public int Apply(Character character)
        {
            if (character == null)
            {
                return 0;
            }

            m_Affected = character;

            StartDurationTimer();

            int initialValue = m_Affected.GetStat(m_Stat);

            switch (m_Type)
            {
                case StatModifierType.Set:
                    return (int)m_Power;
                case StatModifierType.Add:
                    return (int)(initialValue + m_Power);
                case StatModifierType.Subtract:
                    return (int)(initialValue - m_Power);
                case StatModifierType.Multiply:
                    return (int)(initialValue * m_Power);
                case StatModifierType.Divide:
                    // can't divide by zero
                    return (int)(m_Power == 0 ? initialValue : initialValue / m_Power);
                default:
                    return initialValue;
            }
        }

        /// <summary>
        /// Duration timer task.
        /// </summary>
        private IEnumerator DurationTimer()
        {
            while (Time.time < m_EndDurationTime)
            {
                yield return null;
            }

            OnDurationTimerEnd();
        }

        /// <summary>
        /// Start the duration timer task.
        /// </summary>
        private void StartDurationTimer()
        {
            if (m_DurationTimerCoroutine != null)
            {
                return;
            }

            m_EndDurationTime = m_Duration > 0 ? Time.time + m_Duration : 0;

            if (m_EndDurationTime > 0)
            {
                m_DurationTimerCoroutine = m_Affected.StartCoroutine(DurationTimer());
            }
        }

        /// <summary>
        /// Stop the duration timer task.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopDurationTimer(bool end = true)
        {
            if (m_DurationTimerCoroutine == null)
            {
                return;
            }

            m_Affected.StopCoroutine(m_DurationTimerCoroutine);
            OnDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered at the end of duration timer task or if it is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnDurationTimerEnd(bool end = true)
        {
            m_DurationTimerCoroutine = null;
            m_EndDurationTime = 0;

            if (end)
            {
                m_Affected.RemoveStatModifier(this);
            }
        }
    }
}