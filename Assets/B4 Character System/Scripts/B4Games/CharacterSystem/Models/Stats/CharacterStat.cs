using System;
using System.Collections.Generic;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Stats;
using UnityEngine;

namespace B4Games.CharacterSystem.Stats
{
    public class CharacterStat
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        private Character m_Owner;
        public Character owner { get { return m_Owner; } }

        private Dictionary<Stat, int> m_Stats = new Dictionary<Stat, int>();
        public Dictionary<Stat, int> stats { get { return m_Stats; } }

        /// <summary>
        /// Constructor for CharacterStat.
        /// </summary>
        /// <param name="owner">The owner of the set of stats.</param>
        /// <param name="template">The Character template to use.</param>
        protected CharacterStat(Character owner, CharacterTemplate template)
        {
            m_Id = Guid.NewGuid();
            m_Owner = owner;

            // Pools
            m_Stats.Add(Stat.MaxHealth, template.baseMaxHealth);
            m_Stats.Add(Stat.MaxEnergy, template.baseMaxEnergy);
            m_Stats.Add(Stat.HealthRegeneration, template.baseHealthRegeneration);
            m_Stats.Add(Stat.EnergyRegeneration, template.baseEnergyRegeneration);

            // Combat
            m_Stats.Add(Stat.Attack, template.baseAttack);
            m_Stats.Add(Stat.Defense, template.baseDefense);
            m_Stats.Add(Stat.Speed, template.baseSpeed);
            m_Stats.Add(Stat.HealPower, template.baseHealPower);
            m_Stats.Add(Stat.RegenerationPower, template.baseRegenerationPower);
        }

        /// <summary>
        /// Basic factory creator for CharacterStat.
        /// </summary>
        /// <param name="owner">The owner of the set of stats.</param>
        /// <param name="template">The Character template to use.</param>
        /// <returns>A new CharacterStat instance.</returns>
        public static CharacterStat Create(Character owner, CharacterTemplate template)
        {
            if (owner == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid owner.");
#endif

                return null;
            }

            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid Character template.");
#endif

                return null;
            }

            return new CharacterStat(owner, template);
        }

        /// <summary>
        /// Get a stat value.
        /// </summary>
        /// <param name="stat">The stat.</param>
        /// <returns>The value of the stat. 0 if stat doesn't exist.</returns>
        public int GetValue(Stat stat)
        {
            int val;

            if (m_Stats.TryGetValue(stat, out val))
            {
                return val;
            }

            return 0;
        }

        /// <summary>
        /// Set new value for given stat. If the stat doesn't exist, create it with a 0 base value. Take care of signaling owner that health/energy changed if necessary.
        /// </summary>
        /// <param name="stat">The stat to change.</param>
        /// <param name="newValue">The new value to set.</param>
        /// <returns>The new value.</returns>
        public void SetValue(Stat stat, int newValue)
        {
            // make sure stat is in list
            if (!m_Stats.ContainsKey(stat))
            {
                m_Stats.Add(stat, 0);
            }

            int cap = Formulas.GetCap(stat);

            int cappedValue = Mathf.Clamp(newValue, 0, cap > 0 ? cap : int.MaxValue);
            m_Stats[stat] = cappedValue;
        }
    }
}