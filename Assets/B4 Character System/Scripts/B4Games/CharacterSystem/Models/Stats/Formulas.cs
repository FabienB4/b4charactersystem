using System.Collections.Generic;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Effects;

namespace B4Games.CharacterSystem.Models.Stats
{
    public static class Formulas
    {
        /// <summary>
        /// Calculate the effect resistance for a Character.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="effectType">The effect type.</param>
        /// <returns>The effect resistance.</returns>
        public static int CalculateEffectResistance(Character actor, EffectType effectType)
        {
            if (actor == null)
            {
                return 0;
            }

            List<Effect> effects = actor.GetEffectType(effectType);

            if (effects == null)
            {
                return 0;
            }

            float resistance = 0;

            for (int i = 0; i < effects.Count; i++)
            {
                Effect effect = effects[i];

                resistance += effect.power;
            }

            return (int)resistance;
        }

        /// <summary>
        /// Calculate the damage of an effect.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <param name="effect">The damager effect.</param>
        /// <returns>The damage of the effect.</returns>
        public static int CalculateDamage(Character actor, Character target, Effect effect)
        {
            if (!effect.IsDamager())
            {
                return 0;
            }

            return (actor.GetStat(Stat.Attack) + (int)effect.power) - target.GetStat(Stat.Defense);
        }

        /// <summary>
        /// Calculate the restore amount of an effect.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <param name="effect">The restorer effect.</param>
        /// <returns>The restore amount of the effect.</returns>
        public static int CalculateRestoreAmount(Character actor, Character target, Effect effect)
        {
            if (!effect.IsRestorer())
            {
                return 0;
            }

            return (int)(effect.power * actor.healPowerMultiplier * target.regenerationPowerMultiplier);
        }

        /// <summary>
        /// Calculate the chance to resist an effect.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <param name="effect">The resistable effect.</param>
        /// <returns>True if resist.</returns>
        public static bool CalculateResistChance(Character actor, Character target, Effect effect)
        {
            if (!effect.IsResistable())
            {
                return false;
            }

            EffectType? resistanceType = effect.GetResistanceType(effect.template.type);

            return resistanceType != null ? CalculateEffectResistance(target, (EffectType)resistanceType) > effect.power : false;
        }

        /// <summary>
        /// Get cap for stat.
        /// </summary>
        /// <param name="stat">The stat to get cap for.</param>
        /// <returns>The cap for the stat.</returns>
        public static int GetCap(Stat stat)
        {
            switch (stat)
            {
                case Stat.MaxHealth:
                    return Config.Character().maxHealthCap;
                case Stat.MaxEnergy:
                    return Config.Character().maxEnergyCap;
                case Stat.HealthRegeneration:
                    return Config.Character().healthRegenerationCap;
                case Stat.EnergyRegeneration:
                    return Config.Character().energyRegenerationCap;
                case Stat.Attack:
                    return Config.Character().attackCap;
                case Stat.Defense:
                    return Config.Character().defenseCap;
                case Stat.Speed:
                    return Config.Character().speedCap;
                case Stat.HealPower:
                    return Config.Character().healPowerCap;
                case Stat.RegenerationPower:
                    return Config.Character().regenerationPowerCap;
                default:
                    return 0;
            }
        }
    }
}