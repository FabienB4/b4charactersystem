using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Helpers;
using B4Games.CharacterSystem.Interfaces;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Abilities
{
    public class Ability : IUsable
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        protected AbilityTemplate m_Template;
        public AbilityTemplate template { get { return m_Template; } }

        protected Character m_Owner;
        public Character owner { get { return m_Owner; } }

        private int m_Level;
        public int level { get { return m_Level; } }

        private float m_EndCastDurationTime;
        public float endCastDuration { get { return m_EndCastDurationTime; } }

        private float m_EndReuseDurationTime;
        public float endReuseDurationTime { get { return m_EndReuseDurationTime; } }

        private Vector3 m_UsePosition;
        public Vector3 usePosition { get { return m_UsePosition; } }

        public float prefabLifetime { get { return LevelList.GetFloatLevelValue(m_Template.trigger.lifetime, m_Level); } }
        public int healthConsumption { get { return LevelList.GetIntLevelValue(m_Template.healthConsumption, m_Level); } }
        public int energyConsumption { get { return LevelList.GetIntLevelValue(m_Template.energyConsumption, m_Level); } }
        public float castDuration { get { return LevelList.GetFloatLevelValue(m_Template.castDuration, m_Level); } }
        public float castRange { get { return LevelList.GetFloatLevelValue(m_Template.castRange, m_Level); } }
        public float reuseDuration { get { return LevelList.GetFloatLevelValue(m_Template.reuseDuration, m_Level); } }
        public int maxTargets { get { return m_Template.behaviour == AbilityBehaviour.Self ? 1 : LevelList.GetIntLevelValue(m_Template.maxTargets, m_Level); } }
        public Vector3 projectileInitialPosition { get { return LevelList.GetVector3LevelValue(m_Template.projectileInitialPosition, m_Level); } }
        public float projectileForce { get { return LevelList.GetFloatLevelValue(m_Template.projectileForce, m_Level); } }
        public float projectileUpwardForce { get { return LevelList.GetFloatLevelValue(m_Template.projectileUpwardForce, m_Level); } }
        public int projectileCount { get { return LevelList.GetIntLevelValue(m_Template.projectileCount, m_Level); } }
        public float projectileMaxAngle { get { return LevelList.GetFloatLevelValue(m_Template.projectileMaxAngle, m_Level); } }

        private Coroutine m_CastDurationTimerCoroutine = null;
        private Coroutine m_ReuseDurationTimerCoroutine = null;

        /// <summary>
        /// Constructor for Ability.
        /// </summary>
        /// <param name="template">The ability template.</param>
        /// <param name="owner">The owner of the ability.</param>
        /// <param name="level">The level of the ability.</param>
        protected Ability(AbilityTemplate template, Character owner, int level = 1)
        {
            m_Id = Guid.NewGuid();
            m_Template = template;
            m_Owner = owner;
            m_Level = level;

            m_EndCastDurationTime = 0;
            m_EndReuseDurationTime = 0;
        }

        /// <summary>
        /// Basic Factory for Ability.
        /// </summary>
        /// <param name="template">The ability template.</param>
        /// <param name="owner">The owner of the ability.</param>
        /// <param name="level">The level of the ability.</param>
        /// <returns>A new Ability instance.</returns>
        public static Ability Create(AbilityTemplate template, Character owner, int level = 1)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid ability template.");
#endif

                return null;
            }

            if (owner == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Tried to create ability without owner.");
#endif

                return null;
            }

            if (level < 1 || level > template.levels)
            {
#if UNITY_EDITOR
                Debug.LogError("Tried to create ability with invalid level.");
#endif

                return null;
            }

            return new Ability(template, owner, level);
        }

        /// <summary>
        /// Factory for Ability using an AbilityTemplateLevel.
        /// </summary>
        /// <param name="template">The ability level template.</param>
        /// <param name="owner">The owner of the ability.</param>
        /// <returns>A new Ability instance.</returns>
        public static Ability Create(AbilityTemplateLevel template, Character owner)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid ability level template.");
#endif

                return null;
            }

            return Ability.Create(template.template, owner, template.level);
        }

        /// <summary>
        /// Shorthand for Use(m_Owner.transform.position).
        /// </summary>
        public void Use()
        {
            Use(m_Owner.transform.position);
        }

        /// <summary>
        /// Use the ability.
        /// </summary>
        /// <param name="position">The position where to use the ability.</param>
        public void Use(Vector3 position)
        {
            if (m_EndReuseDurationTime != 0)
            {
                m_Owner.Notify(Notification.Create("ABILITY_IN_COOLDOWN"));
                return;
            }

            if (healthConsumption >= m_Owner.health)
            {
                m_Owner.Notify(Notification.Create("NOT_ENOUGH_HEALTH"));
                return;
            }

            if (energyConsumption > m_Owner.energy)
            {
                m_Owner.Notify(Notification.Create("NOT_ENOUGH_ENERGY"));
                return;
            }

            if (!CheckConditions(m_Owner))
            {
                return;
            }

            if (IsDirectional() || IsPositional())
            {
                Vector3 heading = position - m_Owner.transform.position;

                m_Owner.LookAtCastDirection(heading / heading.magnitude);

                // re-adjust cast position if needed
                if (m_Template.behaviour == AbilityBehaviour.Directional)
                {
                    // directional is always spawned on actor's position
                    m_UsePosition = m_Owner.transform.position;
                }
                else if (m_Template.behaviour == AbilityBehaviour.Projectile)
                {
                    // projectile is always spawned in front of actor and its initial position is calculated from there
                    m_UsePosition = m_Owner.transform.position + m_Owner.transform.forward + this.projectileInitialPosition;
                }
                else
                {
                    m_UsePosition = position;
                }
            }
            else if (m_Template.behaviour == AbilityBehaviour.Self)
            {
                // cast at owner's position
                m_UsePosition = m_Owner.transform.position;
            }
            else
            {
                // cast in front of owner
                m_UsePosition = m_Owner.transform.position + m_Owner.transform.forward;
            }

            m_Owner.DecreaseHealth(healthConsumption);
            m_Owner.DecreaseEnergy(energyConsumption);

            // spawns
            m_Owner.Cast(m_Template.castAnimation.castNum);
            CastAnimation();

            StartReuseDurationTimer();

            if (this.castDuration > 0)
            {
                StartCastDurationTimer();
            }
            else
            {
                Cast();
            }
        }

        /// <summary>
        /// Cancel the use of the ability.
        /// </summary>
        public void CancelUse()
        {
            if (m_EndCastDurationTime > 0)
            {
                StopCastDurationTimer(false);
                m_Owner.Cast(0);
            }
        }

        /// <summary>
        /// Register a hit with the specified target.
        /// </summary>
        /// <param name="target">The target Character.</param>
        /// <param name="hitPosition">The position of the hit.</param>
        public void Affect(Character target, Vector3 hitPosition)
        {
            if (target == null)
            {
                return;
            }

            target.RegisterHit(m_Owner, this, hitPosition);
        }

        /// <summary>
        /// Change the level of the ability.
        /// </summary>
        /// <param name="newLevel">The new level.</param>
        public void ChangeLevel(int newLevel)
        {
            if (newLevel < 1 || newLevel == m_Level)
            {
                return;
            }

            m_Level = newLevel;
        }

        /// <summary>
        /// Check the conditions attached to the ability.
        /// </summary>
        /// <param name="actor">The Character acting.</param>
        /// <param name="target">The target Character.</param>
        /// <returns>True if all conditions passed.</returns>
        public bool CheckConditions(Character actor)
        {
            // no condition
            if (!HasConditions())
            {
                return true;
            }

            for (int i = 0; i < m_Template.conditions.Count; i++)
            {
                Condition condition = Condition.Create(m_Template.conditions[i]);

                if (condition == null)
                {
                    continue;
                }

                if (!condition.Test(actor, this))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Determine if ability is usable.
        /// </summary>
        /// <returns>True if usable.</returns>
        public bool IsUsable()
        {
            return m_Template.IsUsable();
        }

        /// <summary>
        /// Determine if ability is directional. A directional ability is casted to move towards the position.
        /// </summary>
        /// <returns>True if directional.</returns>
        public bool IsDirectional()
        {
            return m_Template.IsDirectional();
        }

        /// <summary>
        /// Determine if ability is positional. A positional ability is casted on the ground at the position.
        /// </summary>
        /// <returns>True if positional.</returns>
        public bool IsPositional()
        {
            return m_Template.IsPositional();
        }

        /// <summary>
        /// Determine if ability requires positioning.
        /// </summary>
        /// <returns>True if positioning required.</returns>
        public bool RequirePositioning()
        {
            return m_Template.RequirePositioning();
        }

        /// <summary>
        /// Check whether the ability is a positional self teleport.
        /// </summary>
        /// <returns>True if positional self teleport.</returns>
        public bool IsPositionalSelfTeleport()
        {
            return m_Template.IsPositionalSelfTeleport();
        }

        /// <summary>
        /// Check if the ability requires spawning a prefab to work.
        /// </summary>
        /// <returns>True if prefab required.</returns>
        public bool RequireSpawningPrefab()
        {
            return m_Template.RequireSpawningPrefab();
        }

        /// <summary>
        /// Calculate the collider width of the ability prefab.
        /// </summary>
        /// <returns>The width of the collider.</returns>
        public float CalculatePrefabWidth()
        {
            return m_Template.CalculatePrefabWidth();
        }

        /// <summary>
        /// Check if ability has conditions attached.
        /// </summary>
        /// <returns>True if conditions attached.</returns>
        public bool HasConditions()
        {
            return m_Template.HasConditions();
        }

        /// <summary>
        /// Spawn cast animation.
        /// </summary>
        private void CastAnimation()
        {
            if (m_Template.castAnimation.prefab != null && this.castDuration > 0)
            {
                Vector3 charFront = m_Owner.transform.position + m_Owner.transform.forward;
                Vector3 position = new Vector3(charFront.x, m_Template.castAnimation.prefabHeight, charFront.z);

                GameObject castPrefabInstance = GameObject.Instantiate(m_Template.castAnimation.prefab, position, m_Owner.transform.rotation) as GameObject;

                GameObject.Destroy(castPrefabInstance, this.castDuration * m_Owner.speedMultiplier);
            }
        }

        /// <summary>
        /// Cast the ability.
        /// </summary>
        private void Cast()
        {
            if (RequireSpawningPrefab())
            {
                if (m_Template.trigger.prefab == null)
                {
#if UNITY_EDITOR
                    Debug.LogError("Ability has no prefab assigned");
#endif

                    return;
                }

                if (m_EndCastDurationTime != 0)
                {
                    m_Owner.Notify(Notification.Create("ALREADY_CASTING"));
                    return;
                }

                // more than one projectile
                if (m_Template.behaviour == AbilityBehaviour.Projectile && this.projectileCount > 1)
                {
                    List<Vector3[]> trajectories = CalculateProjectileTrajectories();

                    // ignore if trajectory calculation fails somehow
                    if (trajectories != null && trajectories.Count == this.projectileCount)
                    {
                        for (int i = 0; i < this.projectileCount; i++)
                        {
                            AbilityPrefab.Spawn(m_Owner, this, trajectories[i][0], trajectories[i][1]);
                        }
                    }
                }
                else
                {
                    // not a projectile, or single.
                    AbilityPrefab.Spawn(m_Owner, this, m_UsePosition, m_Owner.transform.forward);
                }
            }
            else
            {
                // spawn shell prefab for animations and such
                AbilityPrefab.Spawn(m_Owner, this, m_UsePosition, m_Owner.transform.forward, true);
                Affect(m_Owner, m_UsePosition);
            }

            // reset animation after cast
            m_Owner.Cast(0);
        }

        /// <summary>
        /// Calculate the trajectories of projectiles.
        /// </summary>
        /// <returns>List of trajectories - position + direction.</returns>
        private List<Vector3[]> CalculateProjectileTrajectories()
        {
            // single projectile uses forward, avoid unnecessary calculations
            if (this.projectileCount == 1)
            {
                return null;
            }

            List<Vector3[]> trajectories = new List<Vector3[]>(this.projectileCount);
            // position calculations
            float prefabWidth = CalculatePrefabWidth();
            float spaceWidth = prefabWidth / 4;

            // 2 half widths + space
            float spacingPosition = prefabWidth + spaceWidth;
            Vector3 furthestLeftPosition = m_UsePosition - (m_Owner.transform.right * (((prefabWidth / 2) + (spaceWidth / 2)) * (this.projectileCount - 1)));

            // direction calculations
            float minAngle = -(this.projectileMaxAngle / 2);
            float angleDivided = this.projectileMaxAngle / (this.projectileCount - 1);

            for (int i = 0; i < this.projectileCount; i++)
            {
                Vector3 newPosition = furthestLeftPosition + (m_Owner.transform.right * (spacingPosition * i));
                Vector3 newDirection = Quaternion.AngleAxis(minAngle + (angleDivided * i), Vector3.up) * m_Owner.transform.forward;

#if UNITY_EDITOR
                Debug.DrawLine(newPosition, newPosition + Vector3.down * 1, Color.blue, 10.0f);
                Debug.DrawLine(newPosition, newPosition + newDirection * 3, Color.red, 10.0f);
#endif

                trajectories.Add(new Vector3[2] { newPosition, newDirection.normalized });
            }

            return trajectories;
        }

        /// <summary>
        /// Cast timer task.
        /// </summary>
        private IEnumerator CastDurationTimer()
        {
            while (Time.time < m_EndCastDurationTime)
            {
                yield return null;
            }

            OnCastDurationTimerEnd();
        }

        /// <summary>
        /// Stop the cast timer task.
        /// </summary>
        private void StartCastDurationTimer()
        {
            if (m_CastDurationTimerCoroutine != null)
            {
                return;
            }

            // take speed into consideration
            m_EndCastDurationTime = this.castDuration > 0 ? (Time.time + (this.castDuration * m_Owner.speedMultiplier)) : 0;

            if (m_EndCastDurationTime > 0)
            {
                m_CastDurationTimerCoroutine = m_Owner.StartCoroutine(CastDurationTimer());
            }
        }

        /// <summary>
        /// Stop the cast timer task.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopCastDurationTimer(bool end = true)
        {
            if (m_CastDurationTimerCoroutine == null)
            {
                return;
            }

            m_Owner.StopCoroutine(m_CastDurationTimerCoroutine);
            OnCastDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered when the cast timer task ends or is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnCastDurationTimerEnd(bool end = true)
        {
            m_CastDurationTimerCoroutine = null;
            m_EndCastDurationTime = 0;

            if (end)
            {
                Cast();
            }
        }

        /// <summary>
        /// Reuse timer task.
        /// </summary>
        private IEnumerator ReuseDurationTimer()
        {
            while (Time.time < m_EndReuseDurationTime)
            {
                yield return null;
            }

            OnReuseDurationTimerEnd();
        }

        /// <summary>
        /// Start the reuse timer task.
        /// </summary>
        private void StartReuseDurationTimer()
        {
            if (m_ReuseDurationTimerCoroutine != null)
            {
                return;
            }

            m_EndReuseDurationTime = this.reuseDuration > 0 ? (Time.time + this.reuseDuration) : 0;

            if (m_EndReuseDurationTime > 0)
            {
                m_ReuseDurationTimerCoroutine = m_Owner.StartCoroutine(ReuseDurationTimer());
            }
        }

        /// <summary>
        /// Stop the reuse timer task.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopReuseDurationTimer(bool end = true)
        {
            if (m_ReuseDurationTimerCoroutine == null)
            {
                return;
            }

            m_Owner.StopCoroutine(m_ReuseDurationTimerCoroutine);
            OnReuseDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered when the reuse timer task ends or is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnReuseDurationTimerEnd(bool end = true)
        {
            m_ReuseDurationTimerCoroutine = null;
            m_EndReuseDurationTime = 0;

            if (end)
            {
            }
        }
    }
}