using System.Collections;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.UnityEditorExtended;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Abilities
{
    public class AbilityPrefab : MonoBehaviour
    {
        private Character m_Actor;
        private Ability m_Ability;
        private Rigidbody m_Rigidbody;
        private float m_Duration;
        private float m_EndDurationTime;
        private bool m_ProcessCollisions;
        private int m_AffectedTargetCount;
        private Coroutine m_DurationTimerCoroutine = null;

        void Start()
        {
            if (m_Actor == null || m_Ability == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Missing property for Ability Prefab, removing prefab.", this);
#endif

                Destroy(gameObject);
                return;
            }

            m_Rigidbody = transform.GetOrAddComponent<Rigidbody>();

            m_Duration = m_Ability.prefabLifetime > 0 ? m_Ability.prefabLifetime : 0;

            StartDurationTimer();

            // only projectiles can move
            if (m_Ability.template.behaviour == AbilityBehaviour.Projectile)
            {
                m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation;

                Vector3 force = (this.transform.forward * m_Ability.projectileForce) + (this.transform.up * m_Ability.projectileUpwardForce);

                m_Rigidbody.AddForce(force, m_Ability.template.projectileForceMode);
            }
            else
            {
                // rigidbody cannot move, just used to detect collisions
                m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            DetectCollision(collision.collider);
        }

        void OnTriggerEnter(Collider other)
        {
            DetectCollision(other);
        }

        /// <summary>
        /// Spawn prefab and setup behaviour.
        /// </summary>
        /// <param name="actor">The Character spawning the prefab.</param>
        /// <param name="ability">The Ability linked to the prefab. Can be null (self-teleports).</param>
        /// <param name="position">The position of the prefab.</param>
        /// <param name="direction">The direction fo the prefab.</param>
        /// <param name="noCollision">[Optional] Avoid processing collisions. Default: false</param>
        public static AbilityPrefab Spawn(Character actor, Ability ability, Vector3 position, Vector3 direction, bool noCollision = false)
        {
            if (actor == null)
            {
                return null;
            }

            GameObject prefabGo = Instantiate(ability.template.trigger.prefab, position, Quaternion.identity) as GameObject;
            prefabGo.transform.forward = direction;
            prefabGo.layer = LayerMask.NameToLayer(Config.Ability().ignoreIndicatorCollisionLayerName);

            AbilityPrefab prefabInstance = prefabGo.GetOrAddComponent<AbilityPrefab>();

            // set properties
            prefabInstance.m_Actor = actor;
            prefabInstance.m_Ability = ability;
            prefabInstance.m_Duration = 0;
            prefabInstance.m_ProcessCollisions = !noCollision;
            prefabInstance.m_AffectedTargetCount = 0;
            prefabInstance.m_DurationTimerCoroutine = null;

            return prefabInstance;
        }

        /// <summary>
        /// Detect a collision with the prefab.
        /// </summary>
        /// <param name="other">The collider.</param>
        private void DetectCollision(Collider other)
        {
            if (!m_ProcessCollisions)
            {
                return;
            }

            if (m_Ability == null || other == null || other.GetComponent<AbilityPrefab>() != null ||
                other.gameObject.layer == LayerMask.NameToLayer(Config.Ability().ignoreCollisionLayerName))
            {
                return;
            }

            Character target = other.GetComponent<Character>();

            if (target == null)
            {
                if (m_Ability.template.trigger.destroyOnNonCharacterCollision)
                {
                    Destroy(this.gameObject);
                }

                return;
            }

            // affect hit target
            m_Ability.Affect(target, this.transform.position);

            // increment the number of targets hit
            m_AffectedTargetCount++;

            // ignore max targets if 0
            if (m_Ability.maxTargets > 0 && m_AffectedTargetCount == m_Ability.maxTargets)
            {
                Destroy(this.gameObject);
            }
        }

        /// <summary>
        /// Duration timer task.
        /// </summary>
        private IEnumerator DurationTimer()
        {
            while (Time.time < m_EndDurationTime)
            {
                yield return null;
            }

            OnDurationTimerEnd();
        }

        /// <summary>
        /// Start the duration timer task.
        /// </summary>
        private void StartDurationTimer()
        {
            if (m_DurationTimerCoroutine != null)
            {
                return;
            }

            m_EndDurationTime = m_Duration > 0 ? Time.time + m_Duration : 0;

            if (m_EndDurationTime > 0)
            {
                m_DurationTimerCoroutine = StartCoroutine(DurationTimer());
            }
        }

        /// <summary>
        /// Stop the duration timer task.
        /// Stopping the task does not remove the prefab from the game.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopDurationTimer(bool end = true)
        {
            if (m_DurationTimerCoroutine == null)
            {
                return;
            }

            StopCoroutine(m_DurationTimerCoroutine);
            OnDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered when the duration timer task ends or is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnDurationTimerEnd(bool end = true)
        {
            m_DurationTimerCoroutine = null;
            m_EndDurationTime = 0;

            if (end)
            {
                Destroy(this.gameObject);
            }
        }
    }
}