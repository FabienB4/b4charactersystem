using System.Collections.Generic;
using UnityEngine;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Effects.Templates;
using System;
using B4Games.CharacterSystem.Models.Conditions.Templates;

namespace B4Games.CharacterSystem.Models.Abilities.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Ability Template")]
    public class AbilityTemplate : ScriptableObject
    {
        [Serializable]
        public struct CastAnimation
        {
            [Tooltip("The animation to play on the character.\nIf 0, no animation is played.")]
            public int castNum;
            [Tooltip("The prefab holding the cast animation for the ability.\nIf empty, no animation is created.")]
            public GameObject prefab;
            [Tooltip("The Y position of the cast animation.\nUsually refers to the height of the character's hand.")]
            public float prefabHeight;
        }

        [Serializable]
        public struct Trigger
        {
            [Tooltip("The game object used to trigger the ability. Use it to display animations and meshes regarding the ability.\nIt a Rigidbody component is not present, it will be created.")]
            public GameObject prefab;
            [Tooltip("The lifetime (in seconds) of the prefab once spawned.\nIf 0, the prefab is destroyed only when the max targets is reached.\nWARNING: If 0 and the behaviour does not process max targets (or also 0), the prefab will stay spawned forever.")]
            public List<float> lifetime;
            [Tooltip("If true, the prefab is destroyed when colliding with any object that isn't a character, unless this object is in the special ignored layer.")]
            public bool destroyOnNonCharacterCollision;
        }

        [Header("Display")]
        public string abilityName;
        public string description;
        public Sprite icon;

        [Header("Spawns")]
        public CastAnimation castAnimation;
        public Trigger trigger;

        [Header("Behaviour")]
        public AbilityType type;
        [Tooltip("The behaviour of the ability when casted.\nInfluences the instantiated prefab.")]
        public AbilityBehaviour behaviour;
        [Tooltip("If true, the behaviour uses the direction the caster is facing instead of requiring the player to set a position/direction.")]
        public bool instantBehaviour = false;

        [Header("Values")]

        [Tooltip("How many levels does this ability have?")]
        public int levels = 1;
        public List<int> healthConsumption;
        public List<int> energyConsumption;
        [Tooltip("A cast time greater than 0 prevents the caster from moving until the end.\nIf the caster moves, the cast is cancelled.")]
        public List<float> castDuration;
        [Tooltip("Maximum cast range in meters.\nOnly for positional abilities.")]
        public List<float> castRange;
        public List<float> reuseDuration;
        [Tooltip("The maximum number of characters that can be affected by the effect.\n If 0, as long as the prefab is spawned, characters coming in contact will get affected.")]
        public List<int> maxTargets;
        public List<ConditionTemplate> conditions;

        [Space]

        public List<EffectTemplate> effectTemplates;

        // Projectile Specific
        [Tooltip("The position (x,y,z) in front of caster the projectile is initially spawned at.")]
        public List<Vector3> projectileInitialPosition;
        public List<float> projectileForce;
        [Tooltip("How much should the projectile be pushed from below (arc of the trajectory).\nCan be combined with initial position for infinite possibilities (supports positive and negative values).")]
        public List<float> projectileUpwardForce;
        public ForceMode projectileForceMode;
        public List<int> projectileCount;
        public List<float> projectileMaxAngle;

        /// <summary>
        /// Determine if ability template is usable.
        /// </summary>
        /// <returns>True if usable.</returns>
        public bool IsUsable()
        {
            return type == AbilityType.Active;
        }

        /// <summary>
        /// Determine if ability template is directional. A directional ability is casted to move towards the position.
        /// </summary>
        /// <returns>True if directional.</returns>
        public bool IsDirectional()
        {
            return behaviour == AbilityBehaviour.Directional || behaviour == AbilityBehaviour.Projectile;
        }

        /// <summary>
        /// Determine if ability template is positional. A positional ability is casted on the ground at the position.
        /// </summary>
        /// <returns>True if positional.</returns>
        public bool IsPositional()
        {
            return behaviour == AbilityBehaviour.Positional;
        }

        /// <summary>
        /// Determine if ability template requires positioning.
        /// </summary>
        /// <returns>True if positioning required.</returns>
        public bool RequirePositioning()
        {
            return IsDirectional() || IsPositional();
        }

        /// <summary>
        /// Check whether the ability is a positional self teleport.
        /// </summary>
        /// <returns>True if positional self teleport.</returns>
        public bool IsPositionalSelfTeleport()
        {
            if (type != AbilityType.Active || behaviour != AbilityBehaviour.Positional)
            {
                return false;
            }

            for (int i = 0; i < effectTemplates.Count; i++)
            {
                EffectTemplate effectTemplate = effectTemplates[i];

                if (effectTemplate.type == EffectType.TeleportPosition && effectTemplate.targetType == TargetType.Self)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Check if the ability template requires spawning a prefab to work.
        /// </summary>
        /// <returns>True if prefab required.</returns>
        public bool RequireSpawningPrefab()
        {
            return type == AbilityType.Active && (behaviour != AbilityBehaviour.Self && behaviour != AbilityBehaviour.Directional && !IsPositionalSelfTeleport());
        }

        /// <summary>
        /// Check if ability template has conditions attached.
        /// </summary>
        /// <returns>True if conditions attached.</returns>
        public bool HasConditions()
        {
            return conditions != null && conditions.Count > 0;
        }

        /// <summary>
        /// Calculate the collider width of the ability template prefab.
        /// </summary>
        /// <returns>The width of the collider.</returns>
        public float CalculatePrefabWidth()
        {
            if (trigger.prefab == null)
            {
                return 0;
            }

            float width = 0;
            Collider collider = trigger.prefab.GetComponent<Collider>();

            if (collider != null)
            {
                if (collider is SphereCollider)
                {
                    width = ((SphereCollider)collider).radius * 2;
                }
                else if (collider is CapsuleCollider)
                {
                    width = ((CapsuleCollider)collider).radius * 2;
                }
                else if (collider is WheelCollider)
                {
                    width = ((WheelCollider)collider).radius * 2;
                }
                else
                {
                    width = collider.bounds.size.x;
                }
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogError("Tried to calculate the width of a prefab that does not have a collider.");
#endif
            }

            if (width == 0)
            {
                // as a last resort, use renderer's width
                // this won't work in many cases (like when using particules)
                Renderer renderer = trigger.prefab.GetComponent<Renderer>();

                if (renderer != null)
                {
                    width = renderer.bounds.size.x;
                }
            }

            return width;
        }
    }
}