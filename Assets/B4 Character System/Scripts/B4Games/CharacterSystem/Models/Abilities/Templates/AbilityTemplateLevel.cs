using System;

namespace B4Games.CharacterSystem.Models.Abilities.Templates
{
    [Serializable]
    public class AbilityTemplateLevel
    {
        public AbilityTemplate template;
        public int level = 1;
    }
}