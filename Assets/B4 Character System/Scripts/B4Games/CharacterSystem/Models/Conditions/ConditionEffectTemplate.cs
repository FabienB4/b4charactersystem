using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionEffectTemplate : Condition
    {
        protected ConditionEffectTemplate(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionEffectTemplate Create(ConditionTemplate template)
        {
            if (template.effectTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid template, missing effect template");
#endif
            }

            return new ConditionEffectTemplate(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !conditionTarget.HasEffect(m_Template.effectTemplate, m_Template.intVal) :
                                                                     conditionTarget.HasEffect(m_Template.effectTemplate, m_Template.intVal));
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_EFFECT" : "CONDITION_EFFECT",
                m_Template.effectTemplate.effectName,
                m_Template.intVal,
                m_Template.target.ToString()
            );
        }
    }
}