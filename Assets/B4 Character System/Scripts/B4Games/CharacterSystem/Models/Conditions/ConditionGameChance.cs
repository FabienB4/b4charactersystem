using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionGameChance : Condition
    {
        protected ConditionGameChance(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionGameChance Create(ConditionTemplate template)
        {
            return new ConditionGameChance(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            return Random.Range(1, 101) <= m_Template.intVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create("CONDITION_GAME_CHANCE");
        }
    }
}