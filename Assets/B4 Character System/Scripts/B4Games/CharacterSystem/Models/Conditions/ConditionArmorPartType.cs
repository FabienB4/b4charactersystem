using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionArmorPartType : Condition
    {
        protected ConditionArmorPartType(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionArmorPartType Create(ConditionTemplate template)
        {
            return new ConditionArmorPartType(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return false;
            }

            Equippable armorPart = conditionTarget.equipment.GetCurrentArmorPart(m_Template.armorPartSlot);

            if (armorPart == null)
            {
                return false;
            }

            return m_Template.boolVal ? armorPart.GetArmorPart().template.type != m_Template.armorPartType :
                                        armorPart.GetArmorPart().template.type == m_Template.armorPartType;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_ARMOR_PART_TYPE" : "CONDITION_ARMOR_PART_TYPE",
                m_Template.armorPartType,
                m_Template.armorPartSlot,
                m_Template.target.ToString()
            );
        }
    }
}