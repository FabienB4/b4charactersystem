using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionEffectType : Condition
    {
        protected ConditionEffectType(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionEffectType Create(ConditionTemplate template)
        {
            return new ConditionEffectType(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !conditionTarget.HasEffectType(m_Template.effectType, m_Template.intVal) :
                                                                     conditionTarget.HasEffectType(m_Template.effectType, m_Template.intVal));
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_EFFECT_TYPE" : "CONDITION_EFFECT_TYPE",
                m_Template.effectType,
                m_Template.intVal,
                m_Template.target.ToString()
            );
        }
    }
}