using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionAbilityTemplate : Condition
    {
        protected ConditionAbilityTemplate(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionAbilityTemplate Create(ConditionTemplate template)
        {
            if (template.abilityTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid template, missing ability template");
#endif
            }

            return new ConditionAbilityTemplate(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !conditionTarget.HasAbility(m_Template.abilityTemplate, m_Template.intVal) :
                                                                     conditionTarget.HasAbility(m_Template.abilityTemplate, m_Template.intVal));
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_ABILITY" : "CONDITION_ABILITY",
                m_Template.abilityTemplate.abilityName,
                m_Template.intVal,
                m_Template.target.ToString()
            );
        }
    }
}