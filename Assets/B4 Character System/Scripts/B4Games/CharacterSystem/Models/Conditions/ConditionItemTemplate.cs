using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionItemTemplate : Condition
    {
        protected ConditionItemTemplate(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionItemTemplate Create(ConditionTemplate template)
        {
            if (template.itemTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid template, missing item template");
#endif
            }

            return new ConditionItemTemplate(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !conditionTarget.HasItem(m_Template.itemTemplate) :
                                                                     conditionTarget.HasItem(m_Template.itemTemplate));
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_ITEM" : "CONDITION_ITEM",
                m_Template.itemTemplate.itemName,
                m_Template.target.ToString()
            );
        }
    }
}