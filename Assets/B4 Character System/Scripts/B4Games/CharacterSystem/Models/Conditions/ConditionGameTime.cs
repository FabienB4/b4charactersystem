using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionGameTime : Condition
    {
        protected ConditionGameTime(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionGameTime Create(ConditionTemplate template)
        {
            return new ConditionGameTime(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            return Time.time == m_Template.floatVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create("CONDITION_GAME_TIME", m_Template.floatVal);
        }
    }
}