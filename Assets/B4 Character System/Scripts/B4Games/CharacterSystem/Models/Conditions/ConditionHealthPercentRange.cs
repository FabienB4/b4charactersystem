using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionHealthPercentRange : Condition
    {
        protected ConditionHealthPercentRange(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionHealthPercentRange Create(ConditionTemplate template)
        {
            return new ConditionHealthPercentRange(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return false;
            }

            int healthPercent = conditionTarget.GetHealthPercent();

            return healthPercent >= m_Template.intRangeMinVal && healthPercent <= m_Template.intRangeMaxVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create("CONDITION_HEALTH_PERCENT_RANGE", m_Template.intRangeMinVal, m_Template.intRangeMaxVal, m_Template.target.ToString());
        }
    }
}