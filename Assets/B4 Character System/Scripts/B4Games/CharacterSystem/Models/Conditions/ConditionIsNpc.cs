using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionIsNpc : Condition
    {
        protected ConditionIsNpc(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionIsNpc Create(ConditionTemplate template)
        {
            return new ConditionIsNpc(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !(conditionTarget is Npc) :
                                                                     conditionTarget is Npc);
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_NPC_CHARACTER" : "CONDITION_NPC_CHARACTER",
                m_Template.target.ToString()
            );
        }
    }
}