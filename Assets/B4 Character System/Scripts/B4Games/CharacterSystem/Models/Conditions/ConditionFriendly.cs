using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionFriendly : Condition
    {
        protected ConditionFriendly(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionFriendly Create(ConditionTemplate template)
        {
            return new ConditionFriendly(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            if (actor == null || target == null || actor == target)
            {
                return false;
            }

            return m_Template.boolVal ? !actor.IsFriendlyWith(actor) : actor.IsFriendlyWith(target);
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create(m_Template.boolVal ? "CONDITION_NOT_FRIENDLY" : "CONDITION_FRIENDLY");
        }
    }
}