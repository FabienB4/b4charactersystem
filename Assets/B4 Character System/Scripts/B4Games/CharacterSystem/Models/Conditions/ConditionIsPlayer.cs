using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionIsPlayer : Condition
    {
        protected ConditionIsPlayer(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionIsPlayer Create(ConditionTemplate template)
        {
            return new ConditionIsPlayer(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !(conditionTarget is Player) :
                                                                     conditionTarget is Player);
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_PLAYER_CHARACTER" : "CONDITION_PLAYER_CHARACTER",
                m_Template.target.ToString()
            );
        }
    }
}