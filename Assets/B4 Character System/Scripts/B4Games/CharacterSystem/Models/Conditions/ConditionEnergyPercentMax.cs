using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionEnergyPercentMax : Condition
    {
        protected ConditionEnergyPercentMax(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionEnergyPercentMax Create(ConditionTemplate template)
        {
            return new ConditionEnergyPercentMax(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && conditionTarget.GetEnergyPercent() <= m_Template.intVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create("CONDITION_ENERGY_PERCENT_MAX", m_Template.intVal, m_Template.target.ToString());
        }
    }
}