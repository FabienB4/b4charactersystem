using B4Games.CharacterSystem.Databases;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionSummonTemplate : Condition
    {
        protected ConditionSummonTemplate(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionSummonTemplate Create(ConditionTemplate template)
        {
            if (template.summonTemplate == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid template, missing summon template");
#endif
            }

            return new ConditionSummonTemplate(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? !conditionTarget.HasSummon(m_Template.summonTemplate, m_Template.intVal) :
                                                                     conditionTarget.HasSummon(m_Template.summonTemplate, m_Template.intVal));
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_SUMMON" : "CONDITION_SUMMON",
                m_Template.summonTemplate.characterName,
                m_Template.intVal,
                m_Template.target.ToString()
            );
        }
    }
}