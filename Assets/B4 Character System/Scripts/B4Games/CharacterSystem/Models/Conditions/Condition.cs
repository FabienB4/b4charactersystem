using System;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public abstract class Condition
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        protected ConditionTemplate m_Template;
        public ConditionTemplate template { get { return m_Template; } }

        /// <summary>
        /// Constructor for Condition.
        /// </summary>
        /// <param name="template">The condition template.</param>
        protected Condition(ConditionTemplate template)
        {
            m_Id = Guid.NewGuid();
            m_Template = template;
        }

        /// <summary>
        /// Factory for Condition. Construct a Condition class according to the template type.
        /// </summary>
        /// <param name="template">The condition template to use.</param>
        /// <returns>A new Condition instance.</returns>
        public static Condition Create(ConditionTemplate template)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid condition template");
#endif

                return null;
            }

            switch (template.type)
            {
                // Game
                case ConditionType.GameChance:
                    return ConditionGameChance.Create(template);
                case ConditionType.GameTime:
                    return ConditionGameTime.Create(template);
                // Player || Target
                case ConditionType.IsPlayer:
                    return ConditionIsPlayer.Create(template);
                case ConditionType.IsNpc:
                    return ConditionIsNpc.Create(template);
                case ConditionType.IsSummon:
                    return ConditionIsSummon.Create(template);
                case ConditionType.Distance:
                    return ConditionDistance.Create(template);
                case ConditionType.DistanceMin:
                    return ConditionDistanceMin.Create(template);
                case ConditionType.DistanceMax:
                    return ConditionDistanceMax.Create(template);
                case ConditionType.DistanceRange:
                    return ConditionDistanceRange.Create(template);
                case ConditionType.Level:
                    return ConditionLevel.Create(template);
                case ConditionType.LevelMin:
                    return ConditionLevelMin.Create(template);
                case ConditionType.LevelMax:
                    return ConditionLevelMax.Create(template);
                case ConditionType.LevelRange:
                    return ConditionLevelRange.Create(template);
                case ConditionType.LevelDifference:
                    return ConditionLevelDifference.Create(template);
                case ConditionType.LevelDifferenceMin:
                    return ConditionLevelDifferenceMin.Create(template);
                case ConditionType.LevelDifferenceMax:
                    return ConditionLevelDifferenceMax.Create(template);
                case ConditionType.LevelDifferenceRange:
                    return ConditionLevelDifferenceRange.Create(template);
                case ConditionType.Stat:
                    return ConditionStat.Create(template);
                case ConditionType.StatMin:
                    return ConditionStatMin.Create(template);
                case ConditionType.StatMax:
                    return ConditionStatMax.Create(template);
                case ConditionType.StatRange:
                    return ConditionStatRange.Create(template);
                case ConditionType.HealthPercent:
                    return ConditionHealthPercent.Create(template);
                case ConditionType.HealthPercentMin:
                    return ConditionHealthPercentMin.Create(template);
                case ConditionType.HealthPercentMax:
                    return ConditionHealthPercentMax.Create(template);
                case ConditionType.HealthPercentRange:
                    return ConditionHealthPercentRange.Create(template);
                case ConditionType.EnergyPercent:
                    return ConditionEnergyPercent.Create(template);
                case ConditionType.EnergyPercentMin:
                    return ConditionEnergyPercentMin.Create(template);
                case ConditionType.EnergyPercentMax:
                    return ConditionEnergyPercentMax.Create(template);
                case ConditionType.EnergyPercentRange:
                    return ConditionEnergyPercentRange.Create(template);
                case ConditionType.CharacterStatus:
                    return ConditionCharacterStatus.Create(template);
                case ConditionType.EffectType:
                    return ConditionEffectType.Create(template);
                case ConditionType.ItemTemplate:
                    return ConditionItemTemplate.Create(template);
                case ConditionType.AbilityTemplate:
                    return ConditionAbilityTemplate.Create(template);
                case ConditionType.EffectTemplate:
                    return ConditionEffectTemplate.Create(template);
                case ConditionType.SummonTemplate:
                    return ConditionSummonTemplate.Create(template);
                case ConditionType.Neutral:
                    return ConditionNeutral.Create(template);
                case ConditionType.Friendly:
                    return ConditionFriendly.Create(template);
                case ConditionType.Enemy:
                    return ConditionEnemy.Create(template);
                case ConditionType.ZoneType:
                    return ConditionZoneType.Create(template);
                case ConditionType.WeaponType:
                    return ConditionWeaponType.Create(template);
                case ConditionType.ArmorPartType:
                    return ConditionArmorPartType.Create(template);
                default:
                    return null;
            }
        }

        /// <summary>
        /// Determine the actor of the condition according to the template.
        /// </summary>
        /// <param name="actor">The actor</param>
        /// <param name="target">The actor's target</param>
        /// <returns>The actor Character.</returns>
        protected virtual Character GetConditionActor(Character actor, Character target)
        {
            return m_Template != null && (m_Template.target == ConditionTarget.Actor) ? target : actor;
        }

        /// <summary>
        /// Determine the target of the condition according to the template.
        /// </summary>
        /// <param name="actor">The actor</param>
        /// <param name="target">The actor's target</param>
        /// <returns>The target Character.</returns>
        protected virtual Character GetConditionTarget(Character actor, Character target)
        {
            return m_Template != null && (m_Template.target == ConditionTarget.Actor) ? actor : target;
        }

        /// <summary>
        /// Shorthand for Test(actor, null, null, null, null).
        /// </summary>
        public bool Test(Character actor)
        {
            return Test(actor, null, null, null, null);
        }

        /// <summary>
        /// Shorthand for Test(actor, null, effect, null, null).
        /// </summary>
        public bool Test(Character actor, Effect effect)
        {
            return Test(actor, null, effect, null, null);
        }

        /// <summary>
        /// Shorthand for Test(actor, null, null, item, null).
        /// </summary>
        public bool Test(Character actor, Item item)
        {
            return Test(actor, null, null, item, null);
        }

        /// <summary>
        /// Shorthand for Test(actor, null, null, null, ability).
        /// </summary>
        public bool Test(Character actor, Ability ability)
        {
            return Test(actor, null, null, null, ability);
        }

        /// <summary>
        /// Shorthand for Test(actor, target, null, null, null).
        /// </summary>
        public bool Test(Character actor, Character target)
        {
            return Test(actor, target, null, null, null);
        }

        /// <summary>
        /// Shorthand for Test(actor, target, effect, null, null).
        /// </summary>
        public bool Test(Character actor, Character target, Effect effect)
        {
            return Test(actor, target, effect, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Test(Character actor, Character target, Item item)
        {
            return Test(actor, target, null, item, null);
        }

        /// <summary>
        /// Shorthand for Test(actor, target, null, null, ability).
        /// </summary>
        public bool Test(Character actor, Character target, Ability ability)
        {
            return Test(actor, target, null, null, ability);
        }

        /// <summary>
        /// Test the condition.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <param name="effect">The effect.</param>
        /// <param name="item">The item.</param>
        /// <param name="ability">The ability.</param>
        /// <returns>True if the condition test passed.</returns>
        public bool Test(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            bool pass = TestImplementation(actor, target, effect, item, ability);

            if (!pass)
            {
                FailNotify(actor, target);
            }

            return pass;
        }

        /// <summary>
        /// Notify on failure.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        public void FailNotify(Character actor, Character target)
        {
            if (actor == null)
            {
                return;
            }

            actor.Notify(FailureNotification(actor, target));
        }

        /// <summary>
        /// Implementation of the condition type.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <param name="effect">The effect.</param>
        /// <param name="item">The item.</param>
        /// <param name="ability">The ability.</param>
        /// <returns>True if the condition test passed.</returns>
        public abstract bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability);

        /// <summary>
        /// The failure notification for the condition type.
        /// </summary>
        /// <param name="actor">The actor.</param>
        /// <param name="target">The target.</param>
        /// <returns>A Notification instance.</returns>
        public abstract Notification FailureNotification(Character actor, Character target);
    }
}