using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionLevelDifference : Condition
    {
        protected ConditionLevelDifference(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionLevelDifference Create(ConditionTemplate template)
        {
            return new ConditionLevelDifference(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            if (actor == null || target == null)
            {
                return false;
            }

            int levelDifference = actor.level - target.level;

            return levelDifference == m_Template.intVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create("CONDITION_LEVEL_DIFFERENCE", m_Template.intVal);
        }
    }
}