using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionCharacterStatus : Condition
    {
        protected ConditionCharacterStatus(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionCharacterStatus Create(ConditionTemplate template)
        {
            return new ConditionCharacterStatus(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            return conditionTarget != null && (m_Template.boolVal ? conditionTarget.status != m_Template.characterStatus :
                                                                    conditionTarget.status == m_Template.characterStatus);
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_CHARACTER_STATUS" : "CONDITION_CHARACTER_STATUS",
                m_Template.characterStatus,
                m_Template.target.ToString()
            );
        }
    }
}