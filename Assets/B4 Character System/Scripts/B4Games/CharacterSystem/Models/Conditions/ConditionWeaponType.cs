using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionWeaponType : Condition
    {
        protected ConditionWeaponType(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionWeaponType Create(ConditionTemplate template)
        {
            return new ConditionWeaponType(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return false;
            }

            Equippable weapon = conditionTarget.equipment.GetCurrentWeapon(m_Template.weaponSlot);

            if (weapon == null)
            {
                return false;
            }

            return m_Template.boolVal ? weapon.GetWeapon().template.type != m_Template.weaponType :
                                        weapon.GetWeapon().template.type == m_Template.weaponType;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create(
                m_Template.boolVal ? "CONDITION_NOT_WEAPON_TYPE" : "CONDITION_WEAPON_TYPE",
                m_Template.weaponType,
                m_Template.weaponSlot,
                m_Template.target.ToString()
            );
        }
    }
}