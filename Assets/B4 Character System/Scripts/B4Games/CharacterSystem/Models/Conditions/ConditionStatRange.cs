using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionStatRange : Condition
    {
        protected ConditionStatRange(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionStatRange Create(ConditionTemplate template)
        {
            return new ConditionStatRange(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            // determine condition target
            Character conditionTarget = GetConditionTarget(actor, target);
            int statValue = conditionTarget.GetStat(m_Template.stat);

            return conditionTarget != null && statValue >= m_Template.intRangeMinVal && statValue <= m_Template.intRangeMaxVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            Character conditionTarget = GetConditionTarget(actor, target);

            if (conditionTarget == null)
            {
                return null;
            }

            return Notification.Create("CONDITION_STAT_RANGE", m_Template.stat, m_Template.intRangeMinVal, m_Template.intRangeMaxVal, m_Template.target.ToString());
        }
    }
}