using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionNeutral : Condition
    {
        protected ConditionNeutral(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionNeutral Create(ConditionTemplate template)
        {
            return new ConditionNeutral(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            if (actor == null || target == null || actor == target)
            {
                return false;
            }

            return m_Template.boolVal ? !actor.IsNeutralWith(actor) : actor.IsNeutralWith(target);
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create(m_Template.boolVal ? "CONDITION_NOT_NEUTRAL" : "CONDITION_NEUTRAL");
        }
    }
}