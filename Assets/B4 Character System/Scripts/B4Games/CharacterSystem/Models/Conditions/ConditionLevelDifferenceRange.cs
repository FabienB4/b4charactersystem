using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionLevelDifferenceRange : Condition
    {
        protected ConditionLevelDifferenceRange(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionLevelDifferenceRange Create(ConditionTemplate template)
        {
            return new ConditionLevelDifferenceRange(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            if (actor == null || target == null)
            {
                return false;
            }

            int levelDifference = actor.level - target.level;

            return levelDifference >= m_Template.intRangeMinVal && levelDifference <= m_Template.intRangeMaxVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create("CONDITION_LEVEL_DIFFERENCE_RANGE", m_Template.intRangeMinVal, m_Template.intRangeMaxVal);
        }
    }
}