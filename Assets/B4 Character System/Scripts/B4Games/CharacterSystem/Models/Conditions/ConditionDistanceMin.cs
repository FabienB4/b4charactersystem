using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions
{
    public class ConditionDistanceMin : Condition
    {
        protected ConditionDistanceMin(ConditionTemplate template) : base(template)
        {
        }

        public new static ConditionDistanceMin Create(ConditionTemplate template)
        {
            return new ConditionDistanceMin(template);
        }

        public override bool TestImplementation(Character actor, Character target, Effect effect, Item item, Ability ability)
        {
            if (actor == null || target == null)
            {
                return false;
            }

            int distance = (int)Vector3.Distance(actor.transform.position, target.transform.position);

            return distance >= m_Template.intVal;
        }

        public override Notification FailureNotification(Character actor, Character target)
        {
            return Notification.Create("CONDITION_DISTANCE_MIN", m_Template.intVal);
        }
    }
}