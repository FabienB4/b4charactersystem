using System;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Effects.Templates;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Conditions.Templates
{
    [Serializable]
    public class ConditionTemplate
    {
        [Tooltip("The target of the condition.\nSome conditions require no target, in these cases, it is just ignored.")]
        public ConditionTarget target;
        public ConditionType type;

        // plain values
        public int intVal;
        public float floatVal;
        public bool boolVal;

        // range values
        public int intRangeMinVal;
        public int intRangeMaxVal;

        // specific
        public Stat stat;
        public CharacterStatus characterStatus;
        public EffectType effectType;
        public ItemTemplate itemTemplate;
        public AbilityTemplate abilityTemplate;
        public EffectTemplate effectTemplate;
        public SummonTemplate summonTemplate;
        public ZoneType zoneType;
        public WeaponSlot weaponSlot;
        public WeaponType weaponType;
        public ArmorPartSlot armorPartSlot;
        public ArmorPartType armorPartType;
    }
}