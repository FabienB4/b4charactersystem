using System.Collections;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Controllers;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.UnityEditorExtended;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors
{
    [RequireComponent(typeof(PlayerController))]
    public class Player : Character
    {
        public new PlayerTemplate template { get { return (PlayerTemplate)m_Template; } }
        public new PlayerController controller { get { return (PlayerController)m_Controller; } }

        private float m_RespawnTime;

        protected override void Start()
        {
            base.Start();

            Init();
        }

        /// <summary>
        /// Factory for Player.
        /// </summary>
        /// <param name="prefab">The prefab to use.</param>
        /// <param name="template">The template to use.</param>
        /// <param name="position">The position of the Player.</param>
        /// <param name="rotation">The rotation of the Player.</param>
        /// <returns>A new Player instance.</returns>
        public static Player Create(GameObject prefab, PlayerTemplate template, Vector3 position, Quaternion rotation)
        {
            if (prefab == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid player prefab.");
#endif

                return null;
            }

            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid player template.");
#endif

                return null;
            }

            GameObject playerGo = Instantiate(prefab, position, rotation) as GameObject;
            playerGo.name = "Player - " + template.characterName;

            // build player
            Player player = playerGo.GetOrAddComponent<Player>();
            player.m_Template = template;

            return player;
        }

        /// <summary>
        /// Initialize Player.
        /// </summary>
        protected override void Init()
        {
            base.Init();

            if (m_Controller == null)
            {
                m_Controller = gameObject.GetOrAddComponent<PlayerController>();
            }
        }

        /// <summary>
        /// Provoke Player's death and start respawn timer.
        /// </summary>
        public override void DoDie()
        {
            base.DoDie();

            m_RespawnTime = Time.time + (Config.Player().respawnDelay > 0 ? Config.Player().respawnDelay : 10.0f);

            StartCoroutine(Respawn());
        }

        /// <summary>
        /// Add ability.
        /// Add active ability to ability bar if possible.
        /// </summary>
        /// <param name="ability">The ability to add.</param>
        public override void AddAbility(Ability ability)
        {
            base.AddAbility(ability);

            if (ability.IsUsable())
            {
                this.controller.TryAddToAbilityBar(ability);
            }
        }

        /// <summary>
        /// Remove ability.
        /// Remove active ability to ability bar if necessary.
        /// </summary>
        /// <param name="ability">The ability to remove.</param>
        public override void RemoveAbility(Ability ability)
        {
            base.RemoveAbility(ability);

            if (ability.IsUsable())
            {
                this.controller.TryRemoveFromAbilityBar(ability);
            }
        }

        /// <summary>
        /// Respawn Player after duration.
        /// TODO END-USER: Add custom logic for your game.
        /// </summary>
        private IEnumerator Respawn()
        {
            while (Time.time < m_RespawnTime)
            {
                yield return null;
            }

            Revive();
        }
    }
}