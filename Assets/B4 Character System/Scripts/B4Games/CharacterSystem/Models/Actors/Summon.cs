using System.Collections;
using B4Games.CharacterSystem.Controllers;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.UnityEditorExtended;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors
{
    public class Summon : Npc
    {
        public new SummonTemplate template { get { return (SummonTemplate)m_Template; } }
        public new SummonController controller { get { return (SummonController)m_Controller; } }

        private Character m_Owner;
        public Character owner { get { return m_Owner; } }

        private float m_Duration;
        public float duration { get { return m_Duration; } }

        private float m_EndDurationTime;
        public float endDurationTime { get { return m_EndDurationTime; } }

        private Coroutine m_DurationTimerCoroutine = null;

        /// <summary>
        /// Shorthand for Summon.Create(template, template.level, position, rotation, owner).
        /// </summary>
        /// <param name="template">The template to use.</param>
        /// <param name="position">The position of the Summon.</param>
        /// <param name="rotation">The rotation of the Summon.</param>
        /// <param name="owner">The owner of the Summon.</param>
        /// <param name="duration">[Optional] The duration of the Summon. If omitted or 0, the summon has no duration.</param>
        /// <returns>A new Summon instance.</returns>
        public static Summon Create(SummonTemplate template, Vector3 position, Quaternion rotation, Character owner, float duration = 0)
        {
            return Summon.Create(template, template.level, position, rotation, owner, duration);
        }

        /// <summary>
        /// Factory for Summon.
        /// </summary>
        /// <param name="template">The template to use.</param>
        /// <param name="level">The level of the Summon.</param>
        /// <param name="position">The position of the Summon.</param>
        /// <param name="rotation">The rotation of the Summon.</param>
        /// <param name="owner">The owner of the Summon.</param>
        /// <param name="duration">[Optional] The duration of the Summon. If omitted or 0, the summon has no duration.</param>
        /// <returns>A new Summon instance.</returns>
        public static Summon Create(SummonTemplate template, int level, Vector3 position, Quaternion rotation, Character owner, float duration = 0)
        {
            if (template.prefab == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid summon prefab.");
#endif

                return null;
            }

            if (template == null || !(template is SummonTemplate))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid summon template.");
#endif

                return null;
            }

            GameObject summonGo = Instantiate(template.prefab, position, rotation) as GameObject;
            summonGo.name = "Summon - " + template.characterName;

            // build summon
            Summon summon = summonGo.GetOrAddComponent<Summon>();
            summon.m_Template = template;
            summon.m_Level = level > 0 ? (level > summon.m_Template.maxLevel ? summon.m_Template.maxLevel : level) : 1;
            summon.m_Owner = owner;
            summon.m_Duration = duration;

            return summon;
        }

        /// <summary>
        /// Initialize Summon.
        /// </summary>
        protected override void Init()
        {
            base.Init();

            StartDurationTimer();
        }

        /// <summary>
        /// Load info (from template or a database).
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        /// <param name="fromTemplate">Load from template.</param>
        public override void LoadInfo(bool fromTemplate = true)
        {
            base.LoadInfo(fromTemplate);

            if (fromTemplate)
            {
                // TODO END-USER: Owner & duration have to be loaded from database, as these are not kept in the template
            }
            else
            {
                // TODO END-USER: Load from database if necessary
            }
        }

        /// <summary>
        /// Provoke Summon's death. Remove the Summon from the game and respawn it if necessary.
        /// </summary>
        public override void DoDie()
        {
            base.DoDie();
            m_Owner.RemoveSummon(this);
        }

        /// <summary>
        /// Shorthand for Follow(m_Owner).
        /// </summary>
        public virtual void FollowOwner()
        {
            Follow(m_Owner);
        }

        /// <summary>
        /// Duration timer task.
        /// </summary>
        private IEnumerator DurationTimer()
        {
            while (Time.time < m_EndDurationTime)
            {
                yield return null;
            }

            OnDurationTimerEnd();
        }

        /// <summary>
        /// Start the duration timer task.
        /// </summary>
        private void StartDurationTimer()
        {
            if (m_DurationTimerCoroutine != null)
            {
                return;
            }

            m_EndDurationTime = m_Duration > 0 ? Time.time + m_Duration : 0;

            if (m_EndDurationTime > 0)
            {
                m_DurationTimerCoroutine = m_Owner.StartCoroutine(DurationTimer());
            }
        }

        /// <summary>
        /// Stop the duration timer task.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopDurationTimer(bool end = true)
        {
            if (m_DurationTimerCoroutine == null)
            {
                return;
            }

            m_Owner.StopCoroutine(m_DurationTimerCoroutine);
            OnDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered when the duration timer task ends or is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnDurationTimerEnd(bool end = true)
        {
            m_DurationTimerCoroutine = null;
            m_EndDurationTime = 0;

            if (end)
            {
                m_Owner.RemoveSummon(this);
                Destroy(this.gameObject);
            }
        }
    }
}