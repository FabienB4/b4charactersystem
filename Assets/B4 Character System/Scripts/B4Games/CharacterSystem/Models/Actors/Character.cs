using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Effects.Templates;
using B4Games.CharacterSystem.Models.ItemContainers;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Items.Templates;
using B4Games.CharacterSystem.Models.Stats;
using B4Games.CharacterSystem.Modifiers.Stats;
using B4Games.CharacterSystem.Stats;
using System;
using B4Games.CharacterSystem.Controllers;
using B4Games.CharacterSystem.Models.Notifications;
using System.Collections.ObjectModel;

namespace B4Games.CharacterSystem.Models.Actors
{
    [RequireComponent(typeof(CapsuleCollider))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Animator))]
    public abstract class Character : MonoBehaviour
    {
        const float BASE_ANIMATION_SPEED = 0.5f;

        protected Guid m_Id;
        public Guid id { get { return m_Id; } }

        [SerializeField]
        protected string m_CharacterName = "";
        public string characterName { get { return m_CharacterName; } }

        [SerializeField]
        protected CharacterTemplate m_Template;
        public CharacterTemplate template { get { return m_Template; } }

        protected CharController m_Controller;
        public CharController controller { get { return m_Controller; } }

        // stats
        protected int m_Level = 1;
        public int level { get { return m_Level; } private set { m_Level = value; } }

        protected int m_Health;
        public int health
        {
            get { return m_Health; }
            private set
            {
                int max = GetStat(Stat.MaxHealth);

                m_Health = value > max ? max : (value < 0 ? 0 : value);

                OnHealthChanged(m_Health);
            }
        }

        protected int m_Energy;
        public int energy
        {
            get { return m_Energy; }
            private set
            {
                int max = GetStat(Stat.MaxEnergy);

                m_Energy = value > max ? max : (value < 0 ? 0 : value);

                OnEnergyChanged(m_Energy);
            }
        }

        protected CharacterStat m_Stat;
        public CharacterStat stat { get { return m_Stat; } }

        protected Inventory m_Inventory;
        public Inventory inventory { get { return m_Inventory; } }

        protected Equipment m_Equipment;
        public Equipment equipment { get { return m_Equipment; } }

        protected Storage m_Storage;
        public Storage storage { get { return m_Storage; } }

        protected List<Ability> m_Abilities = new List<Ability>();
        public ReadOnlyCollection<Ability> abilities { get { return m_Abilities.AsReadOnly(); } }

        protected List<Effect> m_Effects = new List<Effect>();
        public ReadOnlyCollection<Effect> effects { get { return m_Effects.AsReadOnly(); } }

        protected List<StatModifier> m_StatModifiers = new List<StatModifier>();
        public ReadOnlyCollection<StatModifier> statModifiers { get { return m_StatModifiers.AsReadOnly(); } }

        protected List<Summon> m_Summons = new List<Summon>();
        public ReadOnlyCollection<Summon> summons { get { return m_Summons.AsReadOnly(); } }

        protected List<Character> m_Friendlies = new List<Character>();
        public ReadOnlyCollection<Character> friendlies { get { return m_Friendlies.AsReadOnly(); } }

        protected List<Character> m_Enemies = new List<Character>();
        public ReadOnlyCollection<Character> enemies { get { return m_Enemies.AsReadOnly(); } }

        // statuses
        protected CharacterStatus m_Status = CharacterStatus.Idle;
        public CharacterStatus status { get { return m_Status; } }

        protected ZoneType m_ZoneType = ZoneType.Neutral;
        public ZoneType zoneType { get { return m_ZoneType; } }

        // stat driven values
        public float healthRegenerationMultiplier { get { return GetStat(Stat.HealthRegeneration) / 100.0f; } }
        public float energyRegenerationMultiplier { get { return GetStat(Stat.EnergyRegeneration) / 100.0f; } }
        public float speedMultiplier { get { return GetStat(Stat.Speed) / 100.0f; } }
        public float healPowerMultiplier { get { return GetStat(Stat.HealPower) / 100.0f; } }
        public float regenerationPowerMultiplier { get { return GetStat(Stat.RegenerationPower) / 100.0f; } }

        public bool isIdle { get { return m_Status == CharacterStatus.Idle; } }
        public bool isDead { get { return m_Status == CharacterStatus.Dead; } }
        public bool isDisabled { get { return m_Status == CharacterStatus.Disabled; } }

        // coroutines
        protected Coroutine m_HealthRegenerationCoroutine = null;
        protected Coroutine m_EnergyRegenerationCoroutine = null;
        protected Dictionary<Guid, Coroutine> m_RepeatOverTimeCoroutines = new Dictionary<Guid, Coroutine>();
        protected Dictionary<EffectType, Coroutine> m_DisablerCoroutines = new Dictionary<EffectType, Coroutine>();

        // components & calculations
        protected Animator m_Animator;
        protected Rigidbody m_Rigidbody;
        protected CapsuleCollider m_Capsule;
        protected bool m_IsGrounded;
        protected bool m_Crouching;
        protected float m_TurnAmount;
        protected float m_ForwardAmount;
        protected float m_CapsuleHeight;
        protected Vector3 m_CapsuleCenter;
        protected float m_OrigGroundCheckDistance;
        protected Vector3 m_GroundNormal;
        protected float m_GroundCheckDistance;
        protected GameObject m_AggressionGo;
        protected GameObject m_FearGo;
        protected GameObject m_ParalyzeGo;
        protected GameObject m_SleepGo;
        protected GameObject m_SnareGo;
        protected GameObject m_StunGo;

        protected virtual void Start()
        {
            m_Id = Guid.NewGuid();
            m_Controller = GetComponent<CharController>();
            m_Animator = GetComponent<Animator>();
            m_Rigidbody = GetComponent<Rigidbody>();
            m_Capsule = GetComponent<CapsuleCollider>();
            m_CapsuleHeight = m_Capsule.height;
            m_CapsuleCenter = m_Capsule.center;

            m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            m_OrigGroundCheckDistance = m_GroundCheckDistance = Config.Character().groundCheckDistance;
        }

        protected virtual void OnAnimatorMove()
        {
            // we implement this function to override the default root motion.
            // this allows us to modify the positional speed before it's applied.
            if (m_IsGrounded && Time.deltaTime > 0)
            {
                Vector3 v = (m_Animator.deltaPosition * this.speedMultiplier) / Time.deltaTime;
                // we preserve the existing y part of the current velocity.
                v.y = m_Rigidbody.velocity.y;
                m_Rigidbody.velocity = v;
            }
        }

        /// <summary>
        /// Rescale the collider to fit for crouch/stand mode.
        /// </summary>
        /// <param name="crouch">Is the Character crouching?</param>
        protected virtual void ScaleCapsuleForCrouching(bool crouch)
        {
            if (m_IsGrounded && crouch)
            {
                if (m_Crouching)
                {
                    return;
                }

                m_Capsule.height = m_Capsule.height / 2f;
                m_Capsule.center = m_Capsule.center / 2f;
                m_Crouching = true;
            }
            else
            {
                Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * 0.5f, Vector3.up);
                float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * 0.5f;

                if (Physics.SphereCast(crouchRay, m_Capsule.radius * 0.5f, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                {
                    m_Crouching = true;

                    return;
                }

                m_Capsule.height = m_CapsuleHeight;
                m_Capsule.center = m_CapsuleCenter;
                m_Crouching = false;
            }
        }

        /// <summary>
        /// Force crouch mode in low height areas.
        /// </summary>
        protected virtual void PreventStandingInLowHeadroom()
        {
            // prevent standing up in crouch-only zones
            if (!m_Crouching)
            {
                Ray crouchRay = new Ray(m_Rigidbody.position + Vector3.up * m_Capsule.radius * 0.5f, Vector3.up);
                float crouchRayLength = m_CapsuleHeight - m_Capsule.radius * 0.5f;

                if (Physics.SphereCast(crouchRay, m_Capsule.radius * 0.5f, crouchRayLength, Physics.AllLayers, QueryTriggerInteraction.Ignore))
                {
                    m_Crouching = true;
                }
            }
        }

        /// <summary>
        /// Update the animator with current move information.
        /// </summary>
        /// <param name="move">The move vector.</param>
        protected virtual void UpdateAnimator(Vector3 move)
        {
            // update the animator parameters
            m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
            m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
            m_Animator.SetBool("Crouch", m_Crouching);
            m_Animator.SetBool("OnGround", m_IsGrounded);

            if (!m_IsGrounded)
            {
                m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
            }

            // calculate which leg is behind, so as to leave that leg trailing in the jump animation
            // (This code is reliant on the specific run cycle offset in our animations,
            // and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
            float runCycle = Mathf.Repeat(m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_Template.runCycleLegOffset, 1);
            float jumpLeg = (runCycle < 0.5f ? 1 : -1) * m_ForwardAmount;

            if (m_IsGrounded)
            {
                m_Animator.SetFloat("JumpLeg", jumpLeg);
            }

            // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
            // which affects the movement speed because of the root motion.
            if (m_IsGrounded && move.magnitude > 0)
            {
                m_Animator.speed = this.speedMultiplier;
            }
            else
            {
                // don't use that while airborne
                m_Animator.speed = 1;
            }
        }

        /// <summary>
        /// Handle airborne movement.
        /// </summary>
        protected virtual void HandleAirborneMovement()
        {
            // apply extra gravity from multiplier:
            Vector3 extraGravityForce = (Physics.gravity * Config.Character().jumpGravityMultiplier) - Physics.gravity;

            m_Rigidbody.AddForce(extraGravityForce);

            m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
        }

        /// <summary>
        /// Handle grounded movement
        /// </summary>
        /// <param name="crouch">Is the Character crouching?</param>
        /// <param name="jump">Is the Character jumping?</param>
        protected virtual void HandleGroundedMovement(bool crouch, bool jump)
        {
            // check whether conditions are right to allow a jump:
            if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
            {
                // jump!
                m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, Config.Character().jumpPower, m_Rigidbody.velocity.z);
                m_IsGrounded = false;
                m_Animator.applyRootMotion = false;
                m_GroundCheckDistance = 0.1f;
            }
        }

        /// <summary>
        /// Allow to configure the speed at which the Character turns.
        /// </summary>
        protected virtual void ApplyExtraTurnRotation()
        {
            // help the character turn faster (this is in addition to root rotation in the animation)
            float turnSpeed = Mathf.Lerp(Config.Character().stationaryTurnSpeed, Config.Character().movingTurnSpeed, m_ForwardAmount);

            transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
        }

        /// <summary>
        /// Check the ground status of the Character.
        /// </summary>
        protected virtual void CheckGroundStatus()
        {
            RaycastHit hitInfo;

            // 0.1f is a small offset to start the ray from inside the character
            // it is also good to note that the transform position in the sample assets is at the base of the character
            if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, m_GroundCheckDistance))
            {
                m_GroundNormal = hitInfo.normal;
                m_IsGrounded = true;
                m_Animator.applyRootMotion = true;
            }
            else
            {
                m_IsGrounded = false;
                m_GroundNormal = Vector3.up;
                m_Animator.applyRootMotion = false;
            }
        }

        /// <summary>
        /// Triggered by CharacterStat when MaxHealth is changed to prevent current health from being greater, or to start regeneration.
        /// </summary>
        /// <param name="newValue">The new MaxHealth value.</param>
        protected virtual void OnMaxHealthChanged(int newValue)
        {
            if (this.health > newValue)
            {
                this.health = newValue;
            }
            else if (this.health < newValue)
            {
                StartHealthRegeneration();
            }
        }

        /// <summary>
        /// Triggered by CharacterStat when MaxEnergy is changed to prevent current energy from being greater, or to start regeneration.
        /// </summary>
        /// <param name="newValue">The new MaxEnergy value.</param>
        protected virtual void OnMaxEnergyChanged(int newValue)
        {
            if (this.energy > newValue)
            {
                this.energy = newValue;
            }
            else if (this.energy < newValue)
            {
                StartEnergyRegeneration();
            }
        }

        /// <summary>
        /// Initialize Character.
        /// </summary>
        protected virtual void Init()
        {
            // template should already be assigned at this point, this makes sure of it
            if (m_Template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Character instantiated with no CharacterTemplate. Character deactivated.", this);
#endif

                this.gameObject.SetActive(false);
                return;
            }

            LoadInfo();

            m_Stat = CharacterStat.Create(this, m_Template);

            if (m_Stat == null)
            {
                this.gameObject.SetActive(false);
            }

            LoadCurrentStats();
            LoadCurrentInventory();
            LoadCurrentEquipment();
            LoadCurrentAbilities();
        }

        /// <summary>
        /// Load info (from template or a database).
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        /// <param name="fromTemplate">Load from template.</param>
        public virtual void LoadInfo(bool fromTemplate = true)
        {
            if (fromTemplate)
            {
                m_CharacterName = m_Template.characterName;
            }
            else
            {
                // TODO END-USER: Load from database if necessary
            }
        }

        /// <summary>
        /// Load current stats (from template or a database).
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        /// <param name="fromTemplate">Load from template.</param>
        public virtual void LoadCurrentStats(bool fromTemplate = true)
        {
            if (fromTemplate)
            {
                m_Health = m_Template.baseMaxHealth;
                m_Energy = m_Template.baseMaxEnergy;
            }
            else
            {
                // TODO END-USER: Load from database if necessary
            }
        }

        /// <summary>
        /// Load current inventory.
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        public virtual void LoadCurrentInventory()
        {
            m_Inventory = Inventory.Create(this);

            // TODO END-USER: Load from database if necessary
        }

        /// <summary>
        /// Load current equipment (from template or a database).
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        /// <param name="fromTemplate">Load from template.</param>
        public virtual void LoadCurrentEquipment(bool fromTemplate = true)
        {
            m_Equipment = Equipment.Create(this);

            if (fromTemplate)
            {
                Item primaryWeapon = m_Template.primaryWeapon != null ? Item.Create(m_Template.primaryWeapon, this) : null;

                if (primaryWeapon != null && primaryWeapon is Weapon)
                {
                    primaryWeapon.GetWeapon().Equip(this);
                }

                Item secondaryWeapon = m_Template.secondaryWeapon != null ? Item.Create(m_Template.secondaryWeapon, this) : null;

                if (secondaryWeapon != null && secondaryWeapon is Weapon)
                {
                    secondaryWeapon.GetWeapon().Equip(this);
                }

                Item headArmorPart = m_Template.headArmorPart != null ? Item.Create(m_Template.headArmorPart, this) : null;

                if (headArmorPart != null && headArmorPart is ArmorPart)
                {
                    headArmorPart.GetArmorPart().Equip(this);
                }

                Item torsoArmorPart = m_Template.torsoArmorPart != null ? Item.Create(m_Template.torsoArmorPart, this) : null;

                if (torsoArmorPart != null && torsoArmorPart is ArmorPart)
                {
                    torsoArmorPart.GetArmorPart().Equip(this);
                }

                Item handsArmorPart = m_Template.handsArmorPart != null ? Item.Create(m_Template.handsArmorPart, this) : null;

                if (handsArmorPart != null && handsArmorPart is ArmorPart)
                {
                    handsArmorPart.GetArmorPart().Equip(this);
                }

                Item legsArmorPart = m_Template.legsArmorPart != null ? Item.Create(m_Template.legsArmorPart, this) : null;

                if (legsArmorPart != null && legsArmorPart is ArmorPart)
                {
                    legsArmorPart.GetArmorPart().Equip(this);
                }

                Item feetArmorPart = m_Template.feetArmorPart != null ? Item.Create(m_Template.feetArmorPart, this) : null;

                if (feetArmorPart != null && feetArmorPart is ArmorPart)
                {
                    feetArmorPart.GetArmorPart().Equip(this);
                }
            }
            else
            {
                // TODO END-USER: Load from database if necessary
            }

            // make sure equipment has been created properly
            if (m_Equipment == null)
            {
                this.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Load current storage.
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        public virtual void LoadCurrentStorage()
        {
            m_Storage = Storage.Create(this);

            // TODO END-USER: Load from database if necessary
        }

        /// <summary>
        /// Load current abilities (from template or a database).
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        /// <param name="fromTemplate">Load from template.</param>
        public virtual void LoadCurrentAbilities(bool fromTemplate = true)
        {
            if (fromTemplate)
            {
                for (int i = 0; i < m_Template.abilities.Count; i++)
                {
                    AddAbility(Ability.Create(m_Template.abilities[i], this));
                }
            }
            else
            {
                // TODO END-USER: Load from database if necessary
            }
        }

        /// <summary>
        /// Change the level of the Character.
        /// </summary>
        /// <param name="newLevel">The new level.</param>
        public virtual void ChangeLevel(int newLevel)
        {
            if (newLevel < 1 || newLevel == m_Level || newLevel > m_Template.maxLevel)
            {
                return;
            }

            m_Level = newLevel;
        }

        /// <summary>
        /// Handle character movements.
        /// </summary>
        /// <param name="move">The move vector</param>
        /// <param name="crouch">Should the character crouch?</param>
        /// <param name="jump">Should the character jump?</param>
        /// <param name="jump">Is the Character being forced to move?</param>
        public void Move(Vector3 move, bool crouch, bool jump, bool forced)
        {
            // reset move if dead of disabled (except for forced move)
            if (this.isDead || (this.isDisabled && !forced))
            {
                move = Vector3.zero;
                crouch = false;
                jump = false;
            }

            // convert the world relative moveInput vector into a local-relative
            // turn amount and forward amount required to head in the desired
            // direction.
            if (move.magnitude > 1f)
            {
                move.Normalize();
            }

            move = transform.InverseTransformDirection(move);

            CheckGroundStatus();

            move = Vector3.ProjectOnPlane(move, m_GroundNormal);
            m_TurnAmount = Mathf.Atan2(move.x, move.z);
            m_ForwardAmount = move.z;

            ApplyExtraTurnRotation();

            // control and velocity handling is different when grounded and airborne:
            if (m_IsGrounded)
            {
                HandleGroundedMovement(crouch, jump);
            }
            else
            {
                HandleAirborneMovement();
            }

            ScaleCapsuleForCrouching(crouch);
            PreventStandingInLowHeadroom();
            // send input and other state parameters to the animator
            UpdateAnimator(move);
        }

        /// <summary>
        /// Handle character ability casting. [Animation part]
        /// </summary>
        /// <param name="animationName">The name of the casting animation to play</param>
        public virtual void Cast(int castAnimationNum)
        {
            m_Animator.SetInteger("CastNum", castAnimationNum);
        }

        /// <summary>
        /// Provoke Character's death. Does not remove the Character from the game.
        /// </summary>
        public virtual void DoDie()
        {
            if (this.isDead)
            {
                return;
            }

            // stop necessary coroutines
            StopHealthRegeneration();
            StopEnergyRegeneration();
            StopAllRepeatOverTime();
            StopAllDisablers();

            // remove all effects, even passive, will be re-added on resurrection
            RemoveAllEffects();

            // stop movement
            m_Controller.StopMovement();

            if (m_Controller.activeAbility != null)
            {
                m_Controller.activeAbility.CancelUse();
            }

            // reset cast animation
            m_Animator.SetInteger("CastNum", 0);
            m_Animator.SetBool("Dead", true);

            m_Status = CharacterStatus.Dead;
        }

        /// <summary>
        /// Shorthand for Revive(Config.Character().healthPercentOnRevive, Config.Character().energyPercentOnRevive).
        /// </summary>
        public virtual void Revive()
        {
            Revive(Config.Character().healthPercentOnRevive, Config.Character().energyPercentOnRevive);
        }

        /// <summary>
        /// Revive character with specified health and energy values. Do nothing if Character is not dead.
        /// </summary>
        /// <param name="healthPercentOnRevive">The percentage of health the character recovers when revived.</param>
        /// <param name="energyPercentOnRevive">The percentage of energy the character recovers when revived.</param>
        public virtual void Revive(int healthPercentOnRevive, int energyPercentOnRevive)
        {
            if (!this.isDead)
            {
                return;
            }

            m_Status = CharacterStatus.Idle;

            m_Animator.SetBool("Dead", false);

            // automatically reactivates regeneration
            this.health = (int)(GetStat(Stat.MaxHealth) * (healthPercentOnRevive / 100.0f));
            this.energy = (int)(GetStat(Stat.MaxEnergy) * (energyPercentOnRevive / 100.0f));

            // re-add passive effects
            ActivateAllPassiveAbilities();
        }

        /// <summary>
        /// Register a hit from an ability.
        /// </summary>
        /// <param name="actor">The Character who casted the ability.</param>
        /// <param name="ability">The ability to register a hit with.</param>
        public virtual void RegisterHit(Character actor, Ability ability, Vector3 hitPosition)
        {
            if (ability == null)
            {
                return;
            }

            // add effects
            AddAbilityEffects(actor, this, ability, hitPosition);
        }

        /// <summary>
        /// Add ability.
        /// Activate passive ability if necessary.
        /// </summary>
        /// <param name="ability">The ability to add.</param>
        public virtual void AddAbility(Ability ability)
        {
            m_Abilities.Add(ability);

            if (ability.template.type == AbilityType.Passive)
            {
                // add effects
                ActivatePassiveAbility(ability);
            }
        }

        /// <summary>
        /// Remove ability
        /// Deactivate passive ability if necessary.
        /// </summary>
        /// <param name="ability">The ability to remove.</param>
        public virtual void RemoveAbility(Ability ability)
        {
            m_Abilities.Remove(ability);

            if (ability.template.type == AbilityType.Passive)
            {
                // remove effects
                DeactivatePassiveAbility(ability);
            }
        }

        /// <summary>
        /// Remove ability using ability template and level.
        /// </summary>
        /// <param name="abilityTemplateLevel">The ability template level to remove.</param>
        public virtual void RemoveAbility(AbilityTemplateLevel abilityTemplateLevel)
        {
            RemoveAbility(m_Abilities.Find((ability) => ability.template == abilityTemplateLevel.template && ability.level == abilityTemplateLevel.level));
        }

        /// <summary>
        /// Activate passive ability.
        /// </summary>
        /// <param name="ability">The ability to activate.</param>
        public virtual void ActivatePassiveAbility(Ability ability)
        {
            if (ability.template.type != AbilityType.Passive)
            {
                return;
            }

            AddAbilityEffects(this, this, ability);
        }

        /// <summary>
        /// Deactivate passive ability.
        /// </summary>
        /// <param name="ability">The ability to deactivate.</param>
        public virtual void DeactivatePassiveAbility(Ability ability)
        {
            if (ability.template.type != AbilityType.Passive)
            {
                return;
            }

            RemoveAbilityEffects(ability);
        }

        /// <summary>
        /// Activate all passive abilities for Character.
        /// </summary>
        public virtual void ActivateAllPassiveAbilities()
        {
            for (int i = 0; i < m_Abilities.Count; i++)
            {
                Ability ability = m_Abilities[i];

                if (ability.template.type == AbilityType.Passive)
                {
                    ActivatePassiveAbility(ability);
                }
            }
        }

        /// <summary>
        /// Deactivate all passive abilities for Character.
        /// </summary>
        public virtual void DeactivateAllPassiveAbilities()
        {
            for (int i = 0; i < m_Abilities.Count; i++)
            {
                Ability ability = m_Abilities[i];

                if (ability.template.type == AbilityType.Passive)
                {
                    DeactivatePassiveAbility(ability);
                }
            }
        }

        /// <summary>
        /// Add ability effects to Character. Ignore effect if target is invalid.
        /// </summary>
        /// <param name="actor">The Character acting.</param>
        /// <param name="target">The Character receiving the effects.</param>
        /// <param name="ability">The ability to get the effects from.</param>
        /// <param name="position">[Optional] The position associated with the effects, if any.</param>
        public virtual void AddAbilityEffects(Character actor, Character target, Ability ability, Vector3? position = null)
        {
            if (actor == null || target == null || ability == null)
            {
                return;
            }

            for (int i = 0; i < ability.template.effectTemplates.Count; i++)
            {
                EffectTemplate effectTemplate = ability.template.effectTemplates[i];

                if (actor.IsValidTarget(effectTemplate.targetType, target))
                {
                    AddEffect(actor, Effect.Create(effectTemplate, ability.level), position);
                }
                else
                {
                    Notify(Notification.Create("INVALID_TARGET"));
                }
            }
        }

        /// <summary>
        /// Add effect to Character.
        /// </summary>
        /// <param name="actor">The Character who casted the effect.</param>
        /// <param name="effect">The effect to add.</param>
        /// <param name="position">[Optional] The position associated with the effect, if any.</param>
        public virtual void AddEffect(Character actor, Effect effect, Vector3? position = null)
        {
            if (actor == null || effect == null)
            {
                return;
            }

            // process conditions/chances and start duration if any
            if (!effect.Apply(actor, this))
            {
                return;
            }

            // only add effect to the list if lasting
            if (effect.IsLasting())
            {
                m_Effects.Add(effect);
            }

            if (effect.IsDamager())
            {
                int damage = Formulas.CalculateDamage(actor, this, effect);
                // duration and tickCount are both enforced > 0 in Inspector when repeatOverTime = true
                float tickInterval = effect.template.repeatOverTime ? ((effect.duration - 0.1f) / (effect.tickCount - 1)) : 0;

                switch (effect.template.type)
                {
                    case EffectType.Damage:
                        DecreaseHealth(damage);

                        if (tickInterval > 0)
                        {
                            StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => DecreaseHealth(damage));
                        }
                        break;
                    case EffectType.DrainHealth:
                        DecreaseHealth(damage);
                        actor.IncreaseHealth(damage);

                        if (tickInterval > 0)
                        {
                            StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => DecreaseHealth(damage));
                            actor.StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => actor.IncreaseHealth(damage));
                        }
                        break;
                    case EffectType.DrainEnergy:
                        DecreaseEnergy(damage);
                        actor.IncreaseEnergy(damage);

                        if (tickInterval > 0)
                        {
                            StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => DecreaseEnergy(damage));
                            actor.StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => actor.IncreaseEnergy(damage));
                        }
                        break;
                }
            }
            else if (effect.IsRestorer())
            {
                int restoreAmount = Formulas.CalculateRestoreAmount(actor, this, effect);
                // duration and tickCount are both enforced > 0 in Inspector when repeatOverTime = true
                float tickInterval = effect.template.repeatOverTime ? ((effect.duration - 0.1f) / (effect.tickCount - 1)) : 0;

                switch (effect.template.type)
                {
                    case EffectType.RestoreHealth:
                        IncreaseHealth(restoreAmount);

                        if (tickInterval > 0)
                        {
                            StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => IncreaseHealth(restoreAmount));
                        }
                        break;
                    case EffectType.RestoreEnergy:
                        IncreaseEnergy(restoreAmount);

                        if (tickInterval > 0)
                        {
                            StartRepeatOverTime(effect.id, effect.endDurationTime, tickInterval, () => IncreaseEnergy(restoreAmount));
                        }
                        break;
                }
            }
            else if (effect.IsStatChanger())
            {
                AddStatModifier(effect.id, effect.buffStat.stat, effect.buffStat.modifierType, effect.buffStat.order, effect.power, effect.duration);
            }
            else if (effect.IsCleanser())
            {
                switch (effect.template.type)
                {
                    case EffectType.CleanseAll:
                        RemoveDebuffs((int)effect.power);
                        RemoveDisablers((int)effect.power);
                        break;
                    case EffectType.CleanseDebuffs:
                        RemoveDebuffs((int)effect.power);
                        break;
                    case EffectType.CleanseDisablers:
                        RemoveDisablers((int)effect.power);
                        break;
                    case EffectType.CleanseEffectType:
                        RemoveEffectType(effect.cleanseEffectType, (int)effect.power);
                        break;
                }
            }
            else if (effect.IsResist())
            {
            }
            else if (effect.IsDisabler())
            {
                m_Status = CharacterStatus.Disabled;

                StartDisabler(actor, effect);
            }
            else
            {
                // case specific behaviour
                switch (effect.template.type)
                {
                    case EffectType.Resurrection:
                        // dead status is checked in Revive, no need to do it here
                        Revive(effect.resurrectionHealth, effect.resurrectionEnergy);
                        break;
                    case EffectType.TeleportPosition:
                        if (effect.template.targetType == TargetType.Self && position == null)
                        {
                            return;
                        }

                        TeleportToPosition(effect.template.targetType == TargetType.Self ? (Vector3)position : effect.teleportPosition);
                        break;
                    case EffectType.TeleportDirection:
                        Vector3 direction = (effect.template.teleportDirection == TeleportDirection.Backward) ? -this.transform.forward : this.transform.forward;

                        // actor forward is click direction
                        TeleportInDirection(direction * effect.teleportDirectionForce, effect.template.teleportDirectionForceMode);
                        break;
                    case EffectType.Summon:
                        SummonTemplate(effect.summonTemplate.template, effect.summonTemplate.level, effect.duration);
                        break;
                    case EffectType.SummonCharacter:
                        SummonCharacter(actor);
                        break;
                }
            }
        }

        /// <summary>
        /// Cancel an effect. (Does not remove the effect from Character's effect list.)
        /// </summary>
        /// <param name="effect">The effect to cancel.</param>
        public virtual void CancelEffect(Effect effect)
        {
            if (effect == null)
            {
                return;
            }

            if (effect.IsDamager())
            {
                if (effect.template.repeatOverTime)
                {
                    switch (effect.template.type)
                    {
                        case EffectType.Damage:
                            StopRepeatOverTime(effect.id);
                            break;
                        case EffectType.DrainHealth:
                            StopRepeatOverTime(effect.id);
                            // stop actor heal
                            effect.actor.StopRepeatOverTime(effect.id);
                            break;
                        case EffectType.DrainEnergy:
                            StopRepeatOverTime(effect.id);
                            // stop actor heal
                            effect.actor.StopRepeatOverTime(effect.id);
                            break;
                    }
                }
            }
            else if (effect.IsStatChanger())
            {
                RemoveStatModifier(effect.id);
            }
            else if (effect.IsDisabler())
            {
                m_Status = CharacterStatus.Idle;

                StopDisabler(effect.template.type);
            }
        }

        /// <summary>
        /// Remove an effect.
        /// </summary>
        /// <param name="effect">The effect to remove.</param>
        public virtual void RemoveEffect(Effect effect)
        {
            CancelEffect(effect);
            m_Effects.Remove(effect);
        }

        /// <summary>
        /// Remove effects.
        /// </summary>
        /// <param name="effects">The effects to remove.</param>
        /// <param name="maxLevel">The max level to remove. If not specified, remove all levels.</param>
        public virtual void RemoveEffects(List<Effect> effects, int maxLevel = -1)
        {
            if (effects == null)
            {
                return;
            }

            for (int i = 0; i < effects.Count; i++)
            {
                Effect effect = effects[i];

                if (maxLevel < 0 || effect.level <= maxLevel)
                {
                    RemoveEffect(effect);
                }
            }
        }

        /// <summary>
        /// Remove effects from given ability.
        /// </summary>
        /// <param name="ability">The ability to get the effects from.</param>
        public virtual void RemoveAbilityEffects(Ability ability)
        {
            RemoveEffectTemplates(ability.template.effectTemplates, ability.level);
        }

        /// <summary>
        /// Remove effects matching given template.
        /// </summary>
        /// <param name="effectTemplates">Template to remove.</param>
        /// <param name="level">The level of the template.</param>
        public virtual void RemoveEffectTemplate(EffectTemplate effectTemplate, int level)
        {
            if (effectTemplate == null)
            {
                return;
            }

            RemoveEffects(GetEffects(effectTemplate, level));
        }

        /// <summary>
        /// Remove effects matching given templates.
        /// </summary>
        /// <param name="effectTemplates">List of templates to remove.</param>
        /// <param name="level">The level of the templates.</param>
        public virtual void RemoveEffectTemplates(List<EffectTemplate> effectTemplates, int level)
        {
            if (effectTemplates == null)
            {
                return;
            }

            for (int i = 0; i < effectTemplates.Count; i++)
            {
                RemoveEffectTemplate(effectTemplates[i], level);
            }
        }

        /// <summary>
        /// Remove all effects.
        /// </summary>
        public virtual void RemoveAllEffects()
        {
            if (m_Effects == null)
            {
                return;
            }

            for (int i = 0; i < m_Effects.Count; i++)
            {
                Effect effect = m_Effects[i];

                if (effect != null)
                {
                    CancelEffect(effect);
                }
            }

            m_Effects.Clear();
        }

        /// <summary>
        /// Remove debuff effects of given level or inferior.
        /// </summary>
        /// <param name="maxLevel">The max level to remove.</param>
        public virtual void RemoveDebuffs(int maxLevel)
        {
            RemoveEffectType(EffectType.Debuff, maxLevel);
        }

        /// <summary>
        /// Remove disabler effects of given level or inferior.
        /// </summary>
        /// <param name="maxLevel">The max level to remove.</param>
        public virtual void RemoveDisablers(int maxLevel)
        {
            for (int i = 0; i < m_Effects.Count; i++)
            {
                Effect effect = m_Effects[i];

                if (effect.IsDisabler() && effect.level <= maxLevel)
                {
                    RemoveEffect(effect);
                }
            }
        }

        /// <summary>
        /// Remove effects of give type.
        /// </summary>
        /// <param name="effectType">The type of effect to remove.</param>
        /// <param name="maxLevel">The max level to remove.</param>
        public virtual void RemoveEffectType(EffectType effectType, int maxLevel)
        {
            RemoveEffects(GetEffectType(effectType), maxLevel);
        }

        /// <summary>
        /// Add item stats to Character.
        /// </summary>
        /// <param name="item">The item to get the stats from.</param>
        public virtual void AddItemStats(Equippable item)
        {
            if (item == null)
            {
                return;
            }

            if (!item.HasStats())
            {
                return;
            }

            for (int i = 0; i < item.template.stats.Count; i++)
            {
                ItemTemplate.ItemStat stat = item.template.stats[i];

                AddStatModifier(item.id, stat.stat, stat.modifierType, stat.order, stat.power);
            }
        }

        /// <summary>
        /// Remove item stats from Character.
        /// </summary>
        /// <param name="item">The item with the stats to remove.</param>
        public virtual void RemoveItemStats(Equippable item)
        {
            if (item == null)
            {
                return;
            }

            if (!item.HasStats())
            {
                return;
            }

            RemoveStatModifier(item.id);
        }

        /// <summary>
        /// Add item abilities to Character.
        /// </summary>
        /// <param name="item">The item to get the abilities from.</param>
        public virtual void AddItemAbilities(Equippable item)
        {
            if (item == null)
            {
                return;
            }

            if (!item.HasAbilities())
            {
                return;
            }

            for (int i = 0; i < item.template.abilities.Count; i++)
            {
                AddAbility(Ability.Create(item.template.abilities[i], this));
            }
        }

        /// <summary>
        /// Remove item abilities from Character.
        /// </summary>
        /// <param name="item">The item with the abilities to remove.</param>
        public virtual void RemoveItemAbilities(Equippable item)
        {
            if (item == null)
            {
                return;
            }

            if (!item.HasAbilities())
            {
                return;
            }

            for (int i = 0; i < item.template.abilities.Count; i++)
            {
                RemoveAbility(item.template.abilities[i]);
            }
        }

        /// <summary>
        /// Add a stat modifier to Character and update if necessary.
        /// </summary>
        /// <param name="originId">The Guid of the origin of the modifier to add.</param>
        /// <param name="stat">The stat.</param>
        /// <param name="modifierType">The type of modifier.</param>
        /// <param name="order">The order.</param>
        /// <param name="power">The power.</param>
        /// <param name="duration">[Optional] The end time. Default: 0 (infinite)</param>
        /// <param name="calc">[Optional] Whether or not to calculate modifiers when done. Default: true</param>
        public virtual void AddStatModifier(Guid originId, Stat stat, StatModifierType modifierType, int order, float power, float duration = 0, bool calc = true)
        {
            StatModifier statModifier = StatModifier.Create(originId, stat, modifierType, order, power, duration);

            if (statModifier == null)
            {
                return;
            }

            AddStatModifier(statModifier, calc);
        }

        /// <summary>
        /// Add a stat modifier to Character and update if necessary.
        /// </summary>
        /// <param name="statModifier">The stat modifier to add.</param>
        /// <param name="applyModifiers">[Optional] Whether or not to apply modifiers when done. Default: true</param>
        public virtual void AddStatModifier(StatModifier statModifier, bool applyModifiers = true)
        {
            if (statModifier == null)
            {
                return;
            }

            m_StatModifiers.Add(statModifier);

            if (applyModifiers)
            {
                ApplyModifiers(statModifier.stat);
            }
        }

        /// <summary>
        /// Remove a stat modifier from Character, and update if necessary.
        /// </summary>
        /// <param name="origin">The stat modifier to remove.</param>
        /// <param name="applyModifiers">[Optional] Whether or not to apply modifiers when done. Default: true</param>
        public virtual void RemoveStatModifier(StatModifier statModifier, bool applyModifiers = true)
        {
            m_StatModifiers.Remove(statModifier);

            if (applyModifiers)
            {
                ApplyModifiers(statModifier.stat);
            }
        }

        /// <summary>
        /// Remove stat modifiers linked to an origin Guid from Character, and update if necessary.
        /// </summary>
        /// <param name="origin">The origin Guid to remove.</param>
        /// <param name="applyModifiers">[Optional] Whether or not to apply modifiers when done. Default: true</param>
        public virtual void RemoveStatModifier(Guid originId, bool applyModifiers = true)
        {
            List<StatModifier> statModifiers = m_StatModifiers.FindAll((sm) => sm.originId == originId);

            for (int i = 0; i < statModifiers.Count; i++)
            {
                RemoveStatModifier(statModifiers[i], applyModifiers);
            }
        }

        /// <summary>
        /// Apply single modifier to Character.
        /// </summary>
        /// <param name="statModifier">The stat modifier to apply.</param>
        public virtual void ApplyModifier(StatModifier statModifier)
        {
            m_Stat.SetValue(statModifier.stat, statModifier.Apply(this));
        }

        /// <summary>
        /// Apply all modifiers of specified stat of the Character.
        /// </summary>
        /// <param name="stat">The stat.</param>
        public virtual void ApplyModifiers(Stat stat)
        {
            List<StatModifier> singleStatModifiers = m_StatModifiers.FindAll((sm) => sm.stat == stat);

            singleStatModifiers.Sort();

            // make sure base stat value is always added first
            m_Stat.SetValue(stat, GetBaseStat(stat));

            for (int i = 0; i < singleStatModifiers.Count; i++)
            {
                ApplyModifier(singleStatModifiers[i]);
            }

            // triggers
            if (stat == Stat.MaxHealth)
            {
                OnMaxHealthChanged(GetStat(Stat.MaxHealth));
            }
            else if (stat == Stat.MaxEnergy)
            {
                OnMaxEnergyChanged(GetStat(Stat.MaxEnergy));
            }
        }

        /// <summary>
        /// Apply all modifiers of the Character.
        /// </summary>
        public virtual void ApplyAllModifiers()
        {
            ApplyModifiers(Stat.MaxHealth);
            ApplyModifiers(Stat.MaxEnergy);
            ApplyModifiers(Stat.HealthRegeneration);
            ApplyModifiers(Stat.EnergyRegeneration);
            ApplyModifiers(Stat.Attack);
            ApplyModifiers(Stat.Defense);
            ApplyModifiers(Stat.Speed);
            ApplyModifiers(Stat.HealPower);
            ApplyModifiers(Stat.RegenerationPower);
        }

        /// <summary>
        /// Add a friendly Character.
        /// </summary>
        /// <param name="target">The friendly Character.</param>
        public virtual void AddFriendly(Character target)
        {
            if (target == null || this == target)
            {
                return;
            }

            if (m_Friendlies.Contains(target))
            {
                return;
            }

            m_Friendlies.Add(target);
        }

        /// <summary>
        /// Remove a friendly Character.
        /// </summary>
        /// <param name="target">The friendly Character.</param>
        public virtual bool RemoveFriendly(Character target)
        {
            if (target == null || this == target)
            {
                return false;
            }

            return m_Friendlies.Remove(target);
        }

        /// <summary>
        /// Remove all Character classified as friendly.
        /// </summary>
        public virtual void RemoveFriendlies()
        {
            m_Friendlies.Clear();
        }

        /// <summary>
        /// Add an enemy Character.
        /// </summary>
        /// <param name="target">The enemy Character.</param>
        public virtual void AddEnemy(Character target)
        {
            if (target == null || this == target)
            {
                return;
            }

            if (m_Enemies.Contains(target))
            {
                return;
            }

            m_Enemies.Add(target);
        }

        /// <summary>
        /// Remove an enemy Character.
        /// </summary>
        /// <param name="target">The enemy Character.</param>
        public virtual bool RemoveEnemy(Character target)
        {
            if (target == null || this == target)
            {
                return false;
            }

            return m_Enemies.Remove(target);
        }

        /// <summary>
        /// Remove all Character classified as enemy.
        /// </summary>
        public virtual void RemoveEnemies()
        {
            m_Enemies.Clear();
        }

        /// <summary>
        /// Add a summon.
        /// </summary>
        /// <param name="summon">The summon.</param>
        public virtual void AddSummon(Summon summon)
        {
            if (summon == null)
            {
                return;
            }

            if (m_Summons.Contains(summon))
            {
                return;
            }

            // check limit
            if (m_Summons.Count >= m_Template.maxSummonCount)
            {
                return;
            }

            m_Summons.Add(summon);
        }

        /// <summary>
        /// Remove a summon.
        /// </summary>
        /// <param name="summon">The summon.</param>
        public virtual bool RemoveSummon(Summon summon)
        {
            if (summon == null)
            {
                return false;
            }

            // remove summon from world
            Destroy(summon.gameObject);

            return m_Summons.Remove(summon);
        }

        /// <summary>
        /// Increase health by amount.
        /// </summary>
        /// <param name="amount">The amount to increase by.</param>
        public virtual void IncreaseHealth(int amount)
        {
            if (this.isDead || amount == 0)
            {
                return;
            }

            this.health += amount;
        }

        /// <summary>
        /// Shorthand for IncreaseHealth(-amount);
        /// </summary>
        /// <param name="amount">The amount to decrease by.</param>
        public virtual void DecreaseHealth(int amount)
        {
            IncreaseHealth(-amount);
        }

        /// <summary>
        /// Increase energy by amount.
        /// </summary>
        /// <param name="amount">The amount to increase by.</param>
        public virtual void IncreaseEnergy(int amount)
        {
            if (this.isDead || amount == 0)
            {
                return;
            }

            this.energy += amount;
        }

        /// <summary>
        /// Shorthand for IncreaseEnergy(-amount);
        /// </summary>
        /// <param name="amount">The amount to decrease by.</param>
        public virtual void DecreaseEnergy(int amount)
        {
            IncreaseEnergy(-amount);
        }

        /// <summary>
        /// Get current health in percent.
        /// </summary>
        /// <returns>The percentage of health.</returns>
        public int GetHealthPercent()
        {
            int maxHealth = GetStat(Stat.MaxHealth);

            // can't divide by zero
            if (maxHealth == 0)
            {
                return 0;
            }

            return (int)(this.health * 100.0f / maxHealth);
        }

        /// <summary>
        /// Get current energy in percent.
        /// </summary>
        /// <returns>The percentage of energy.</returns>
        public int GetEnergyPercent()
        {
            int maxEnergy = GetStat(Stat.MaxEnergy);

            // can't divide by zero
            if (maxEnergy == 0)
            {
                return 0;
            }

            return (int)(this.energy * 100.0f / maxEnergy);
        }

        /// <summary>
        /// Get the Character current stat.
        /// </summary>
        /// <param name="stat">The stat to get.</param>
        /// <returns>The stat value.</returns>
        public int GetStat(Stat stat)
        {
            return m_Stat.GetValue(stat);
        }

        /// <summary>
        /// Get the Character base stat.
        /// </summary>
        /// <param name="stat">The stat to get.</param>
        /// <returns>The stat value.</returns>
        public int GetBaseStat(Stat stat)
        {
            switch (stat)
            {
                case Stat.MaxHealth:
                    return m_Template.baseMaxHealth;
                case Stat.MaxEnergy:
                    return m_Template.baseMaxEnergy;
                case Stat.HealthRegeneration:
                    return m_Template.baseHealthRegeneration;
                case Stat.EnergyRegeneration:
                    return m_Template.baseEnergyRegeneration;
                case Stat.Attack:
                    return m_Template.baseAttack;
                case Stat.Defense:
                    return m_Template.baseDefense;
                case Stat.Speed:
                    return m_Template.baseSpeed;
                case Stat.HealPower:
                    return m_Template.baseHealPower;
                case Stat.RegenerationPower:
                    return m_Template.baseRegenerationPower;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Teleport Character to given position.
        /// </summary>
        /// <param name="position">The position.</param>
        public virtual void TeleportToPosition(Vector3 position)
        {
            this.transform.position = position;
        }

        /// <summary>
        /// Teleport Character in given direction.
        /// </summary>
        /// <param name="direction">The direction.</param>
        /// <param name="forceMode">The force mode to use.</param>
        public virtual void TeleportInDirection(Vector3 direction, TeleportForceMode forceMode)
        {
            m_Rigidbody.AddForce(direction, (ForceMode)forceMode);
        }

        /// <summary>
        /// Summon a new Summon using given prefab and template. The summon is placed behind the character.
        /// </summary>
        /// <param name="summonTemplate">The Summon template.</param>
        /// <param name="level">[Optional] The level of the Summon. If omitted, the level in the template is used.</param>
        /// <param name="duration">[Optional] The duration of the Summon. If omitted or 0, the summon has no duration.</param>
        public virtual void SummonTemplate(SummonTemplate summonTemplate, int level = -1, float duration = 0)
        {
            if (summonTemplate == null)
            {
                return;
            }

            if (m_Template.maxSummonCount <= 0)
            {
                Notify(Notification.Create("CANNOT_HAVE_SUMMONS"));
                return;
            }

            // check limit
            if (m_Summons.Count >= m_Template.maxSummonCount)
            {
                // remove first summon if limit reached
                if (Config.Character().replaceSummonOnMaxCount)
                {
                    RemoveSummon(m_Summons[0]);
                }
                else
                {
                    Notify(Notification.Create("MAXIMUM_SUMMONS"));
                    return;
                }
            }

            Vector3 adjustedPosition = this.transform.position + (-this.transform.forward * UnityEngine.Random.Range(1.0f, 5.0f));

            AddSummon(Summon.Create(summonTemplate, (level > 0 ? level : summonTemplate.level), adjustedPosition, this.transform.rotation, this, duration));
        }

        /// <summary>
        /// Summon Character to the actor.
        /// </summary>
        /// <param name="actor">The actor to summon to.</param>
        public virtual void SummonCharacter(Character actor)
        {
            TeleportToPosition(actor.transform.position);
            this.transform.LookAt(actor.transform);
        }

        /// <summary>
        /// Follow the specified target.
        /// </summary>
        /// <param name="target">The target to follow.</param>
        public void Follow(Character target)
        {
            m_Controller.SetMovement(target.controller.move);

            // teleport if distance becomes too great (avoid stuck summons)
            if (Vector3.Distance(this.transform.position, target.transform.position) > Config.Character().maxSummonDistanceFromOwner)
            {
                this.transform.position = target.transform.position + (-target.transform.forward * UnityEngine.Random.Range(1.0f, 5.0f));
            }
        }

        /// <summary>
        /// Rotate character to look at the cast direction.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public void LookAtCastDirection(Vector3 direction)
        {
            // overwrite y to avoid weird character rotation
            this.transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0.0f, direction.z));
        }

        /// <summary>
        /// Notify the character of an event.
        /// TODO END-USER: Implement me
        /// </summary>
        /// <param name="message">The message</param>
        public void Notify(string message)
        {
#if UNITY_EDITOR
            Debug.Log("NOTIFY: " + message);
#endif

            // TODO END-USER: implement me
        }

        /// <summary>
        /// Shorthand for Notify(notification.message).
        /// </summary>
        /// <param name="notification"></param>
        public void Notify(Notification notification)
        {
            Notify(notification.message);
        }

        /// <summary>
        /// Check if Character has ability of specified template at given level.
        /// </summary>
        /// <param name="abilityTemplate">The ability template to check.</param>
        /// <param name="level">[Optional] The level of the ability. If ommitted, retrieve first of any level.</param>
        /// <returns>True if affected by ability template.</returns>
        public virtual bool HasAbility(AbilityTemplate abilityTemplate, int level = -1)
        {
            return m_Abilities.Find((ability) => ability.template == abilityTemplate && (level < 0 || ability.level == level)) != null;
        }

        /// <summary>
        /// Check if Character is affected by effect of specified template at given level.
        /// </summary>
        /// <param name="effectTemplate">The effect template to check.</param>
        /// <param name="level">[Optional] The level of the effect. If ommitted, retrieve first of any level.</param>
        /// <returns>True if affected by effect template.</returns>
        public virtual bool HasEffect(EffectTemplate effectTemplate, int level = -1)
        {
            // won't find it if not lasting, avoid looping through effects for nothing
            if (!effectTemplate.IsLasting())
            {
                return false;
            }

            return m_Effects.Find((effect) => effect.template == effectTemplate && (level < 0 || effect.level == level)) != null;
        }

        /// <summary>
        /// Get Character effects of specified template and level.
        /// </summary>
        /// <param name="effectTemplate">The effect template to get.</param>
        /// <param name="level">[Optional] The level of the effect. If ommitted, retrieve all levels.</param>
        /// <returns>List of effects.</returns>
        public List<Effect> GetEffects(EffectTemplate effectTemplate, int level = -1)
        {
            // won't find it if not lasting, avoid looping through effects for nothing
            if (!effectTemplate.IsLasting())
            {
                return null;
            }

            return m_Effects.FindAll((effect) => effect.template == effectTemplate && (level < 0 || effect.level == level));
        }

        /// <summary>
        /// Check if Character is affected by effect of specified type.
        /// </summary>
        /// <param name="effectType">The effect type to check.</param>
        /// <param name="level">[Optional] The level of the effect. If ommitted, retrieve first of any level.</param>
        /// <returns>True if affected by effect type.</returns>
        public virtual bool HasEffectType(EffectType effectType, int level = -1)
        {
            return m_Effects.Find((effect) => effect.template.type == effectType && (level < 0 || effect.level == level)) != null;
        }

        /// <summary>
        /// Get Character effects of specified type.
        /// </summary>
        /// <param name="effectType">The effect type to get.</param>
        /// <param name="level">[Optional] The level of the effect. If ommitted, retrieve all levels.</param>
        /// <returns>List of effects.</returns>
        public List<Effect> GetEffectType(EffectType effectType, int level = -1)
        {
            return m_Effects.FindAll((effect) => effect.template.type == effectType && (level < 0 || effect.level == level));
        }

        /// <summary>
        /// Check if character is not unable to act because of a status or an effect.
        /// </summary>
        /// <returns>True if character can act.</returns>
        public virtual bool CanAct()
        {
            return !(this.isDead || this.isDisabled);
        }

        /// <summary>
        /// Check if target is attackable.
        /// </summary>
        /// <returns>True if attackable.</returns>
        public virtual bool IsAttackable()
        {
            return true;
        }

        /// <summary>
        /// Check if given target is valid.
        /// </summary>
        /// <param name="targetType">The target type used to check.</param>
        /// <param name="target">The target to check.</param>
        /// <returns>True if target is valid.</returns>
        public virtual bool IsValidTarget(TargetType targetType, Character target)
        {
            if (target == null)
            {
                return false;
            }

            switch (targetType)
            {
                case TargetType.None:
                    return target == this;
                case TargetType.Self:
                    return target == this;
                case TargetType.Any:
                    return true;
                case TargetType.NotSelf:
                    return target != this;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Check if Character is neutral (not friendly and not enemy) with specified target.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns>True if Characters are neutral.</returns>
        public virtual bool IsNeutralWith(Character target)
        {
            return !IsFriendlyWith(target) && !IsEnemyWith(target);
        }

        /// <summary>
        /// Check if Character is friendly with specified target.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns>True if Characters are friendly.</returns>
        public virtual bool IsFriendlyWith(Character target)
        {
            return m_Friendlies.Find((friendly) => friendly == target) != null;
        }

        /// <summary>
        /// Check if Character is enemy with specified target.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns>True if Characters are enemy.</returns>
        public virtual bool IsEnemyWith(Character target)
        {
            return m_Enemies.Find((enemy) => enemy == target) != null;
        }

        /// <summary>
        /// Determines if character is behind the specified target within a 45 degrees angle.
        /// </summary>
        /// <param name="target">Character - The target</param>
        /// <returns>True if the character is behind the target within a 45 degrees angle.</returns>
        public virtual bool IsBehind(Character target)
        {
            if (target == null)
            {
                return false;
            }

            return Vector3.Angle(transform.forward, target.transform.forward) <= 45.0f;
        }

        /// <summary>
        /// Determines if character is facing the specified target within a 45 degrees angle.
        /// </summary>
        /// <param name="target">Character - The target</param>
        /// <returns>True if the character is facing the target within a 45 degrees angle.</returns>
        public virtual bool IsFacing(Character target)
        {
            if (target == null)
            {
                return false;
            }

            return Vector3.Angle(-transform.forward, target.transform.forward) <= 45.0f;
        }

        /// <summary>
        /// Check if Character has item of specified template in inventory.
        /// </summary>
        /// <param name="itemTemplate">The item template to check.</param>
        /// <returns>True if Character has item template.</returns>
        public virtual bool HasItem(ItemTemplate itemTemplate)
        {
            return m_Inventory.Find((i) => i.template == itemTemplate) != null;
        }

        /// <summary>
        /// Check if Character has summon of specified template.
        /// </summary>
        /// <param name="template">The summon template to check.</param>
        /// <param name="level">[Optional] The level of the summon. If ommitted, retrieve all levels.</param>
        /// <returns></returns>
        public virtual bool HasSummon(SummonTemplate template, int level = -1)
        {
            return m_Summons.Find((summon) => summon.m_Template == template && (level < 0 || summon.level == level));
        }

        /// <summary>
        /// Check if Character has summons.
        /// </summary>
        /// <returns>True if at least one summon.</returns>
        public virtual bool HasSummons()
        {
            return m_Summons.Count > 0;
        }

        /// <summary>
        /// Event triggered on health change.
        /// </summary>
        /// <param name="newHealth">The new health value</param>
        private void OnHealthChanged(int newHealth)
        {
            if (newHealth <= 0)
            {
                DoDie();
                return;
            }

            // make sure not to restart regen after death or when full
            if (newHealth > 0 && newHealth < GetStat(Stat.MaxHealth))
            {
                StartHealthRegeneration();
            }

            if (newHealth == GetStat(Stat.MaxHealth))
            {
                StopHealthRegeneration();
            }
        }

        /// <summary>
        /// Event triggered on energy change.
        /// </summary>
        /// <param name="newEnergy">The new energy value</param>
        private void OnEnergyChanged(int newEnergy)
        {
            if (newEnergy < GetStat(Stat.MaxEnergy))
            {
                StartEnergyRegeneration();
            }

            if (newEnergy == GetStat(Stat.MaxEnergy))
            {
                StopEnergyRegeneration();
            }
        }

        /// <summary>
        /// Health regeneration task.
        /// </summary>
        /// <returns></returns>
        private IEnumerator HealthRegeneration()
        {
            float tickInterval = 1.0f / Config.Character().healthRegeneratonTicksPerSecond;
            float nextTickTime = Time.time + tickInterval;

            while (true)
            {
                if (Time.time >= nextTickTime)
                {
                    this.health += (int)(1.0f * this.healthRegenerationMultiplier);
                    nextTickTime += tickInterval;
                }

                yield return null;
            }
        }

        /// <summary>
        /// Start the health regeneration task.
        /// </summary>
        private void StartHealthRegeneration()
        {
            if (this.isDead || m_HealthRegenerationCoroutine != null)
            {
                return;
            }

            // only start regeneration if there is something to regenerate, and not dead
            if (m_Health > 0 && m_Health < GetStat(Stat.MaxHealth))
            {
                m_HealthRegenerationCoroutine = StartCoroutine(HealthRegeneration());
            }
        }

        /// <summary>
        /// Stop the health regeneration task.
        /// </summary>
        private void StopHealthRegeneration()
        {
            if (m_HealthRegenerationCoroutine == null)
            {
                return;
            }

            StopCoroutine(m_HealthRegenerationCoroutine);

            m_HealthRegenerationCoroutine = null;
        }

        /// <summary>
        /// Energy regeneration task.
        /// </summary>
        /// <returns></returns>
        private IEnumerator EnergyRegeneration()
        {
            float tickInterval = 1.0f / Config.Character().energyRegeneratonTicksPerSecond;
            float nextTickTime = Time.time + tickInterval;

            while (true)
            {
                if (Time.time >= nextTickTime)
                {
                    this.energy += (int)(1.0f * this.energyRegenerationMultiplier);
                    nextTickTime += tickInterval;
                }

                yield return null;
            }
        }

        /// <summary>
        /// Start the energy regeneration task.
        /// </summary>
        private void StartEnergyRegeneration()
        {
            if (this.isDead || m_EnergyRegenerationCoroutine != null)
            {
                return;
            }

            // only start regeneration if there is something to regenerate
            if (m_Energy < GetStat(Stat.MaxEnergy))
            {
                m_EnergyRegenerationCoroutine = StartCoroutine(EnergyRegeneration());
            }
        }

        /// <summary>
        /// Stop the energy regeneration task.
        /// </summary>
        private void StopEnergyRegeneration()
        {
            if (m_EnergyRegenerationCoroutine == null)
            {
                return;
            }

            StopCoroutine(m_EnergyRegenerationCoroutine);

            m_EnergyRegenerationCoroutine = null;
        }

        /// <summary>
        /// Over time repeat task.
        /// </summary>
        /// <param name="originId">The ID of the object that started the task.</param>
        /// <param name="endDurationTime">The end of the repetition.</param>
        /// <param name="tickInterval">The interval between each repetition.</param>
        /// <param name="funcToRepeat">The function to repeat.</param>
        private IEnumerator RepeatOverTime(Guid originId, float endDurationTime, float tickInterval, Action funcToRepeat)
        {
            float nextTickTime = Time.time + tickInterval;

            while (Time.time < endDurationTime)
            {
                if (Time.time >= nextTickTime)
                {
                    if (funcToRepeat != null)
                    {
                        funcToRepeat();
                    }

                    nextTickTime += tickInterval;
                }

                yield return null;
            }

            OnRepeatOverTimeEnd(originId);
        }

        /// <summary>
        /// Start an over time repeat task.
        /// </summary>
        /// <param name="originId">The ID of the object that is starting the task.</param>
        /// <param name="endDurationTime">The end of the repetition.</param>
        /// <param name="tickInterval">The interval between each repetition.</param>
        /// <param name="funcToRepeat">The function to repeat.</param>
        /// <returns></returns>
        private void StartRepeatOverTime(Guid originId, float endDurationTime, float tickInterval, Action funcToRepeat)
        {
            if (endDurationTime <= 0 || tickInterval <= 0 || funcToRepeat == null)
            {
                return;
            }

            m_RepeatOverTimeCoroutines.Add(originId, StartCoroutine(RepeatOverTime(originId, endDurationTime, tickInterval, funcToRepeat)));
        }

        /// <summary>
        /// Stop an over time repeat task.
        /// </summary>
        /// <param name="originId">The ID of the object that started the task.</param>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopRepeatOverTime(Guid originId, bool end = true)
        {
            Coroutine coroutine;

            if (m_RepeatOverTimeCoroutines.TryGetValue(originId, out coroutine))
            {
                StopCoroutine(coroutine);
                OnRepeatOverTimeEnd(originId, end);
            }
        }

        /// <summary>
        /// Stop all over time repeat tasks.
        /// </summary>
        private void StopAllRepeatOverTime()
        {
            foreach (KeyValuePair<Guid, Coroutine> kvp in m_RepeatOverTimeCoroutines)
            {
                StopCoroutine(kvp.Value);
            }

            m_RepeatOverTimeCoroutines.Clear();
        }

        /// <summary>
        /// Triggered when the repeat over time task ends or is stopped.
        /// </summary>
        /// <param name="originId">The ID of the object that started the task.</param>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnRepeatOverTimeEnd(Guid originId, bool end = true)
        {
            if (end)
            {
            }

            m_RepeatOverTimeCoroutines.Remove(originId);
        }

        /// <summary>
        /// Disabler task.
        /// </summary>
        /// <param name="actor">The actor responsible for the disabler.</param>
        /// <param name="effect">The effect.</param>
        private IEnumerator Disabler(Character actor, Effect effect)
        {
            EffectType disablerType = effect.template.type;

            StartDisablerAnimation(disablerType);

            while (Time.time < effect.endDurationTime)
            {
                switch (disablerType)
                {
                    case EffectType.Fear:
                        if (actor == null)
                        {
                            break;
                        }

                        // force target to moving away from actor
                        m_Controller.SetForcedMovement(actor.transform.forward);
                        break;
                    case EffectType.Paralyze:
                        DisableMovements();
                        DisableAnimations();
                        break;
                    case EffectType.Sleep:
                    case EffectType.Snare:
                    case EffectType.Stun:
                        DisableMovements();
                        break;
                    case EffectType.Aggression:
                        if (actor == null)
                        {
                            break;
                        }

                        // force target to look at actor
                        transform.LookAt(actor.transform);
                        break;
                }

                yield return null;
            }

            OnDisablerEnd(disablerType);
        }

        /// <summary>
        /// Start a disabler task.
        /// </summary>
        /// <param name="actor">The actor responsible for the disabler.</param>
        /// <param name="effect">The effect.</param>
        private void StartDisabler(Character actor, Effect effect)
        {
            if (!effect.IsDisabler() || effect.endDurationTime <= 0)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid disabler", this);
#endif

                return;
            }

            // stop current if any
            StopDisabler(effect.template.type);
            m_DisablerCoroutines.Add(effect.template.type, StartCoroutine(Disabler(actor, effect)));
        }

        /// <summary>
        /// Stop a disable task.
        /// </summary>
        /// <param name="disablerType">The type of the disabler.</param>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopDisabler(EffectType disablerType, bool end = true)
        {
            Coroutine coroutine;

            if (m_DisablerCoroutines.TryGetValue(disablerType, out coroutine) && coroutine != null)
            {
                StopCoroutine(coroutine);
                OnDisablerEnd(disablerType, end);
            }
        }

        /// <summary>
        /// Stop all disabler tasks.
        /// </summary>
        private void StopAllDisablers()
        {
            foreach (KeyValuePair<EffectType, Coroutine> kvp in m_DisablerCoroutines)
            {
                if (kvp.Value != null)
                {
                    StopCoroutine(kvp.Value);
                    OnDisablerEnd(kvp.Key);
                }
            }

            m_DisablerCoroutines.Clear();
        }

        /// <summary>
        /// Triggered when the disabler task ends or is stopped.
        /// </summary>
        /// <param name="disablerType">The disabler type that is ending.</param>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnDisablerEnd(EffectType disablerType, bool end = true)
        {
            if (end)
            {
                if (m_DisablerCoroutines.ContainsKey(disablerType))
                {
                    StopDisablerAnimation(disablerType);

                    switch (disablerType)
                    {
                        case EffectType.Fear:
                            // stop movements
                            m_Controller.StopMovement();
                            break;
                        case EffectType.Paralyze:
                            EnableMovements();
                            EnableAnimations();
                            break;
                        case EffectType.Sleep:
                        case EffectType.Snare:
                        case EffectType.Stun:
                            EnableMovements();
                            break;
                        case EffectType.Aggression:
                            // attraction is stopped automatically
                            break;
                    }
                }
            }

            m_DisablerCoroutines.Remove(disablerType);
        }

        /// <summary>
        /// Enable Character animations.
        /// </summary>
        private void EnableAnimations()
        {
            if (!m_Animator.enabled)
            {
                m_Animator.enabled = true;
            }
        }

        /// <summary>
        /// Disable Character animations.
        /// </summary>
        private void DisableAnimations()
        {
            if (m_Animator.enabled)
            {
                m_Animator.enabled = false;
            }
        }

        /// <summary>
        /// Enable Character movements.
        /// </summary>
        private void EnableMovements()
        {
            if (m_Rigidbody.constraints == RigidbodyConstraints.FreezeAll)
            {
                m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            }
        }

        /// <summary>
        /// Disable Character movements.
        /// </summary>
        private void DisableMovements()
        {
            m_Controller.StopMovement();

            if (m_Rigidbody.constraints == RigidbodyConstraints.FreezeRotation)
            {
                m_Rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            }
        }

        /// <summary>
        /// Start a disabler animation.
        /// </summary>
        /// <param name="type">The type of disabler.</param>
        private void StartDisablerAnimation(EffectType type)
        {
            switch (type)
            {
                case EffectType.Aggression:
                    GameObject aggressionAnimation = Config.Ability().aggressionAnimation;

                    if (aggressionAnimation != null)
                    {
                        Vector3 aggressionAnimationPosition = this.transform.position;

                        switch (Config.Ability().aggressionAnimationPosition)
                        {
                            case DisablerAnimationPosition.Middle:
                                aggressionAnimationPosition.y += m_CapsuleHeight / 2;
                                break;
                            case DisablerAnimationPosition.Head:
                                aggressionAnimationPosition.y += m_CapsuleHeight;
                                break;
                        }

                        m_AggressionGo = Instantiate(aggressionAnimation, aggressionAnimationPosition, this.transform.rotation, this.transform) as GameObject;
                    }
                    break;
                case EffectType.Fear:
                    GameObject fearAnimation = Config.Ability().fearAnimation;

                    if (fearAnimation != null)
                    {
                        Vector3 fearAnimationPosition = this.transform.position;

                        switch (Config.Ability().fearAnimationPosition)
                        {
                            case DisablerAnimationPosition.Middle:
                                fearAnimationPosition.y += m_CapsuleHeight / 2;
                                break;
                            case DisablerAnimationPosition.Head:
                                fearAnimationPosition.y += m_CapsuleHeight;
                                break;
                        }

                        m_FearGo = Instantiate(fearAnimation, fearAnimationPosition, this.transform.rotation, this.transform) as GameObject;
                    }
                    break;
                case EffectType.Paralyze:
                    GameObject paralyzeAnimation = Config.Ability().paralyzeAnimation;

                    if (paralyzeAnimation != null)
                    {
                        Vector3 paralyzeAnimationPosition = this.transform.position;

                        switch (Config.Ability().paralyzeAnimationPosition)
                        {
                            case DisablerAnimationPosition.Middle:
                                paralyzeAnimationPosition.y += m_CapsuleHeight / 2;
                                break;
                            case DisablerAnimationPosition.Head:
                                paralyzeAnimationPosition.y += m_CapsuleHeight;
                                break;
                        }

                        m_ParalyzeGo = Instantiate(paralyzeAnimation, paralyzeAnimationPosition, this.transform.rotation, this.transform) as GameObject;
                    }
                    break;
                case EffectType.Sleep:
                    GameObject sleepAnimation = Config.Ability().sleepAnimation;

                    if (sleepAnimation != null)
                    {
                        Vector3 sleepAnimationPosition = this.transform.position;

                        switch (Config.Ability().sleepAnimationPosition)
                        {
                            case DisablerAnimationPosition.Middle:
                                sleepAnimationPosition.y += m_CapsuleHeight / 2;
                                break;
                            case DisablerAnimationPosition.Head:
                                sleepAnimationPosition.y += m_CapsuleHeight;
                                break;
                        }

                        m_SleepGo = Instantiate(sleepAnimation, sleepAnimationPosition, this.transform.rotation, this.transform) as GameObject;
                    }
                    break;
                case EffectType.Snare:
                    GameObject snareAnimation = Config.Ability().snareAnimation;

                    if (snareAnimation != null)
                    {
                        Vector3 snareAnimationPosition = this.transform.position;

                        switch (Config.Ability().snareAnimationPosition)
                        {
                            case DisablerAnimationPosition.Middle:
                                snareAnimationPosition.y += m_CapsuleHeight / 2;
                                break;
                            case DisablerAnimationPosition.Head:
                                snareAnimationPosition.y += m_CapsuleHeight;
                                break;
                        }

                        m_SnareGo = Instantiate(snareAnimation, snareAnimationPosition, this.transform.rotation, this.transform) as GameObject;
                    }
                    break;
                case EffectType.Stun:
                    GameObject stunAnimation = Config.Ability().stunAnimation;

                    if (stunAnimation != null)
                    {
                        Vector3 stunAnimationPosition = this.transform.position;

                        switch (Config.Ability().stunAnimationPosition)
                        {
                            case DisablerAnimationPosition.Middle:
                                stunAnimationPosition.y += m_CapsuleHeight / 2;
                                break;
                            case DisablerAnimationPosition.Head:
                                stunAnimationPosition.y += m_CapsuleHeight;
                                break;
                        }

                        m_StunGo = Instantiate(stunAnimation, stunAnimationPosition, this.transform.rotation, this.transform) as GameObject;
                    }
                    break;
                default:
                    return;
            }
        }

        /// <summary>
        /// Stop a disabler animation.
        /// </summary>
        /// <param name="type">The type of disabler.</param>
        private void StopDisablerAnimation(EffectType type)
        {
            switch (type)
            {
                case EffectType.Aggression:
                    Destroy(m_AggressionGo);
                    break;
                case EffectType.Fear:
                    Destroy(m_FearGo);
                    break;
                case EffectType.Paralyze:
                    Destroy(m_ParalyzeGo);
                    break;
                case EffectType.Sleep:
                    Destroy(m_SleepGo);
                    break;
                case EffectType.Snare:
                    Destroy(m_SnareGo);
                    break;
                case EffectType.Stun:
                    Destroy(m_StunGo);
                    break;
                default:
                    return;
            }
        }
    }
}