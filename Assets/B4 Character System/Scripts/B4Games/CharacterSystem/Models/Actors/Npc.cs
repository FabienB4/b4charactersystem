using B4Games.CharacterSystem.Controllers;
using B4Games.CharacterSystem.Managers;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.UnityEditorExtended;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors
{
    public class Npc : Character
    {
        public new NpcTemplate template { get { return (NpcTemplate)m_Template; } }
        public new NpcController controller { get { return (NpcController)m_Controller; } }

        protected override void Start()
        {
            base.Start();

            Init();
        }

        /// <summary>
        /// Shorthand for Npc.Create(template, template.level, template.spawnPosition, Quaternion.identity).
        /// </summary>
        /// <param name="template">The template to use.</param>
        /// <returns>A new Npc instance.</returns>
        public static Npc Create(NpcTemplate template)
        {
            return Npc.Create(template, template.level, template.spawnPosition, Quaternion.identity);
        }

        /// <summary>
        /// Factory for Npc.
        /// </summary>
        /// <param name="template">The template to use.</param>
        /// <param name="level">The level of the npc.</param>
        /// <param name="position">The position of the Npc.</param>
        /// <param name="rotation">The rotation of the Npc.</param>
        /// <returns>A new Npc instance.</returns>
        public static Npc Create(NpcTemplate template, int level, Vector3 position, Quaternion rotation)
        {
            if (template.prefab == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid npc prefab.");
#endif

                return null;
            }

            if (template == null || template is SummonTemplate)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid npc template.");
#endif

                return null;
            }

            GameObject npcGo = Instantiate(template.prefab, position, rotation) as GameObject;
            npcGo.name = "Npc - " + template.characterName;

            // build npc
            Npc npc = npcGo.GetOrAddComponent<Npc>();
            npc.m_Template = template;
            npc.m_Level = level > 0 ? (level > npc.m_Template.maxLevel ? npc.m_Template.maxLevel : level) : 1;

            return npc;
        }

        /// <summary>
        /// Initialize Npc.
        /// </summary>
        protected override void Init()
        {
            base.Init();

            if (m_Controller == null)
            {
                if (m_Template is SummonTemplate)
                {
                    m_Controller = gameObject.GetOrAddComponent<SummonController>();
                }
                else
                {
                    m_Controller = gameObject.GetOrAddComponent<NpcController>();
                }
            }
        }

        /// <summary>
        /// Load info (from template or a database).
        /// TODO END-USER: Implement database object loading if needed.
        /// </summary>
        /// <param name="fromTemplate">Load from template.</param>
        public override void LoadInfo(bool fromTemplate = true)
        {
            base.LoadInfo(fromTemplate);

            if (fromTemplate)
            {
            }
            else
            {
                // TODO END-USER: Load from database if necessary
            }
        }

        /// <summary>
        /// Provoke Npc's death. Remove the Npc from the game and respawn it if necessary.
        /// </summary>
        public override void DoDie()
        {
            base.DoDie();

            if (this.template.respawnOnDeath)
            {
                SpawnManager.AddSpawn(new SpawnManager.SpawnInfo(this.template, m_Level, this.transform.position, this.transform.rotation), this.template.respawnDelay);
            }

            Destroy(this.gameObject);
        }

        /// <summary>
        /// Set the spawn position of the Npc.
        /// </summary>
        /// <param name="newPosition">The position to set.</param>
        public virtual void SetSpawnPosition(Vector3 newPosition)
        {
            this.transform.position = newPosition;
        }

        /// <summary>
        /// Check if target is attackable.
        /// </summary>
        /// <returns>True if attackable.</returns>
        public override bool IsAttackable()
        {
            return this.template.attackable;
        }
    }
}