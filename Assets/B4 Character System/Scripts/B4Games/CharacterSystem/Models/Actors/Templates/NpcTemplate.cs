using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Characters/Npc Template")]
    public class NpcTemplate : CharacterTemplate
    {
        [Header("Npc Specific")]
        public GameObject prefab;
        public int level = 1;
        public bool spawnOnStart;
        public Vector3 spawnPosition;
        public bool attackable = false;
        public bool respawnOnDeath = true;
        public float respawnDelay;
    }
}