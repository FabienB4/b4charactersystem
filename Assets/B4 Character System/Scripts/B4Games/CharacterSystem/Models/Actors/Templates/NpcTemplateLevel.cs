using System;

namespace B4Games.CharacterSystem.Models.Actors.Templates
{
    [Serializable]
    public class NpcTemplateLevel
    {
        public NpcTemplate template;
        public int level = 1;
    }
}