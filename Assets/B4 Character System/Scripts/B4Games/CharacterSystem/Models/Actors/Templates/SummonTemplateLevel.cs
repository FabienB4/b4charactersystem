using System;

namespace B4Games.CharacterSystem.Models.Actors.Templates
{
    [Serializable]
    public class SummonTemplateLevel
    {
        public SummonTemplate template;
        public int level = 1;
    }
}