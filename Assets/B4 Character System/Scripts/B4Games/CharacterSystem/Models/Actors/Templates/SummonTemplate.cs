using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Characters/Summon Template")]
    public class SummonTemplate : NpcTemplate
    {
        //[Header("Summon Specific")]
    }
}