using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Characters/Player Template")]
    public class PlayerTemplate : CharacterTemplate
    {
        //[Header("Player Specific")]
    }
}