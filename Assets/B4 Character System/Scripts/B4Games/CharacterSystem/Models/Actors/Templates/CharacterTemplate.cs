using System.Collections.Generic;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Actors.Templates
{
    public class CharacterTemplate : ScriptableObject
    {
        public string characterName;

        [Header("Base Stats")]
        public int baseMaxHealth;
        public int baseMaxEnergy;
        [Tooltip("This value is a percentage (100 = 1 health per tick).\n> 100 = more health recovered per tick\n< 100 = less health recovered per tick")]
        public int baseHealthRegeneration;
        [Tooltip("This value is a percentage (100 = 1 energy per tick).\n> 100 = more energy recovered per tick\n< 100 = less energy recovered per tick")]
        public int baseEnergyRegeneration;
        public int baseAttack;
        public int baseDefense;
        [Tooltip("This value is a percentage (100 = 100%).\n> 100 = faster\n< 100 = slower\nAffect run speed, cast speed and any animations in character's animator.")]
        public int baseSpeed = 100;
        [Tooltip("This value is a percentage (100 = 100%).\n> 100 = casted heal are more powerful\n< 100 = caster heal are less powerful")]
        public int baseHealPower = 100;
        [Tooltip("This value is a percentage (100 = 100%).\n> 100 = received heal are more powerful\n< 100 = received heal are less powerful")]
        public int baseRegenerationPower = 100;

        [Header("Limits")]
        [Tooltip("The maximum level this character can reach.")]
        public int maxLevel;
        [Tooltip("Maximum number of summons this character can summon at a time.")]
        public int maxSummonCount;

        [Header("Animations")]
        [Tooltip("Specific to the character in sample assets, will need to be modified to work with others.")]
        public float runCycleLegOffset = 0.2f;

        [Header("Starter Abilities")]
        public List<AbilityTemplateLevel> abilities;

        [Header("Starter Equipment")]
        public WeaponTemplate primaryWeapon;
        public WeaponTemplate secondaryWeapon;

        public ArmorPartTemplate headArmorPart;
        public ArmorPartTemplate torsoArmorPart;
        public ArmorPartTemplate handsArmorPart;
        public ArmorPartTemplate legsArmorPart;
        public ArmorPartTemplate feetArmorPart;
    }
}