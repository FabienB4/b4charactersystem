using System.Collections;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Interfaces;
using B4Games.CharacterSystem.Models.Abilities;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items.Templates;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items
{
    public class Consumable : Item, IUsable
    {
        public new ConsumableTemplate template { get { return (ConsumableTemplate)m_Template; } }

        private float m_EndReuseDurationTime;
        public float endReuseDurationTime { get { return m_EndReuseDurationTime; } }

        private Ability m_LinkedAbility = null;
        public Ability linkedAbility { get { return m_LinkedAbility; } }

        private Effect m_LinkedEffect = null;
        public Effect linkedEffect { get { return m_LinkedEffect; } }

        private Coroutine m_ReuseDurationTimerCoroutine = null;

        /// <summary>
        /// Constructor for Consumable.
        /// </summary>
        /// <param name="template">The item template.</param>
        /// <param name="quantity">The quantity of item.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        protected Consumable(ItemTemplate template, long quantity, Character owner, ItemLocation location) : base(template, quantity, owner, location)
        {
            m_EndReuseDurationTime = 0;
        }

        /// <summary>
        /// Factory for Consumable.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="quantity">The quantity of item.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>A new Consumable instance.</returns>
        public new static Consumable Create(ItemTemplate template, long quantity, Character owner, ItemLocation location)
        {
            if (!Consumable.IsValid(template, quantity, owner, location))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid item template.");
#endif

                return null;
            }

            Consumable consumable = new Consumable(template, quantity, owner, location);

            // attach behaviour
            switch (consumable.template.consumableType)
            {
                case ConsumableType.AbilityTemplate:
                    consumable.m_LinkedAbility = Ability.Create(consumable.template.abilityTemplateLevel, consumable.m_Owner);
                    break;
                case ConsumableType.EffectTemplate:
                    consumable.m_LinkedEffect = Effect.Create(consumable.template.effectTemplateLevel);
                    break;
            }

            return consumable;
        }

        /// <summary>
        /// Check if a Factory params are valid.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="quantity">The quantity of item.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>True if valid.</returns>
        public new static bool IsValid(ItemTemplate template, long quantity, Character owner, ItemLocation location)
        {
            if (!(template is ConsumableTemplate))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid Consumable template");
#endif

                return false;
            }

            return true;
        }

        /// <summary>
        /// Determine if consumable is usable. [Only here to satisfy the interface.]
        /// </summary>
        /// <returns>True</returns>
        public bool IsUsable()
        {
            return true;
        }

        /// <summary>
        /// Use item.
        /// </summary>
        public void Use()
        {
            if (!IsConsumable())
            {
                m_Owner.Notify(Notification.Create("NOT_USABLE"));
                return;
            }

            // can only use item in inventory
            if (!this.isInInventory)
            {
                m_Owner.Notify(Notification.Create("ITEM_NOT_IN_INVENTORY"));
                return;
            }

            if (!CheckConditions(m_Owner, m_Owner))
            {
                return;
            }

            if (m_EndReuseDurationTime != 0)
            {
                m_Owner.Notify(Notification.Create("ITEM_IN_COOLDOWN"));
                return;
            }

            StartReuseDurationTimer();

            switch (this.template.consumableType)
            {
                case ConsumableType.RegenerateHealth:
                    m_Owner.IncreaseHealth(this.template.regenerationValue);
                    break;
                case ConsumableType.RegenerateEnergy:
                    m_Owner.IncreaseEnergy(this.template.regenerationValue);
                    break;
                case ConsumableType.AbilityTemplate:
                    if (m_LinkedAbility != null)
                    {
                        m_LinkedAbility.Use();
                    }
                    break;
                case ConsumableType.EffectTemplate:
                    if (m_LinkedEffect != null)
                    {
                        m_Owner.AddEffect(m_Owner, m_LinkedEffect);
                    }
                    break;
            }

            RemoveQuantity(1);
        }

        /// <summary>
        /// Cancel the use of linked ability, if any.
        /// </summary>
        public void CancelUse()
        {
            if (!IsConsumable())
            {
                m_Owner.Notify(Notification.Create("NOT_USABLE"));
                return;
            }

            if (m_LinkedAbility != null || (this.template.consumableType != ConsumableType.AbilityTemplate))
            {
                return;
            }

            // will do nothing if no cast in progress
            m_LinkedAbility.CancelUse();
        }

        /// <summary>
        /// Reuse timer task.
        /// </summary>
        private IEnumerator ReuseDurationTimer()
        {
            while (Time.time < m_EndReuseDurationTime)
            {
                yield return null;
            }

            OnReuseDurationTimerEnd();
        }

        /// <summary>
        /// Start the reuse timer task.
        /// TODO END-USER: Needs to be adapted if used with a multi-stacks capable inventory and item is stackable (reuse per template, not per stack).
        /// </summary>
        private void StartReuseDurationTimer()
        {
            if (m_ReuseDurationTimerCoroutine != null)
            {
                return;
            }

            m_EndReuseDurationTime = this.template.reuseDuration > 0 ? (Time.time + this.template.reuseDuration) : 0;

            if (m_EndReuseDurationTime > 0)
            {
                m_ReuseDurationTimerCoroutine = m_Owner.StartCoroutine(ReuseDurationTimer());
            }
        }

        /// <summary>
        /// Stop the reuse timer task.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void StopReuseDurationTimer(bool end = true)
        {
            if (m_ReuseDurationTimerCoroutine == null)
            {
                return;
            }

            m_Owner.StopCoroutine(m_ReuseDurationTimerCoroutine);
            OnReuseDurationTimerEnd(end);
        }

        /// <summary>
        /// Triggered when the reuse timer task ends or is stopped.
        /// </summary>
        /// <param name="end">[Optional] Behave as if the timer was ending. Default: true.</param>
        private void OnReuseDurationTimerEnd(bool end = true)
        {
            m_ReuseDurationTimerCoroutine = null;
            m_EndReuseDurationTime = 0;

            if (end)
            {
            }
        }
    }
}