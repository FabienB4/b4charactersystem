using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items
{
    public class Weapon : Equippable
    {
        public new WeaponTemplate template { get { return (WeaponTemplate)m_Template; } }

        /// <summary>
        /// Constructor for Weapon.
        /// </summary>
        /// <param name="template">The item template.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        protected Weapon(ItemTemplate template, Character owner, ItemLocation location) : base(template, owner, location)
        {
        }

        /// <summary>
        /// Factory for Weapon.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>A new Weapon instance.</returns>
        public new static Weapon Create(ItemTemplate template, Character owner, ItemLocation location)
        {
            if (!Weapon.IsValid(template, owner, location))
            {
                return null;
            }

            return new Weapon(template, owner, location);
        }

        /// <summary>
        /// Check if a Factory params are valid.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>True if valid.</returns>
        public static bool IsValid(ItemTemplate template, Character owner, ItemLocation location)
        {
            if (!(template is WeaponTemplate))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid Weapon template");
#endif

                return false;
            }

            return true;
        }
    }
}