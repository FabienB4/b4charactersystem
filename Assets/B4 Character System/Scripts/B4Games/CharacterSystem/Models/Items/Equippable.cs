using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Interfaces;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items.Templates;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items
{
    public abstract class Equippable : Item, IEquippable
    {
        public new EquippableTemplate template { get { return (EquippableTemplate)m_Template; } }

        /// <summary>
        /// Constructor for Equippable.
        /// </summary>
        /// <param name="template">The item template.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        protected Equippable(ItemTemplate template, Character owner, ItemLocation location) : base(template, 1, owner, location)
        {
        }

        /// <summary>
        /// Equip item.
        /// <param name="actor">The Character requesting the action.</param>
        /// </summary>
        /// <returns>True if sucessful.</returns>
        public bool Equip(Character actor)
        {
            if (m_Owner == null)
            {
                return false;
            }

            // can only equip item if owner is requester
            if (!CheckOwner(actor))
            {
                m_Owner.Notify(Notification.Create("NOT_OWNER"));
                return false;
            }

            if (this.isEquipped)
            {
                m_Owner.Notify(Notification.Create("ITEM_ALREADY_EQUIPPED"));
                return false;
            }

            // can only equip item from inventory
            if (!this.isInInventory)
            {
                m_Owner.Notify(Notification.Create("ITEM_NOT_IN_INVENTORY"));
                return false;
            }

            if (!CheckConditions(m_Owner, m_Owner))
            {
                return false;
            }

            if (IsWeapon())
            {
                Weapon weapon = GetWeapon();
                WeaponSlot slot = weapon.template.slot;
                Equippable currentEquippedItem = m_Owner.equipment.GetCurrentWeapon(slot);

                // slot not empty
                if (currentEquippedItem != null)
                {
                    if (currentEquippedItem == this)
                    {
                        m_Owner.Notify(Notification.Create("ITEM_ALREADY_EQUIPPED"));
                        return false;
                    }

                    // unequip from item for proper item update
                    if (!currentEquippedItem.UnEquip(actor))
                    {
                        m_Owner.Notify(Notification.Create("UNABLE_TO_EQUIP_ITEM"));
                        return false;
                    }
                }

                if (!m_Owner.equipment.ReplaceWeapon(slot, weapon))
                {
                    m_Owner.Notify(Notification.Create("UNABLE_TO_EQUIP_ITEM"));
                    return false;
                }
            }
            else if (IsArmorPart())
            {
                ArmorPart armorPart = GetArmorPart();
                ArmorPartSlot slot = armorPart.template.slot;
                Equippable currentEquippedItem = m_Owner.equipment.GetCurrentArmorPart(slot);

                // slot not empty
                if (currentEquippedItem != null)
                {
                    if (currentEquippedItem == this)
                    {
                        m_Owner.Notify(Notification.Create("ITEM_ALREADY_EQUIPPED"));
                        return false;
                    }

                    // unequip from item for proper item update
                    if (!currentEquippedItem.UnEquip(actor))
                    {
                        m_Owner.Notify(Notification.Create("UNABLE_TO_EQUIP_ITEM"));
                        return false;
                    }
                }

                if (!m_Owner.equipment.ReplaceArmorPart(slot, armorPart))
                {
                    return false;
                }
            }

            m_Owner.AddItemAbilities(this);
            m_Owner.AddItemStats(this);
            m_Owner.inventory.Remove(this);

            m_Location = ItemLocation.Equipment;

            return true;
        }

        /// <summary>
        /// UnEquip item.
        /// <param name="actor">The Character requesting the action.</param>
        /// </summary>
        /// <returns>True if sucessful.</returns>
        public bool UnEquip(Character actor)
        {
            if (m_Owner == null)
            {
                return false;
            }

            // can only unequip item if owner is requester
            if (!CheckOwner(actor))
            {
                m_Owner.Notify(Notification.Create("NOT_OWNER"));
                return false;
            }

            if (!this.isEquipped)
            {
                m_Owner.Notify(Notification.Create("ITEM_NOT_EQUIPPED"));
                return false;
            }

            if (!CheckConditions(actor, m_Owner))
            {
                return false;
            }

            if (IsWeapon())
            {
                if (!m_Owner.equipment.RemoveWeapon(GetWeapon().template.slot))
                {
                    return false;
                }
            }
            else if (IsArmorPart())
            {
                if (!m_Owner.equipment.RemoveArmorPart(GetArmorPart().template.slot))
                {
                    return false;
                }
            }

            m_Owner.RemoveItemAbilities(this);
            m_Owner.RemoveItemStats(this);
            m_Owner.inventory.Add(this);

            m_Location = ItemLocation.Inventory;

            return true;
        }

        /// <summary>
        /// Check if equippable item has stats.
        /// </summary>
        /// <returns>True if item has stats.</returns>
        public bool HasStats()
        {
            return template.HasStats();
        }

        /// <summary>
        /// Check if equippable item has abilities.
        /// </summary>
        /// <returns>True if item has abilities.</returns>
        public bool HasAbilities()
        {
            return template.HasAbilities();
        }
    }
}