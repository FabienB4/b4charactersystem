using System;
using B4Games.CharacterSystem.Configs;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Conditions;
using B4Games.CharacterSystem.Models.Items.Templates;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items
{
    public class Item
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        protected ItemTemplate m_Template;
        public ItemTemplate template { get { return m_Template; } }

        protected Character m_Owner;
        public Character owner { get { return m_Owner; } }

        private long m_Quantity = 1;
        public long quantity
        {
            get { return m_Quantity; }
            private set
            {
                if (!m_Template.stackable && value > 1)
                {
#if UNITY_EDITOR
                    Debug.LogError("Non-stackable item " + m_Template.itemName + " was being stacked for character " + m_Owner.id + ".");
#endif

                    return;
                }

                if (value <= 0)
                {
                    return;
                }

                long maxStackQuantity = Config.Item().maxStackQuantity;
                m_Quantity = value > maxStackQuantity ? maxStackQuantity : value;

                OnQuantityChanged(m_Quantity);
            }
        }

        protected ItemLocation m_Location;
        public ItemLocation location { get { return m_Location; } }

        public bool isEquipped { get { return m_Location == ItemLocation.Equipment; } }
        public bool isInInventory { get { return m_Location == ItemLocation.Inventory; } }
        public bool isInStorage { get { return m_Location == ItemLocation.Storage; } }

        /// <summary>
        /// Constructor for Item.
        /// </summary>
        /// <param name="template">The item template.</param>
        /// <param name="quantity">The quantity of item.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        protected Item(ItemTemplate template, long quantity, Character owner, ItemLocation location)
        {
            m_Id = Guid.NewGuid();
            m_Template = template;
            m_Quantity = quantity;
            m_Owner = owner;
            m_Location = location;
        }

        /// <summary>
        /// Shorthand for Item.Create(template, 1, null, ItemLocation.None).
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <returns>A new Item instance.</returns>
        public static Item Create(ItemTemplate template)
        {
            return Item.Create(template, 1, null, ItemLocation.None);
        }

        /// <summary>
        /// Shorthand for Item.Create(template, quantity, null, ItemLocation.None).
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="quantity">The quantity of item.</param>
        /// <returns>A new Item instance.</returns>
        public static Item Create(ItemTemplate template, long quantity)
        {
            return Item.Create(template, quantity, null, ItemLocation.None);
        }

        /// <summary>
        /// Shorthand for Item.Create(template, quantity, owner, owner == null ? ItemLocation.None : ItemLocation.Inventory).
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="quantity">The quantity of item.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <returns>A new Item instance.</returns>
        public static Item Create(ItemTemplate template, long quantity, Character owner)
        {
            return Item.Create(template, quantity, owner, owner == null ? ItemLocation.None : ItemLocation.Inventory);
        }

        /// <summary>
        /// Shorthand for Item.Create(template, 1, owner, owner == null ? ItemLocation.None : ItemLocation.Inventory).
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <returns>A new Item instance.</returns>
        public static Item Create(ItemTemplate template, Character owner)
        {
            return Item.Create(template, 1, owner, owner == null ? ItemLocation.None : ItemLocation.Inventory);
        }

        /// <summary>
        /// Shorthand for Item.Create(template, 1, owner, location).
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>A new Item instance.</returns>
        public static Item Create(ItemTemplate template, Character owner, ItemLocation location)
        {
            return Item.Create(template, 1, owner, location);
        }

        /// <summary>
        /// Factory for Item.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="quantity">The quantity of item. Ignored for ArmorPart and Weapon (always 1).</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>A new Item instance.</returns>
        public static Item Create(ItemTemplate template, long quantity, Character owner, ItemLocation location)
        {
            if (!Item.IsValid(template, quantity, owner, location))
            {
                return null;
            }

            Item item = null;

            if (template is ArmorPartTemplate)
            {
                item = ArmorPart.Create(template, owner, location);
            }
            else if (template is WeaponTemplate)
            {
                item = Weapon.Create(template, owner, location);
            }
            else if (template is ConsumableTemplate)
            {
                item = Consumable.Create(template, quantity, owner, location);
            }
            else
            {
                item = new Item(template, quantity, owner, location);
            }

            // bypass location logic
            if (item == null)
            {
                return null;
            }

            // location logic
            if (item.m_Owner != null)
            {
                if ((item.m_Location == ItemLocation.Storage && !item.template.storable) || (item.m_Location == ItemLocation.Equipment && !(item is Equippable)))
                {
                    // item should cannot be put in designated container, default to inventory
                    item.m_Owner.inventory.Add(item);

                    item.m_Location = ItemLocation.Inventory;
                }
                else
                {
                    switch (item.m_Location)
                    {
                        case ItemLocation.Inventory:
                            if (item.m_Owner.inventory != null)
                            {
                                item.m_Owner.inventory.Add(item);
                            }
                            break;
                        case ItemLocation.Storage:
                            if (item.m_Owner.storage != null)
                            {
                                item.m_Owner.storage.Add(item);
                            }
                            break;
                        case ItemLocation.Equipment:
                            if (item.m_Owner.equipment != null)
                            {
                                item.GetEquippable().Equip(item.m_Owner);
                            }
                            break;
                    }
                }
            }
            else
            {
                // make sure non-owned items are not in containers
                item.m_Location = ItemLocation.None;
            }

            return item;
        }

        /// <summary>
        /// Check if a Factory params are valid.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="quantity">The quantity of item. Ignored for ArmorPart and Weapon (always 1).</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>True if valid.</returns>
        public static bool IsValid(ItemTemplate template, long quantity, Character owner, ItemLocation location)
        {
            if (template == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid item template.");
#endif

                return false;
            }

            if (quantity <= 0 || (quantity > 1 && !template.stackable) || (quantity > Config.Item().maxStackQuantity))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid item quantity.");
#endif

                return false;
            }

            if (owner != null && template.npcOnly && !(owner is Npc))
            {
#if UNITY_EDITOR
                Debug.LogError("Item only for Npc.");
#endif

                return false;
            }

            if (owner != null && location == ItemLocation.None)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid item location.");
#endif

                return false;
            }

            return true;
        }

        /// <summary>
        /// Cast Item to Equippable. Make sure the Item is actually an Equippable before using this.
        /// </summary>
        /// <returns>Equippable instance.</returns>
        public Equippable GetEquippable()
        {
            return (Equippable)this;
        }

        /// <summary>
        /// Cast Item to Weapon. Make sure the Item is actually a Weapon before using this.
        /// </summary>
        /// <returns>Weapon instance.</returns>
        public Weapon GetWeapon()
        {
            return (Weapon)this;
        }

        /// <summary>
        /// Cast Item to ArmorPart if possible. Make sure the Item is actually an ArmorPart before using this.
        /// </summary>
        /// <returns>ArmorPart instance.</returns>
        public ArmorPart GetArmorPart()
        {
            return (ArmorPart)this;
        }

        /// <summary>
        /// Cast Item to Consumable if possible. Make sure the Item is actually a Consumable before using this.
        /// </summary>
        /// <returns>Consumable instance.</returns>
        public Consumable GetConsumable()
        {
            return (Consumable)this;
        }

        /// <summary>
        /// Destroy an item from the game world.
        /// TODO END-USER: Implement database object removal if needed.
        /// </summary>
        public virtual void Destroy()
        {
            if (m_Owner != null)
            {
                if (!m_Template.destroyable)
                {
                    m_Owner.Notify(Notification.Create("ITEM_CANNOT_BE_DESTROYED"));
                    return;
                }

                if (!this.isInInventory)
                {
                    m_Owner.Notify(Notification.Create("CANNOT_DESTROY_ITEM_NOT_IN_INVENTORY"));
                    return;
                }

                m_Owner.inventory.Remove(this);
            }

            // TODO END-USER: Remove from database if necessary
        }

        /// <summary>
        /// Put an item in storage.
        /// </summary>
        /// <param name="actor">The Character requesting the storage.</param>
        public void Store(Character actor)
        {
            if (!m_Template.storable)
            {
                m_Owner.Notify(Notification.Create("CANNOT_STORE_ITEM"));
                return;
            }

            // can only store item if owner is requester
            if (!CheckOwner(actor))
            {
                m_Owner.Notify(Notification.Create("NOT_OWNER"));
                return;
            }

            if (!this.isInInventory)
            {
                m_Owner.Notify(Notification.Create("CANNOT_STORE_ITEM_NOT_IN_INVENTORY"));
                return;
            }

            if (m_Owner.inventory.Remove(this))
            {
                m_Owner.storage.Add(this);

                m_Location = ItemLocation.Storage;
            }
        }

        /// <summary>
        /// Retrieve an item from storage.
        /// </summary>
        /// <param name="actor">The Character requesting the retrieval.</param>
        public void Retrieve(Character actor)
        {
            if (!m_Template.storable)
            {
                m_Owner.Notify(Notification.Create("CANNOT_RETRIEVE_ITEM"));
                return;
            }

            // can only retrieve item if owner is requester
            if (!CheckOwner(actor))
            {
                m_Owner.Notify(Notification.Create("NOT_OWNER"));
                return;
            }

            if (this.isInStorage)
            {
                m_Owner.Notify(Notification.Create("CANNOT_RETRIEVE_ITEM_NOT_IN_STORAGE"));
                return;
            }

            if (m_Owner.storage.Remove(this))
            {
                m_Owner.inventory.Add(this);

                m_Location = ItemLocation.Inventory;
            }
        }

        /// <summary>
        /// Change the owner of the item. Take care of switching to new inventory and cleaning up from previous location.
        /// </summary>
        /// <param name="actor">The Character requesting the change. Can be null.</param>
        /// <param name="newOwner">The Character receiving the item.</param>
        public void ChangeOwner(Character actor, Character newOwner)
        {
            if (newOwner == null)
            {
                return;
            }

            if (m_Owner != null)
            {
                if (!CheckOwner(actor))
                {
                    m_Owner.Notify(Notification.Create("NOT_OWNER"));
                    return;
                }

                switch (m_Location)
                {
                    case ItemLocation.Inventory:
                        m_Owner.inventory.Remove(this);
                        break;
                    case ItemLocation.Storage:
                        m_Owner.storage.Remove(this);
                        break;
                    case ItemLocation.Equipment:
                        if (IsEquippable())
                        {
                            GetEquippable().UnEquip(actor);
                        }

                        m_Owner.inventory.Remove(this);
                        break;
                }
            }

            // always put in inventory
            m_Location = ItemLocation.Inventory;
            m_Owner = newOwner;

            newOwner.inventory.Add(this);
        }

        /// <summary>
        /// Change the quantity of the item by the specified amount.
        /// </summary>
        /// <param name="amount">The amount to change. Can be positive or negative.</param>
        public void ChangeQuantity(long amount)
        {
            // can't change the quantity of equippable items and non-stackable
            if (this is Equippable || !m_Template.stackable)
            {
                return;
            }

            if (amount == 0)
            {
                return;
            }

            long newQuantity = m_Quantity + amount;

            if (newQuantity <= 0)
            {
                Destroy();

                // no need to assign quantity, object is removed from the game
                return;
            }

            this.quantity = newQuantity;
        }

        /// <summary>
        /// Shorthand for ChangeQuantity(amount).
        /// </summary>
        /// <param name="amount">The amount to add.</param>
        public void AddQuantity(long amount)
        {
            ChangeQuantity(amount);
        }

        /// <summary>
        /// Shorthand for ChangeQuantity(-amount).
        /// </summary>
        /// <param name="amount">The amount to remove.</param>
        public void RemoveQuantity(long amount)
        {
            ChangeQuantity(-amount);
        }

        /// <summary>
        /// Check the conditions attached to the item.
        /// </summary>
        /// <param name="actor">The Character acting.</param>
        /// <param name="target">The target Character.</param>
        /// <returns>True if all conditions passed.</returns>
        public bool CheckConditions(Character actor, Character target)
        {
            if (target == null)
            {
                return false;
            }

            // no condition
            if (!HasConditions())
            {
                return true;
            }

            for (int i = 0; i < m_Template.conditions.Count; i++)
            {
                Condition condition = Condition.Create(m_Template.conditions[i]);

                if (condition == null)
                {
                    continue;
                }

                if (!condition.Test(actor, target, this))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Check if item has conditions attached.
        /// </summary>
        /// <returns>True if conditions attached.</returns>
        public bool HasConditions()
        {
            return m_Template.HasConditions();
        }

        /// <summary>
        /// Check if item is equippable.
        /// </summary>
        /// <returns>True if equippable.</returns>
        public virtual bool IsEquippable()
        {
            return this is Equippable;
        }

        /// <summary>
        /// Test if item is a Weapon.
        /// </summary>
        /// <returns>True if item is Weapon.</returns>
        public bool IsWeapon()
        {
            return this is Weapon;
        }

        /// <summary>
        /// Test if item is an ArmorPart.
        /// </summary>
        /// <returns>True if item is ArmorPart.</returns>
        public bool IsArmorPart()
        {
            return this is ArmorPart;
        }

        /// <summary>
        /// Test if item is a Consumable.
        /// </summary>
        /// <returns>True if item is Consumable.</returns>
        public bool IsConsumable()
        {
            return this is Consumable;
        }

        /// <summary>
        /// Check if specified character is owner of the item.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>True if owner</returns>
        public bool CheckOwner(Character character)
        {
            return m_Owner != null && character != null && m_Owner == character;
        }

        /// <summary>
        /// Event triggered when an item quantity changed.
        /// </summary>
        /// <param name="newQuantity">The new item quantity.</param>
        private void OnQuantityChanged(long newQuantity)
        {
            // if quantity reached 0, destroy the item
            if (newQuantity <= 0)
            {
                Destroy();
            }
        }
    }
}