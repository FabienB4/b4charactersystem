using B4Games.CharacterSystem.Enums;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Items/Weapon Template")]
    public class WeaponTemplate : EquippableTemplate
    {
        [Header("Weapon Specific")]
        public WeaponType type;
        public WeaponSlot slot;
    }
}