using System.Collections.Generic;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items.Templates
{
    public class EquippableTemplate : ItemTemplate
    {
        [Header("Equippable Item Specific")]
        public List<ItemStat> stats;
        public List<AbilityTemplateLevel> abilities;

        /// <summary>
        /// Check if equippable item template has stats.
        /// </summary>
        /// <returns>True if item has stats.</returns>
        public bool HasStats()
        {
            return stats != null && stats.Count > 0;
        }

        /// <summary>
        /// Check if equippable item template has abilities.
        /// </summary>
        /// <returns>True if item has abilities.</returns>
        public bool HasAbilities()
        {
            return abilities != null && abilities.Count > 0;
        }
    }
}