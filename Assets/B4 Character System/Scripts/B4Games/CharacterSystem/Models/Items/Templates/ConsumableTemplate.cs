using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using B4Games.CharacterSystem.Models.Effects.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Items/Consumable Item Template")]
    public class ConsumableTemplate : ItemTemplate
    {
        [Header("Consumable Item Specific")]
        public ConsumableType consumableType;
        public float reuseDuration;

        // Regeneration Specific
        public int regenerationValue;

        // Ability Specific
        public AbilityTemplateLevel abilityTemplateLevel;

        // Effect Specific
        public EffectTemplateLevel effectTemplateLevel;
    }
}