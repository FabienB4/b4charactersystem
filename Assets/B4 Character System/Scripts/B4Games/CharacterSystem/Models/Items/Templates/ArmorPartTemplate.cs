using B4Games.CharacterSystem.Enums;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Items/Armor Part Template")]
    public class ArmorPartTemplate : EquippableTemplate
    {
        [Header("Armor Specific")]
        public ArmorPartType type;
        public ArmorPartSlot slot;
    }
}