using System.Collections.Generic;
using UnityEngine;
using B4Games.CharacterSystem.Models.Conditions.Templates;
using System;
using B4Games.CharacterSystem.Enums;

namespace B4Games.CharacterSystem.Models.Items.Templates
{
    [CreateAssetMenu(menuName = "B4 Character System/Items/Item Template")]
    public class ItemTemplate : ScriptableObject
    {
        [Serializable]
        public struct ItemStat
        {
            public Stat stat;
            public StatModifierType modifierType;
            public float power;
            [Tooltip("Low orders are applied first, high orders are applied after.\nNote: You probably want item orders to be lower than effect orders.")]
            [Range(0, 100)]
            public int order;
        }

        [Header("Display")]
        public string itemName;
        public string description;
        public Sprite icon;

        [Header("Statuses")]
        public bool storable = true;
        public bool destroyable = true;
        public bool stackable = false;
        [Tooltip("If true, only Npcs can possess the item.")]
        public bool npcOnly = false;
        public List<ConditionTemplate> conditions;

        /// <summary>
        /// Check if item template has conditions attached.
        /// </summary>
        /// <returns>True if conditions attached.</returns>
        public bool HasConditions()
        {
            return conditions != null && conditions.Count > 0;
        }
    }
}