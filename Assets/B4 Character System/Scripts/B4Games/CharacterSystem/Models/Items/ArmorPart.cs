using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.Items
{
    public class ArmorPart : Equippable
    {
        public new ArmorPartTemplate template { get { return (ArmorPartTemplate)m_Template; } }

        /// <summary>
        /// Constructor for ArmorPart.
        /// </summary>
        /// <param name="template">The item template.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        protected ArmorPart(ItemTemplate template, Character owner, ItemLocation location) : base(template, owner, location)
        {
        }

        /// <summary>
        /// Factory for ArmorPart.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>A new ArmorPart instance.</returns>
        public new static ArmorPart Create(ItemTemplate template, Character owner, ItemLocation location)
        {
            if (!ArmorPart.IsValid(template, owner, location))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid item template.");
#endif

                return null;
            }

            return new ArmorPart(template, owner, location);
        }

        /// <summary>
        /// Check if a Factory params are valid.
        /// </summary>
        /// <param name="template">The item template to use.</param>
        /// <param name="owner">The owner of the item.</param>
        /// <param name="location">The location of the item.</param>
        /// <returns>True if valid.</returns>
        public static bool IsValid(ItemTemplate template, Character owner, ItemLocation location)
        {
            if (!(template is ArmorPartTemplate))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid ArmorPart template");
#endif

                return false;
            }

            return true;
        }
    }
}