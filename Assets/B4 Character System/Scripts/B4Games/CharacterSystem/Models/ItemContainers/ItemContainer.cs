using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.ItemContainers
{
    public class ItemContainer
    {
        protected Guid m_Id;
        public Guid id { get { return m_Id; } }

        protected Character m_Owner;
        public Character owner { get { return m_Owner; } }

        protected List<Item> m_Items;
        public ReadOnlyCollection<Item> items { get { return m_Items.AsReadOnly(); } }

        /// <summary>
        /// Constructor for ItemContainer.
        /// </summary>
        /// <param name="owner">The owner of the ItemContainer container.</param>
        protected ItemContainer(Character owner)
        {
            m_Id = Guid.NewGuid();
            m_Owner = owner;

            m_Items = new List<Item>();
        }

        /// <summary>
        /// Basic factory creator for ItemContainer.
        /// </summary>
        /// <param name="owner">The owner of the ItemContainer container.</param>
        /// <returns>A new ItemContainer instance.</returns>
        public static ItemContainer Create(Character owner)
        {
            if (owner == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid owner");
#endif

                return null;
            }

            return new ItemContainer(owner);
        }

        /// <summary>
        /// Add an item to the ItemContainer.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public virtual void Add(Item item)
        {
            if (item == null)
            {
                return;
            }

            if (m_Items.Contains(item))
            {
                return;
            }

            m_Items.Add(item);
        }

        /// <summary>
        /// Remove an item from the ItemContainer.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        public virtual bool Remove(Item item)
        {
            if (item == null)
            {
                return false;
            }

            return m_Items.Remove(item);
        }

        /// <summary>
        /// Shorthand for m_Items.Find(match).
        /// </summary>
        public virtual Item Find(Predicate<Item> match)
        {
            return m_Items.Find(match);
        }

        /// <summary>
        /// Shorthand for m_Items.FindAll(match).
        /// </summary>
        public virtual List<Item> FindAll(Predicate<Item> match)
        {
            return m_Items.FindAll(match);
        }
    }
}