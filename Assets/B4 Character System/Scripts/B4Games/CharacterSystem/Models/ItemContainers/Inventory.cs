using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.ItemContainers
{
    public class Inventory : ItemContainer
    {
        /// <summary>
        /// Constructor for Inventory.
        /// </summary>
        /// <param name="owner">The owner of the Inventory container.</param>
        protected Inventory(Character owner) : base(owner)
        {
        }

        /// <summary>
        /// Basic factory creator for Inventory.
        /// </summary>
        /// <param name="owner">The owner of the Inventory container.</param>
        /// <returns>A new Inventory instance.</returns>
        public new static Inventory Create(Character owner)
        {
            if (owner == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid owner");
#endif

                return null;
            }

            return new Inventory(owner);
        }

        /// <summary>
        /// Add an item to the inventory.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public override void Add(Item item)
        {
            base.Add(item);

            // TODO END-USER: DEBUG
/* UNCOMMENT TO TEST SAMPLES
#if UNITY_EDITOR
            List<CombatUI> uis = GameObject.FindObjectsOfType<CombatUI>().ToList();
            CombatUI ownUI = uis.Find((ui) => ui.character == this.owner);

            if (ownUI != null)
            {
                ownUI.AddItemToInventory(item);
            }
#endif
*/
            // END DEBUG
        }

        /// <summary>
        /// Remove an item from the inventory.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        public override bool Remove(Item item)
        {
            if (item == null)
            {
                return false;
            }

            // TODO END-USER: DEBUG
/*
#if UNITY_EDITOR
            List<CombatUI> uis = GameObject.FindObjectsOfType<CombatUI>().ToList();
            CombatUI ownUI = uis.Find((ui) => ui.character == this.m_Owner);

            if (ownUI != null)
            {
                ownUI.RemoveItemFromInventory(item);
            }
#endif
*/
            // END DEBUG

            return base.Remove(item);
        }
    }
}