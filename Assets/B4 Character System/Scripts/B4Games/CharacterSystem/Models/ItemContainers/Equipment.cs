using System;
using System.Collections.Generic;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Notifications;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.ItemContainers
{
    public class Equipment
    {
        private Guid m_Id;
        public Guid id { get { return m_Id; } }

        private Character m_Owner;
        public Character owner { get { return m_Owner; } }

        private Dictionary<WeaponSlot, Weapon> m_Weapons;
        public Dictionary<WeaponSlot, Weapon> weapons { get { return m_Weapons; } }
        private Dictionary<ArmorPartSlot, ArmorPart> m_ArmorParts;
        public Dictionary<ArmorPartSlot, ArmorPart> armorParts { get { return m_ArmorParts; } }

        /// <summary>
        /// Constructor for Equipment.
        /// </summary>
        /// <param name="owner">The owner of the Equipment container.</param>
        protected Equipment(Character owner)
        {
            m_Id = Guid.NewGuid();
            m_Owner = owner;

            // initialize dictionaries
            m_Weapons = new Dictionary<WeaponSlot, Weapon>() {
                { WeaponSlot.Primary, null },
                { WeaponSlot.Secondary, null }
            };

            m_ArmorParts = new Dictionary<ArmorPartSlot, ArmorPart>() {
                { ArmorPartSlot.Head, null },
                { ArmorPartSlot.Torso, null },
                { ArmorPartSlot.Hands, null },
                { ArmorPartSlot.Legs, null },
                { ArmorPartSlot.Feet, null }
            };
        }

        /// <summary>
        /// Basic factory creator for Equipment.
        /// </summary>
        /// <param name="owner">The owner of the Equipment container.</param>
        /// <returns>A new Equipment instance.</returns>
        public static Equipment Create(Character owner)
        {
            if (owner == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid owner");
#endif

                return null;
            }

            return new Equipment(owner);
        }

        /// <summary>
        /// Replace the weapon in the specified slot.
        /// </summary>
        /// <param name="slot">The slot.</param>
        /// <param name="newWeapon">The replacement weapon.</param>
        /// <returns>True if replacement was successful.</returns>
        public bool ReplaceWeapon(WeaponSlot slot, Weapon newWeapon)
        {
            if (!m_Weapons.ContainsKey(slot))
            {
                return false;
            }

            m_Weapons[slot] = newWeapon;

            return true;
        }

        /// <summary>
        /// Replace the armor part in the specified slot.
        /// </summary>
        /// <param name="slot">The slot.</param>
        /// <param name="newArmorPart">The replacement armor part.</param>
        /// <returns>True if replacement was successful.</returns>
        public bool ReplaceArmorPart(ArmorPartSlot slot, ArmorPart newArmorPart)
        {
            if (!m_ArmorParts.ContainsKey(slot))
            {
                return false;
            }

            m_ArmorParts[slot] = newArmorPart;

            return true;
        }

        /// <summary>
        /// Remove the weapon in the specified slot.
        /// </summary>
        /// <param name="slot">The slot.</param>
        /// <returns>True if unequip was successful.</returns>
        public bool RemoveWeapon(WeaponSlot slot)
        {
            if (GetCurrentWeapon(slot) == null)
            {
                m_Owner.Notify(Notification.Create("SLOT_EMPTY", slot.ToString()));
                return false;
            }

            return ReplaceWeapon(slot, null);
        }

        /// <summary>
        /// Remove the armor part in the specified slot.
        /// </summary>
        /// <param name="slot">The slot.</param>
        /// <returns>True if unequip successful.</returns>
        public bool RemoveArmorPart(ArmorPartSlot slot)
        {
            if (GetCurrentArmorPart(slot) == null)
            {
                m_Owner.Notify(Notification.Create("SLOT_EMPTY", slot.ToString()));
                return false;
            }

            return ReplaceArmorPart(slot, null);
        }

        /// <summary>
        /// Get the currenly equipped item in the specified weapon slot.
        /// </summary>
        /// <param name="slot">The slot.</param>
        /// <returns>The item equipped in the slot, if any.</returns>
        public Equippable GetCurrentWeapon(WeaponSlot slot)
        {
            Weapon currentEquipped = null;

            if (!m_Weapons.TryGetValue(slot, out currentEquipped))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid slot");
#endif

                return null;
            }

            return currentEquipped;
        }

        /// <summary>
        /// Get the currenly equipped item in the specified armort part slot.
        /// </summary>
        /// <param name="slot">The slot.</param>
        /// <returns>The item equipped in the slot, if any.</returns>
        public Equippable GetCurrentArmorPart(ArmorPartSlot slot)
        {
            ArmorPart currentEquipped = null;

            if (!m_ArmorParts.TryGetValue(slot, out currentEquipped))
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid slot");
#endif

                return null;
            }

            return currentEquipped;
        }
    }
}