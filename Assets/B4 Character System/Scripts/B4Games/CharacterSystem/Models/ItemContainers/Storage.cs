using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items;
using UnityEngine;

namespace B4Games.CharacterSystem.Models.ItemContainers
{
    public class Storage : ItemContainer
    {
        /// <summary>
        /// Constructor for Storage.
        /// </summary>
        /// <param name="owner">The owner of the Storage container.</param>
        protected Storage(Character owner) : base(owner)
        {
        }

        /// <summary>
        /// Basic factory creator for Storage.
        /// </summary>
        /// <param name="owner">The owner of the Storage container.</param>
        /// <returns>A new Storage instance.</returns>
        public new static Storage Create(Character owner)
        {
            if (owner == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Invalid owner");
#endif

                return null;
            }

            return new Storage(owner);
        }

        /// <summary>
        /// Add an item to the Storage.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public override void Add(Item item)
        {
            base.Add(item);

            // TODO: DEBUG
/* UNCOMMENT TO TEST SAMPLES
#if UNITY_EDITOR
            List<CombatUI> uis = GameObject.FindObjectsOfType<CombatUI>().ToList();
            CombatUI ownUI = uis.Find((ui) => ui.character == this.m_Owner);

            ownUI.AddItemToStorage(item);
#endif
*/
            // END DEBUG
        }

        /// <summary>
        /// Remove an item from the Storage.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        public override bool Remove(Item item)
        {
            if (item == null)
            {
                return false;
            }

            // TODO: DEBUG
/*
#if UNITY_EDITOR
            List<CombatUI> uis = GameObject.FindObjectsOfType<CombatUI>().ToList();
            CombatUI ownUI = uis.Find((ui) => ui.character == this.m_Owner);

            ownUI.RemoveItemFromStorage(item);
#endif
*/
            // END DEBUG

            return base.Remove(item);
        }
    }
}