using System;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Effects.Templates;
using B4Games.UnityEditorExtended;
using UnityEditor;
using UnityEngine;

namespace B4Games.CharacterSystem.Editor.Helpers
{
    public static class B4CharacterSystemEditorGUILayout
    {
        /// <summary>
        /// Ensure int property minimum value. Support arrays.
        /// </summary>
        /// <param name="property">The property</param>
        /// <param name="minValue">The minimum value.</param>
        public static void EnsureMinIntValue(SerializedProperty property, int minValue)
        {
            if (property.isArray)
            {
                for (int i = 0; i < property.arraySize; i++)
                {
                    EnsureMinIntValue(property.GetArrayElementAtIndex(i), minValue);
                }

                return;
            }

            if (property.intValue < minValue)
            {
                property.intValue = minValue;
            }
        }

        /// <summary>
        /// Ensure int property maximum value. Support arrays.
        /// </summary>
        /// <param name="property">The property</param>
        /// <param name="minValue">The minimum value.</param>
        public static void EnsureMaxIntValue(SerializedProperty property, int maxValue)
        {
            if (property.isArray)
            {
                for (int i = 0; i < property.arraySize; i++)
                {
                    EnsureMaxIntValue(property.GetArrayElementAtIndex(i), maxValue);
                }

                return;
            }

            if (property.intValue > maxValue)
            {
                property.intValue = maxValue;
            }
        }

        /// <summary>
        /// Ensure float property minimum value. Support arrays.
        /// </summary>
        /// <param name="property">The property</param>
        /// <param name="minValue">The minimum value.</param>
        public static void EnsureMinFloatValue(SerializedProperty property, float minValue)
        {
            if (property.isArray)
            {
                for (int i = 0; i < property.arraySize; i++)
                {
                    EnsureMinFloatValue(property.GetArrayElementAtIndex(i), minValue);
                }

                return;
            }

            if (property.floatValue < minValue)
            {
                property.floatValue = minValue;
            }
        }

        /// <summary>
        /// Ensure float property maximum value. Support arrays.
        /// </summary>
        /// <param name="property">The property</param>
        /// <param name="minValue">The minimum value.</param>
        public static void EnsureMaxFloatValue(SerializedProperty property, float maxValue)
        {
            if (property.isArray)
            {
                for (int i = 0; i < property.arraySize; i++)
                {
                    EnsureMaxFloatValue(property.GetArrayElementAtIndex(i), maxValue);
                }

                return;
            }

            if (property.floatValue > maxValue)
            {
                property.floatValue = maxValue;
            }
        }

        /// <summary>
        /// Ensure list property minimum size.
        /// </summary>
        /// <param name="property">The property</param>
        /// <param name="minValue">The minimum value.</param>
        public static void EnsureListMinSize(SerializedProperty property, int minSize)
        {
            if (property.arraySize < minSize)
            {
                property.arraySize = minSize;
            }
        }

        /// <summary>
        /// Ensure list property maximum size.
        /// </summary>
        /// <param name="property">The property</param>
        /// <param name="minValue">The maximum size.</param>
        public static void EnsureListMaxSize(SerializedProperty property, int maxSize)
        {
            if (property.arraySize > maxSize)
            {
                property.arraySize = maxSize;
            }
        }

        /// <summary>
        /// Display a helpbox signaling that property with specified label cannot be null.
        /// </summary>
        /// <param name="label">The label of the property</param>
        public static void NotNullHelpBox(string label)
        {
            EditorGUILayout.HelpBox(label + " cannot be null.", MessageType.Error);
        }

        /// <summary>
        /// Display a helpbox signaling that property value cannot be null.
        /// </summary>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="label">The label of the property</param>
        public static void NotNullHelpBox(object propertyValue, string label)
        {
            if (propertyValue == null)
            {
                NotNullHelpBox(label);
            }
        }

        /// <summary>
        /// Shorthand for NotNullHelpBox(property.objectReferenceValue, property.displayName).
        /// </summary>
        /// <param name="property">The property</param>
        public static void NotNullHelpBox(SerializedProperty property)
        {
            NotNullHelpBox(property.objectReferenceValue, property.displayName);
        }

        /// <summary>
        /// Display a helpbox signaling that property with specified label cannot be empty.
        /// </summary>
        /// <param name="label">The label of the property</param>
        public static void NotEmptyHelpBox(string label)
        {
            EditorGUILayout.HelpBox(label + " cannot be empty.", MessageType.Error);
        }

        /// <summary>
        /// Display a helpbox signaling that property value cannot be null.
        /// </summary>
        /// <param name="propertyValue">The value of the property</param>
        /// <param name="label">The label of the property</param>
        public static void NotEmptyHelpBox(string propertyValue, string label)
        {
            if (propertyValue == "")
            {
                NotEmptyHelpBox(label);
            }
        }

        /// <summary>
        /// Shorthand for NotNullHelpBox(property.stringValue, property.displayName).
        /// </summary>
        /// <param name="property">The property</param>
        public static void NotEmptyHelpBox(SerializedProperty property)
        {
            NotNullHelpBox(property.stringValue, property.displayName);
        }

        /// <summary>
        /// Display a CastAnimation struct in the inspector.
        /// </summary>
        /// <param name="property">The CastAnimation struct.</param>
        public static void CastAnimationPropertyField(SerializedProperty property)
        {
            EditorGUILayout.PropertyField(property);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    // props
                    SerializedProperty castNumProperty = property.FindPropertyRelative("castNum");
                    SerializedProperty prefabProperty = property.FindPropertyRelative("prefab");
                    SerializedProperty prefabHeightProperty = property.FindPropertyRelative("prefabHeight");

                    EditorGUILayout.PropertyField(castNumProperty);
                    EditorGUILayout.PropertyField(prefabProperty);
                    EditorGUILayout.PropertyField(prefabHeightProperty);

                    B4CharacterSystemEditorGUILayout.EnsureMinFloatValue(prefabHeightProperty, 0.1f);
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// Display a Trigger struct in the inspector.
        /// </summary>
        /// <param name="property">The Trigger struct</param>
        /// <param name="levels">The amount of levels associated with the Trigger.</param>
        public static void TriggerPropertyField(SerializedProperty property, int levels)
        {
            EditorGUILayout.PropertyField(property);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    // props
                    SerializedProperty prefabProperty = property.FindPropertyRelative("prefab");
                    SerializedProperty lifetimeProperty = property.FindPropertyRelative("lifetime");
                    SerializedProperty destroyOnNonCharacterCollisionProperty = property.FindPropertyRelative("destroyOnNonCharacterCollision");

                    EditorGUILayout.PropertyField(prefabProperty);

                    EditorGUI.indentLevel++;
                    {
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(lifetimeProperty, levels);

                        EditorGUILayout.PropertyField(destroyOnNonCharacterCollisionProperty);
                    }
                    EditorGUI.indentLevel--;
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// Display a list of level values in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        /// <param name="maxLevel">The maximum level for list size. If omitted, ignored.</param>
        public static void LevelListPropertyField(SerializedProperty property, int maxLevel = -1)
        {
            EditorGUILayoutExtended.ListPropertyField(property, new GUIContent(property.displayName + " (Levels: " + property.arraySize + ")"));

            // make sure at least one level is present
            EnsureListMinSize(property, 1);

            if (maxLevel > 0)
            {
                EnsureListMaxSize(property, maxLevel);
            }

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        EditorGUILayout.PropertyField(property.GetArrayElementAtIndex(i), new GUIContent("Level " + (i + 1)), true);
                    }
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// Display a list of stats in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        public static void ItemStatListPropertyField(SerializedProperty property)
        {
            EditorGUILayoutExtended.ListPropertyField(property, new GUIContent(property.displayName + " (" + property.arraySize + ")"));

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        SerializedProperty statProperty = property.GetArrayElementAtIndex(i);

                        // no special behaviour for nested props, just include children
                        EditorGUILayout.PropertyField(statProperty, new GUIContent("Stat " + (i + 1)), true);
                    }
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// Display a list of conditions in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        /// <param name="label">[Optional] The label for the list property.</param>
        public static void ConditionListPropertyField(SerializedProperty property, string label = null)
        {
            EditorGUILayoutExtended.ListPropertyField(property, new GUIContent((label != null ? label : property.displayName) + " (" + property.arraySize + ")"));

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        SerializedProperty conditionProperty = property.GetArrayElementAtIndex(i);

                        EditorGUILayout.PropertyField(conditionProperty, new GUIContent("Condition " + (i + 1)));

                        if (conditionProperty.isExpanded)
                        {
                            EditorGUI.indentLevel++;
                            {
                                // props
                                SerializedProperty typeProperty = conditionProperty.FindPropertyRelative("type");
                                SerializedProperty targetProperty = conditionProperty.FindPropertyRelative("target");
                                SerializedProperty intValProperty = conditionProperty.FindPropertyRelative("intVal");
                                SerializedProperty floatValProperty = conditionProperty.FindPropertyRelative("floatVal");
                                SerializedProperty boolValProperty = conditionProperty.FindPropertyRelative("boolVal");
                                SerializedProperty intRangeMinValProperty = conditionProperty.FindPropertyRelative("intRangeMinVal");
                                SerializedProperty intRangeMaxValProperty = conditionProperty.FindPropertyRelative("intRangeMaxVal");
                                SerializedProperty statProperty = conditionProperty.FindPropertyRelative("stat");
                                SerializedProperty characterStatusProperty = conditionProperty.FindPropertyRelative("characterStatus");
                                SerializedProperty effectTypeProperty = conditionProperty.FindPropertyRelative("effectType");
                                SerializedProperty itemTemplateProperty = conditionProperty.FindPropertyRelative("itemTemplate");
                                SerializedProperty abilityTemplateProperty = conditionProperty.FindPropertyRelative("abilityTemplate");
                                SerializedProperty effectTemplateProperty = conditionProperty.FindPropertyRelative("effectTemplate");
                                SerializedProperty summonTemplateProperty = conditionProperty.FindPropertyRelative("summonTemplate");
                                SerializedProperty zoneTypeProperty = conditionProperty.FindPropertyRelative("zoneType");
                                SerializedProperty weaponSlotProperty = conditionProperty.FindPropertyRelative("weaponSlot");
                                SerializedProperty weaponTypeProperty = conditionProperty.FindPropertyRelative("weaponType");
                                SerializedProperty armorPartSlotProperty = conditionProperty.FindPropertyRelative("armorPartSlot");
                                SerializedProperty armorPartTypeProperty = conditionProperty.FindPropertyRelative("armorPartType");

                                EditorGUILayout.PropertyField(typeProperty);

                                ConditionType type = (ConditionType)typeProperty.enumValueIndex;

                                EditorGUI.indentLevel++;
                                {
                                    if (type != ConditionType.GameChance && type != ConditionType.GameTime && type != ConditionType.Distance &&
                                        type != ConditionType.DistanceMax && type != ConditionType.DistanceMin && type != ConditionType.DistanceRange &&
                                        type != ConditionType.LevelDifference && type != ConditionType.LevelDifferenceMax && type != ConditionType.LevelDifferenceMin &&
                                        type != ConditionType.LevelDifferenceRange && type != ConditionType.Neutral && type != ConditionType.Friendly && type != ConditionType.Enemy)
                                    {
                                        EditorGUILayout.PropertyField(targetProperty);
                                    }

                                    switch (type)
                                    {
                                        // Game
                                        case ConditionType.GameChance:
                                            EditorGUILayout.IntSlider(intValProperty, 0, 100, new GUIContent("Chance"));

                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intValProperty, 100);
                                            break;
                                        case ConditionType.GameTime:
                                            EditorGUILayout.PropertyField(floatValProperty, new GUIContent("Time"));
                                            break;
                                        // Player || Target
                                        case ConditionType.IsPlayer:
                                        case ConditionType.IsNpc:
                                        case ConditionType.IsSummon:
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                        case ConditionType.Distance:
                                        case ConditionType.Level:
                                        case ConditionType.LevelDifference:
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Value"));
                                            break;
                                        case ConditionType.DistanceMin:
                                        case ConditionType.LevelMin:
                                        case ConditionType.LevelDifferenceMin:
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Min Value"));
                                            break;
                                        case ConditionType.DistanceMax:
                                        case ConditionType.LevelMax:
                                        case ConditionType.LevelDifferenceMax:
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Max Value"));
                                            break;
                                        case ConditionType.DistanceRange:
                                        case ConditionType.LevelRange:
                                        case ConditionType.LevelDifferenceRange:
                                            EditorGUILayout.PropertyField(intRangeMaxValProperty, new GUIContent("Max Value"));
                                            EditorGUILayout.PropertyField(intRangeMinValProperty, new GUIContent("Min Value"));

                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intRangeMinValProperty, intRangeMaxValProperty.intValue);
                                            break;
                                        case ConditionType.Stat:
                                            EditorGUILayout.PropertyField(statProperty, new GUIContent("Stat"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Value"));
                                            break;
                                        case ConditionType.StatMin:
                                            EditorGUILayout.PropertyField(statProperty, new GUIContent("Stat"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Min Value"));
                                            break;
                                        case ConditionType.StatMax:
                                            EditorGUILayout.PropertyField(statProperty, new GUIContent("Stat"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Max Value"));
                                            break;
                                        case ConditionType.StatRange:
                                            EditorGUILayout.PropertyField(statProperty, new GUIContent("Stat"));
                                            EditorGUILayout.PropertyField(intRangeMaxValProperty, new GUIContent("Max Value"));
                                            EditorGUILayout.PropertyField(intRangeMinValProperty, new GUIContent("Min Value"));

                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intRangeMaxValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intRangeMaxValProperty, 100);
                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intRangeMinValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intRangeMinValProperty, intRangeMaxValProperty.intValue);
                                            break;
                                        case ConditionType.HealthPercent:
                                        case ConditionType.EnergyPercent:
                                            EditorGUILayout.IntSlider(intValProperty, 0, 100, new GUIContent("Percent"));

                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intValProperty, 100);
                                            break;
                                        case ConditionType.HealthPercentMin:
                                        case ConditionType.EnergyPercentMin:
                                            EditorGUILayout.IntSlider(intValProperty, 0, 100, new GUIContent("Min Percent"));

                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intValProperty, 100);
                                            break;
                                        case ConditionType.HealthPercentMax:
                                        case ConditionType.EnergyPercentMax:
                                            EditorGUILayout.IntSlider(intValProperty, 0, 100, new GUIContent("Max Percent"));

                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intValProperty, 100);
                                            break;
                                        case ConditionType.HealthPercentRange:
                                        case ConditionType.EnergyPercentRange:
                                            EditorGUILayout.IntSlider(intRangeMaxValProperty, 0, 100, new GUIContent("Max Percent"));
                                            EditorGUILayout.IntSlider(intRangeMinValProperty, 0, 100, new GUIContent("Min Percent"));

                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intRangeMaxValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intRangeMaxValProperty, 100);
                                            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(intRangeMinValProperty, 0);
                                            B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(intRangeMinValProperty, intRangeMaxValProperty.intValue);
                                            break;
                                        case ConditionType.CharacterStatus:
                                            EditorGUILayout.PropertyField(characterStatusProperty, new GUIContent("Character Status"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                        case ConditionType.EffectType:
                                            EditorGUILayout.PropertyField(effectTypeProperty, new GUIContent("Effect Type"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Level"));

                                            if (intValProperty.intValue < 1 && intValProperty.intValue != -1)
                                            {
                                                intValProperty.intValue = 1;
                                            }

                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                        case ConditionType.ItemTemplate:
                                            EditorGUILayout.PropertyField(itemTemplateProperty, new GUIContent("Item Template"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));

                                            B4CharacterSystemEditorGUILayout.NotNullHelpBox(itemTemplateProperty);
                                            break;
                                        case ConditionType.AbilityTemplate:
                                            EditorGUILayout.PropertyField(abilityTemplateProperty, new GUIContent("Ability Template"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Level"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));

                                            B4CharacterSystemEditorGUILayout.NotNullHelpBox(abilityTemplateProperty);
                                            break;
                                        case ConditionType.EffectTemplate:
                                            EditorGUILayout.PropertyField(effectTemplateProperty, new GUIContent("Effect Template"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Level"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));

                                            B4CharacterSystemEditorGUILayout.NotNullHelpBox(effectTemplateProperty);

                                            EffectTemplate effectTemplate = (EffectTemplate)effectTemplateProperty.objectReferenceValue;

                                            if (effectTemplate != null)
                                            {
                                                // only lasting effects are valid
                                                if (!effectTemplate.IsLasting())
                                                {
                                                    effectTemplateProperty.objectReferenceValue = null;
                                                }
                                            }
                                            break;
                                        case ConditionType.SummonTemplate:
                                            EditorGUILayout.PropertyField(summonTemplateProperty, new GUIContent("Summon Template"));
                                            EditorGUILayout.PropertyField(intValProperty, new GUIContent("Level"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));

                                            B4CharacterSystemEditorGUILayout.NotNullHelpBox(summonTemplateProperty);
                                            break;
                                        case ConditionType.Neutral:
                                        case ConditionType.Friendly:
                                        case ConditionType.Enemy:
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                        case ConditionType.ZoneType:
                                            EditorGUILayout.PropertyField(zoneTypeProperty, new GUIContent("Type"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                        case ConditionType.WeaponType:
                                            EditorGUILayout.PropertyField(weaponSlotProperty, new GUIContent("Slot"));
                                            EditorGUILayout.PropertyField(weaponTypeProperty, new GUIContent("Type"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                        case ConditionType.ArmorPartType:
                                            EditorGUILayout.PropertyField(armorPartSlotProperty, new GUIContent("Slot"));
                                            EditorGUILayout.PropertyField(armorPartTypeProperty, new GUIContent("Type"));
                                            EditorGUILayout.PropertyField(boolValProperty, new GUIContent("Negated?"));
                                            break;
                                    }
                                }
                                EditorGUI.indentLevel--;
                            }
                            EditorGUI.indentLevel--;
                        }
                    }
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// Display a list of Effect Templates in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        public static void AbilityEffectTemplateListPropertyField(SerializedProperty property, AbilityTemplate abilityTemplate)
        {
            EditorGUILayoutExtended.ListPropertyField(property, new GUIContent(property.displayName + " (" + property.arraySize + ")"));

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        SerializedProperty effectTemplateProperty = property.GetArrayElementAtIndex(i);
                        string elementName = "Effect Template " + (i + 1);

                        EditorGUILayout.PropertyField(effectTemplateProperty, new GUIContent(elementName));

                        if (abilityTemplate == null)
                        {
                            B4CharacterSystemEditorGUILayout.NotNullHelpBox(abilityTemplate, elementName);
                            break;
                        }

                        EffectTemplate effectTemplate = (EffectTemplate)effectTemplateProperty.objectReferenceValue;

                        if (effectTemplate == null)
                        {
                            B4CharacterSystemEditorGUILayout.NotNullHelpBox(effectTemplate, elementName);
                            break;
                        }

                        if (abilityTemplate.type == AbilityType.Active)
                        {
                            if (effectTemplate.type == EffectType.TeleportPosition && effectTemplate.targetType == TargetType.Self)
                            {
                                EditorGUILayout.HelpBox(
                                    "Positional self-teleport has a very specific behaviour. If you use other effects with it, make sure they are compatible with the behaviour.",
                                    MessageType.Warning
                                );

                                if (abilityTemplate.behaviour != AbilityBehaviour.Positional)
                                {
                                    EditorGUILayout.HelpBox("Effect incompatible with ability behaviour (use AbilityBehaviour.Positional instead).", MessageType.Error);
                                }
                            }

                            if (abilityTemplate.behaviour == AbilityBehaviour.Directional && effectTemplate.targetType != TargetType.Self)
                            {
                                EditorGUILayout.HelpBox("Ability behaviour incompatible with target type (use TargetType.Self instead).", MessageType.Error);
                            }
                        }

                        if (abilityTemplate.type == AbilityType.Passive)
                        {
                            if (effectTemplate.targetType != TargetType.Self)
                            {
                                EditorGUILayout.HelpBox("Effect target type incompatible with ability type.", MessageType.Error);
                            }

                            if (!effectTemplate.IsStatChanger() && !effectTemplate.IsResist())
                            {
                                EditorGUILayout.HelpBox("Effect type incompatible with ability type.", MessageType.Error);
                            }

                            bool hasDuration = false;

                            for (int j = 0; j < effectTemplate.duration.Count; j++)
                            {
                                if (effectTemplate.duration[j] != 0)
                                {
                                    hasDuration = true;
                                    break;
                                }
                            }

                            if (hasDuration)
                            {
                                EditorGUILayout.HelpBox("Effect duration should be 0 to be compatible with ability type.", MessageType.Error);
                            }
                        }
                    }
                }
                EditorGUI.indentLevel--;
            }
        }

        /// <summary>
        /// Display a Level Template in the inspector.
        /// </summary>
        /// <param name="property">The property to display.</param>
        /// <param name="isInArray">The property is part of an array.</param>
        public static void LevelTemplatePropertyField(SerializedProperty property, bool isInArray = false)
        {
            LevelTemplatePropertyField(property, null, isInArray);
        }

        /// <summary>
        /// Display a Level Template in the inspector.
        /// </summary>
        /// <param name="property">The property to display.</param>
        /// <param name="label">Optional label to use. If not specified the label of the property itself is used. Use GUIContent.none to not display a label at all.</param>
        /// <param name="isInArray">The property is part of an array.</param>
        public static void LevelTemplatePropertyField(SerializedProperty property, GUIContent label, bool isInArray = false)
        {
            Action relativePropsDisplay = () =>
            {
                SerializedProperty templateProperty = property.FindPropertyRelative("template");
                SerializedProperty levelProperty = property.FindPropertyRelative("level");

                EditorGUILayout.PropertyField(templateProperty);

                UnityEngine.Object template = templateProperty.objectReferenceValue;

                if (template != null)
                {
                    EditorGUILayout.PropertyField(levelProperty);

                    // allow only -1 (for omitting level), else min 1
                    if (levelProperty.intValue < 1 && levelProperty.intValue != -1)
                    {
                        levelProperty.intValue = 1;
                    }

                    if (template is AbilityTemplate)
                    {
                        B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(levelProperty, ((AbilityTemplate)template).levels);
                    }

                    if (template is NpcTemplate)
                    {
                        B4CharacterSystemEditorGUILayout.EnsureMaxIntValue(levelProperty, ((NpcTemplate)template).maxLevel);
                    }
                }

                B4CharacterSystemEditorGUILayout.NotNullHelpBox(templateProperty);
            };

            // add indentation if property is part of an array
            if (isInArray)
            {
                EditorGUILayout.PropertyField(property, label);

                if (property.isExpanded)
                {
                    EditorGUI.indentLevel++;
                    {
                        relativePropsDisplay();
                    }
                    EditorGUI.indentLevel--;
                }
            }
            else
            {
                relativePropsDisplay();
            }
        }

        /// <summary>
        /// Display a list of Level Template in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        public static void LevelTemplateListPropertyField(SerializedProperty property)
        {
            LevelTemplateListPropertyField(property, null, null);
        }

        /// <summary>
        /// Display a list of Level Template in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        /// <param name="label">Optional label to use. If not specified the label of the property itself is used. Use GUIContent.none to not display a label at all.</param>
        public static void LevelTemplateListPropertyField(SerializedProperty property, GUIContent label)
        {
            LevelTemplateListPropertyField(property, label, null);
        }

        /// <summary>
        /// Display a list of Level Template in the inspector.
        /// </summary>
        /// <param name="property">The list property to display.</param>
        /// <param name="label">Optional label to use. If not specified the label of the property itself is used. Use GUIContent.none to not display a label at all.</param>
        /// <param name="childLabelText">Optional child label to use. If not specified the label of the property itself is used. The element number gets appended to the given text.</param>
        public static void LevelTemplateListPropertyField(SerializedProperty property, GUIContent label, string childLabelText)
        {
            EditorGUILayoutExtended.ListPropertyField(property, label != null ? label : new GUIContent(property.displayName + " (" + property.arraySize + ")"));

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;
                {
                    for (int i = 0; i < property.arraySize; i++)
                    {
                        SerializedProperty listElementProperty = property.GetArrayElementAtIndex(i);

                        if (childLabelText != null)
                        {
                            string labelText = childLabelText + " " + (i + 1);

                            LevelTemplatePropertyField(listElementProperty, new GUIContent(labelText), true);
                        }
                        else
                        {
                            LevelTemplatePropertyField(listElementProperty, true);
                        }
                    }
                }
                EditorGUI.indentLevel--;
            }
        }
    }
}