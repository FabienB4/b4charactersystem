using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(NpcTemplate), true)]
    public class NpcTemplateEditor : CharacterTemplateEditor
    {
        // Npc Specific
        SerializedProperty m_PrefabProperty;
        SerializedProperty m_LevelProperty;
        SerializedProperty m_SpawnOnStartProperty;
        SerializedProperty m_SpawnPositionProperty;
        SerializedProperty m_AttackableProperty;
        SerializedProperty m_RespawnOnDeathProperty;
        SerializedProperty m_RespawnDelayProperty;

        AnimBool m_ShowAttackablePropsTransition = new AnimBool();

        protected override void OnEnable()
        {
            base.OnEnable();

            // Npc Specific
            m_PrefabProperty = serializedObject.FindProperty("prefab");
            m_LevelProperty = serializedObject.FindProperty("level");
            m_SpawnOnStartProperty = serializedObject.FindProperty("spawnOnStart");
            m_SpawnPositionProperty = serializedObject.FindProperty("spawnPosition");
            m_AttackableProperty = serializedObject.FindProperty("attackable");
            m_RespawnOnDeathProperty = serializedObject.FindProperty("respawnOnDeath");
            m_RespawnDelayProperty = serializedObject.FindProperty("respawnDelay");

            m_ShowAttackablePropsTransition.value = m_AttackableProperty.boolValue;

            m_ShowAttackablePropsTransition.valueChanged.AddListener(Repaint);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_ShowAttackablePropsTransition.valueChanged.RemoveListener(Repaint);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            EditorGUILayout.PropertyField(m_PrefabProperty);
            B4CharacterSystemEditorGUILayout.NotNullHelpBox(m_PrefabProperty.objectReferenceValue, m_PrefabProperty.displayName);

            EditorGUILayout.PropertyField(m_LevelProperty);

            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_LevelProperty, 1);

            EditorGUILayout.PropertyField(m_SpawnOnStartProperty);

            if (m_SpawnOnStartProperty.boolValue)
            {
                EditorGUI.indentLevel++;
                {
                    EditorGUILayout.PropertyField(m_SpawnPositionProperty);
                }
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.PropertyField(m_AttackableProperty);

            m_ShowAttackablePropsTransition.target = (!m_AttackableProperty.hasMultipleDifferentValues && m_AttackableProperty.boolValue);

            if (EditorGUILayout.BeginFadeGroup(m_ShowAttackablePropsTransition.faded))
            {
                EditorGUI.indentLevel++;

                EditorGUILayout.PropertyField(m_RespawnOnDeathProperty);

                if (m_RespawnOnDeathProperty.boolValue)
                {
                    EditorGUILayout.PropertyField(m_RespawnDelayProperty);
                }

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndFadeGroup();

            serializedObject.ApplyModifiedProperties();
        }
    }
}