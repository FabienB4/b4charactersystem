using B4Games.CharacterSystem.Configs.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(PlayerConfigTemplate), true)]
    public class PlayerConfigTemplateEditor : ConfigTemplateEditor
    {
        protected override void OnEnable()
        {
            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }
    }
}