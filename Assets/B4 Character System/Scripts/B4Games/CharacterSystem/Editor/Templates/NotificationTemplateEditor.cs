using B4Games.CharacterSystem.Models.Notifications.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(NotificationTemplate), true)]
    public class NotificationTemplateEditor : UnityEditor.Editor
    {
        SerializedProperty m_IdProperty;
        SerializedProperty m_MessageProperty;

        protected virtual void OnEnable()
        {
            m_IdProperty = serializedObject.FindProperty("id");
            m_MessageProperty = serializedObject.FindProperty("message");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(m_IdProperty);
            EditorGUILayout.PropertyField(m_MessageProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}