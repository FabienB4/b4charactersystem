using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Effects.Templates;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(EffectTemplate), true)]
    public class EffectTemplateEditor : UnityEditor.Editor
    {
        // Display
        SerializedProperty m_EffectNameProperty;
        SerializedProperty m_DescriptionProperty;
        SerializedProperty m_IconProperty;

        // Behaviour
        SerializedProperty m_TargetTypeProperty;
        SerializedProperty m_TypeProperty;

        // Values
        SerializedProperty m_PowerProperty;
        SerializedProperty m_DurationProperty;
        SerializedProperty m_RepeatOverTimeProperty;
        SerializedProperty m_TickCountProperty;
        SerializedProperty m_ConditionsProperty;

        // Buff/Debuff Specific
        SerializedProperty m_BuffStatProperty;

        // Cleanse Specific
        SerializedProperty m_CleanseEffectTypeProperty;

        // Resurrection Specific
        SerializedProperty m_ResurrectionHealthProperty;
        SerializedProperty m_ResurrectionEnergyProperty;

        // Teleport Specific
        SerializedProperty m_TeleportPositionProperty;
        SerializedProperty m_TeleportDirectionProperty;
        SerializedProperty m_TeleportDirectionForceProperty;
        SerializedProperty m_TeleportDirectionForceModeProperty;

        // Summon Specific
        SerializedProperty m_SummonTemplateProperty;

        AnimBool m_ShowEffectTypePropsTransition = new AnimBool();
        AnimBool m_ShowPowerPropTransition = new AnimBool();

        AnimBool m_ShowDurationPropTransition = new AnimBool();
        AnimBool m_ShowRepeatOverTimePropTransition = new AnimBool();
        AnimBool m_ShowRepeatOverTimePropsTransition = new AnimBool();

        protected virtual void OnEnable()
        {
            // Display
            m_EffectNameProperty = serializedObject.FindProperty("effectName");
            m_DescriptionProperty = serializedObject.FindProperty("description");
            m_IconProperty = serializedObject.FindProperty("icon");

            // Behaviour
            m_TargetTypeProperty = serializedObject.FindProperty("targetType");
            m_TypeProperty = serializedObject.FindProperty("type");

            // Values
            m_PowerProperty = serializedObject.FindProperty("power");
            m_DurationProperty = serializedObject.FindProperty("duration");
            m_RepeatOverTimeProperty = serializedObject.FindProperty("repeatOverTime");
            m_TickCountProperty = serializedObject.FindProperty("tickCount");
            m_ConditionsProperty = serializedObject.FindProperty("conditions");

            // Buff/Debuff Specific
            m_BuffStatProperty = serializedObject.FindProperty("buffStat");

            // Cleanse Specific
            m_CleanseEffectTypeProperty = serializedObject.FindProperty("cleanseEffectType");

            // Resurrection Specific
            m_ResurrectionHealthProperty = serializedObject.FindProperty("resurrectionHealth");
            m_ResurrectionEnergyProperty = serializedObject.FindProperty("resurrectionEnergy");

            // Teleport Specific
            m_TeleportPositionProperty = serializedObject.FindProperty("teleportPosition");
            m_TeleportDirectionProperty = serializedObject.FindProperty("teleportDirection");
            m_TeleportDirectionForceProperty = serializedObject.FindProperty("teleportDirectionForce");
            m_TeleportDirectionForceModeProperty = serializedObject.FindProperty("teleportDirectionForceMode");

            // Summon Specific
            m_SummonTemplateProperty = serializedObject.FindProperty("summonTemplate");

            EffectType type = GetEffectType(m_TypeProperty);
            m_ShowEffectTypePropsTransition.value = ShowEffectTypeProps(type);

            m_ShowEffectTypePropsTransition.valueChanged.AddListener(Repaint);

            m_ShowPowerPropTransition.value = ShowPowerProp(type);

            m_ShowPowerPropTransition.valueChanged.AddListener(Repaint);

            m_ShowDurationPropTransition.value = ShowDurationProp(type);

            m_ShowDurationPropTransition.valueChanged.AddListener(Repaint);

            m_ShowRepeatOverTimePropTransition.value = ShowRepeatOverTimeProp(type);

            m_ShowRepeatOverTimePropTransition.valueChanged.AddListener(Repaint);

            m_ShowRepeatOverTimePropsTransition.value = m_RepeatOverTimeProperty.boolValue;

            m_ShowRepeatOverTimePropsTransition.valueChanged.AddListener(Repaint);
        }

        protected virtual void OnDisable()
        {
            m_ShowEffectTypePropsTransition.valueChanged.RemoveListener(Repaint);
            m_ShowPowerPropTransition.valueChanged.RemoveListener(Repaint);
            m_ShowDurationPropTransition.valueChanged.RemoveListener(Repaint);
            m_ShowRepeatOverTimePropTransition.valueChanged.RemoveListener(Repaint);
            m_ShowRepeatOverTimePropsTransition.valueChanged.RemoveListener(Repaint);
        }

        static TargetType GetTargetType(SerializedProperty targetType)
        {
            return (TargetType)targetType.enumValueIndex;
        }

        static EffectType GetEffectType(SerializedProperty effectType)
        {
            return (EffectType)effectType.enumValueIndex;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            // Display
            EditorGUILayout.PropertyField(m_EffectNameProperty);
            EditorGUILayout.PropertyField(m_DescriptionProperty);
            EditorGUILayout.PropertyField(m_IconProperty);

            // Behaviour
            EditorGUILayout.PropertyField(m_TargetTypeProperty);

            TargetType targetType = GetTargetType(m_TargetTypeProperty);

            EditorGUILayout.PropertyField(m_TypeProperty);

            EffectType effectType = GetEffectType(m_TypeProperty);

            // Target type for Summon is always None
            if (effectType == EffectType.Summon)
            {
                m_TargetTypeProperty.enumValueIndex = (int)TargetType.None;
            }

            // Target type for non-Summon is never None
            if (effectType != EffectType.Summon)
            {
                if (targetType == TargetType.None)
                {
                    m_TargetTypeProperty.enumValueIndex = (int)TargetType.Any;
                }
            }

            // Target type for Resurrection & SummonCharacter can never be None, Self or Any
            if (effectType == EffectType.Resurrection || effectType == EffectType.SummonCharacter)
            {
                if (targetType == TargetType.None || targetType == TargetType.Self || targetType == TargetType.Any)
                {
                    m_TargetTypeProperty.enumValueIndex = (int)TargetType.NotSelf;
                }
            }

            m_ShowEffectTypePropsTransition.target = (!m_TypeProperty.hasMultipleDifferentValues && ShowEffectTypeProps(effectType));

            if (EditorGUILayout.BeginFadeGroup(m_ShowEffectTypePropsTransition.faded))
            {
                EditorGUI.indentLevel++;

                switch (effectType)
                {
                    case EffectType.Buff:
                    case EffectType.Debuff:
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_BuffStatProperty);
                        break;
                    case EffectType.CleanseEffectType:
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_CleanseEffectTypeProperty);
                        break;
                    case EffectType.Resurrection:
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ResurrectionHealthProperty);
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ResurrectionEnergyProperty);

                        B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_ResurrectionHealthProperty, 1);
                        B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_ResurrectionEnergyProperty, 0);
                        break;
                    case EffectType.TeleportPosition:
                        if (targetType == TargetType.Any)
                        {
                            m_TargetTypeProperty.enumValueIndex = (int)TargetType.NotSelf;
                        }

                        if (targetType != TargetType.Self)
                        {
                            B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_TeleportPositionProperty);
                        }
                        break;
                    case EffectType.TeleportDirection:
                        EditorGUILayout.PropertyField(m_TeleportDirectionProperty);
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_TeleportDirectionForceProperty);
                        EditorGUILayout.PropertyField(m_TeleportDirectionForceModeProperty);
                        break;
                    case EffectType.Summon:
                        B4CharacterSystemEditorGUILayout.LevelTemplateListPropertyField(m_SummonTemplateProperty, null, "Level");
                        // make sure at least one level is present
                        B4CharacterSystemEditorGUILayout.EnsureListMinSize(m_SummonTemplateProperty, 1);
                        break;
                }

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndFadeGroup();

            // Values
            m_ShowPowerPropTransition.target = (!m_TypeProperty.hasMultipleDifferentValues && ShowPowerProp(effectType));

            if (EditorGUILayout.BeginFadeGroup(m_ShowPowerPropTransition.faded))
            {
                B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_PowerProperty);
            }

            EditorGUILayout.EndFadeGroup();

            m_ShowDurationPropTransition.target = (!m_TypeProperty.hasMultipleDifferentValues && ShowDurationProp(effectType));

            if (EditorGUILayout.BeginFadeGroup(m_ShowDurationPropTransition.faded))
            {
                B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_DurationProperty);
            }

            EditorGUILayout.EndFadeGroup();

            m_ShowRepeatOverTimePropTransition.target = (!m_TypeProperty.hasMultipleDifferentValues && ShowRepeatOverTimeProp(effectType));

            if (EditorGUILayout.BeginFadeGroup(m_ShowRepeatOverTimePropTransition.faded))
            {
                EditorGUILayout.PropertyField(m_RepeatOverTimeProperty);

                m_ShowRepeatOverTimePropsTransition.target = (!m_RepeatOverTimeProperty.hasMultipleDifferentValues && m_RepeatOverTimeProperty.boolValue);

                if (EditorGUILayout.BeginFadeGroup(m_ShowRepeatOverTimePropsTransition.faded))
                {
                    B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_TickCountProperty);

                    // when repeat over time is activated, duration and tick count are mandatory
                    B4CharacterSystemEditorGUILayout.EnsureMinFloatValue(m_DurationProperty, 1);
                    B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_TickCountProperty, 1);
                }

                EditorGUILayout.EndFadeGroup();
            }

            EditorGUILayout.EndFadeGroup();

            EditorGUILayout.HelpBox(
                "If one of the previous ability level related lists has a size that doesn't match the number of levels, the last value of the list is used for the remaining levels.",
                MessageType.Info
            );

            B4CharacterSystemEditorGUILayout.ConditionListPropertyField(m_ConditionsProperty);

            serializedObject.ApplyModifiedProperties();
        }

        private bool ShowEffectTypeProps(EffectType type)
        {
            switch (type)
            {
                case EffectType.Buff:
                case EffectType.Debuff:
                case EffectType.CleanseEffectType:
                case EffectType.Resurrection:
                case EffectType.TeleportPosition:
                case EffectType.TeleportDirection:
                case EffectType.Summon:
                    return true;
                default:
                    return false;
            }
        }

        private bool ShowPowerProp(EffectType type)
        {
            switch (type)
            {
                case EffectType.Damage:
                case EffectType.DrainHealth:
                case EffectType.DrainEnergy:
                case EffectType.RestoreHealth:
                case EffectType.RestoreEnergy:
                case EffectType.Buff:
                case EffectType.Debuff:
                case EffectType.CleanseAll:
                case EffectType.CleanseDebuffs:
                case EffectType.CleanseDisablers:
                case EffectType.CleanseEffectType:
                case EffectType.Aggression:
                case EffectType.Fear:
                case EffectType.Paralyze:
                case EffectType.Sleep:
                case EffectType.Snare:
                case EffectType.Stun:
                case EffectType.ResistAggression:
                case EffectType.ResistFear:
                case EffectType.ResistParalyze:
                case EffectType.ResistSleep:
                case EffectType.ResistSnare:
                case EffectType.ResistStun:
                    return true;
                default:
                    return false;
            }
        }

        private bool ShowDurationProp(EffectType type)
        {
            switch (type)
            {
                case EffectType.Damage:
                case EffectType.DrainHealth:
                case EffectType.DrainEnergy:
                case EffectType.RestoreHealth:
                case EffectType.RestoreEnergy:
                case EffectType.Buff:
                case EffectType.Debuff:
                case EffectType.Aggression:
                case EffectType.Fear:
                case EffectType.Paralyze:
                case EffectType.Sleep:
                case EffectType.Snare:
                case EffectType.Stun:
                case EffectType.ResistAggression:
                case EffectType.ResistFear:
                case EffectType.ResistParalyze:
                case EffectType.ResistSleep:
                case EffectType.ResistSnare:
                case EffectType.ResistStun:
                case EffectType.Summon:
                    return true;
                default:
                    return false;
            }
        }

        private bool ShowRepeatOverTimeProp(EffectType type)
        {
            switch (type)
            {
                case EffectType.Damage:
                case EffectType.DrainHealth:
                case EffectType.DrainEnergy:
                case EffectType.RestoreHealth:
                case EffectType.RestoreEnergy:
                    return true;
                default:
                    return false;
            }
        }
    }
}