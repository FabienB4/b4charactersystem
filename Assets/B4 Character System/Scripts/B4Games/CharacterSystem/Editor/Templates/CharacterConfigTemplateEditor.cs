using B4Games.CharacterSystem.Configs.Templates;
using B4Games.CharacterSystem.Editor.Helpers;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(CharacterConfigTemplate), true)]
    public class CharacterConfigTemplateEditor : ConfigTemplateEditor
    {
        // Stats
        SerializedProperty m_StatDisplayMultiplierProperty;
        SerializedProperty m_HealthRegeneratonTicksPerSecondProperty;
        SerializedProperty m_EnergyRegeneratonTicksPerSecondProperty;
        SerializedProperty m_HealthPercentOnReviveProperty;
        SerializedProperty m_EnergyPercentOnReviveProperty;

        // Stat Caps
        SerializedProperty m_MaxHealthCapProperty;
        SerializedProperty m_MaxEnergyCapProperty;
        SerializedProperty m_HealthRegenerationCapProperty;
        SerializedProperty m_EnergyRegenerationCapProperty;
        SerializedProperty m_AttackCapProperty;
        SerializedProperty m_DefenseCapProperty;
        SerializedProperty m_SpeedCapProperty;
        SerializedProperty m_HealPowerCapProperty;
        SerializedProperty m_RegenerationPowerCapProperty;

        // Movements
        SerializedProperty m_WalkingSpeedMultiplierProperty;
        SerializedProperty m_MovingTurnSpeedProperty;
        SerializedProperty m_StationaryTurnSpeedProperty;
        SerializedProperty m_JumpPowerProperty;
        SerializedProperty m_JumpGravityMultiplierProperty;
        SerializedProperty m_GroundCheckDistanceProperty;

        // Summons
        SerializedProperty m_ReplaceSummonOnMaxCountProperty;
        SerializedProperty m_MaxSummonDistanceFromOwnerProperty;

        protected override void OnEnable()
        {
            base.OnEnable();

            // Stats
            m_StatDisplayMultiplierProperty = serializedObject.FindProperty("statDisplayMultiplier");
            m_HealthRegeneratonTicksPerSecondProperty = serializedObject.FindProperty("healthRegeneratonTicksPerSecond");
            m_EnergyRegeneratonTicksPerSecondProperty = serializedObject.FindProperty("energyRegeneratonTicksPerSecond");
            m_HealthPercentOnReviveProperty = serializedObject.FindProperty("healthPercentOnRevive");
            m_EnergyPercentOnReviveProperty = serializedObject.FindProperty("energyPercentOnRevive");

            // Stat Caps
            m_MaxHealthCapProperty = serializedObject.FindProperty("maxHealthCap");
            m_MaxEnergyCapProperty = serializedObject.FindProperty("maxEnergyCap");
            m_HealthRegenerationCapProperty = serializedObject.FindProperty("healthRegenerationCap");
            m_EnergyRegenerationCapProperty = serializedObject.FindProperty("energyRegenerationCap");
            m_AttackCapProperty = serializedObject.FindProperty("attackCap");
            m_DefenseCapProperty = serializedObject.FindProperty("defenseCap");
            m_SpeedCapProperty = serializedObject.FindProperty("speedCap");
            m_HealPowerCapProperty = serializedObject.FindProperty("healPowerCap");
            m_RegenerationPowerCapProperty = serializedObject.FindProperty("regenerationPowerCap");

            // Movements
            m_WalkingSpeedMultiplierProperty = serializedObject.FindProperty("walkingSpeedMultiplier");
            m_MovingTurnSpeedProperty = serializedObject.FindProperty("movingTurnSpeed");
            m_StationaryTurnSpeedProperty = serializedObject.FindProperty("stationaryTurnSpeed");
            m_JumpPowerProperty = serializedObject.FindProperty("jumpPower");
            m_JumpGravityMultiplierProperty = serializedObject.FindProperty("jumpGravityMultiplier");
            m_GroundCheckDistanceProperty = serializedObject.FindProperty("groundCheckDistance");

            // Summons
            m_ReplaceSummonOnMaxCountProperty = serializedObject.FindProperty("replaceSummonOnMaxCount");
            m_MaxSummonDistanceFromOwnerProperty = serializedObject.FindProperty("maxSummonDistanceFromOwner");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            // Stats
            EditorGUILayout.PropertyField(m_StatDisplayMultiplierProperty);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_StatDisplayMultiplierProperty, 1);
            EditorGUILayout.PropertyField(m_HealthRegeneratonTicksPerSecondProperty);
            EditorGUILayout.PropertyField(m_EnergyRegeneratonTicksPerSecondProperty);
            EditorGUILayout.PropertyField(m_HealthPercentOnReviveProperty);
            EditorGUILayout.PropertyField(m_EnergyPercentOnReviveProperty);

            // Stat Caps
            EditorGUILayout.PropertyField(m_MaxHealthCapProperty);
            EditorGUILayout.PropertyField(m_MaxEnergyCapProperty);
            EditorGUILayout.PropertyField(m_HealthRegenerationCapProperty);
            EditorGUILayout.PropertyField(m_EnergyRegenerationCapProperty);
            EditorGUILayout.PropertyField(m_AttackCapProperty);
            EditorGUILayout.PropertyField(m_DefenseCapProperty);
            EditorGUILayout.PropertyField(m_SpeedCapProperty);
            EditorGUILayout.PropertyField(m_HealPowerCapProperty);
            EditorGUILayout.PropertyField(m_RegenerationPowerCapProperty);

            // Movements
            EditorGUILayout.PropertyField(m_WalkingSpeedMultiplierProperty);
            EditorGUILayout.PropertyField(m_MovingTurnSpeedProperty);
            EditorGUILayout.PropertyField(m_StationaryTurnSpeedProperty);
            EditorGUILayout.PropertyField(m_JumpPowerProperty);
            EditorGUILayout.PropertyField(m_JumpGravityMultiplierProperty);
            EditorGUILayout.PropertyField(m_GroundCheckDistanceProperty);

            // Summons
            EditorGUILayout.PropertyField(m_ReplaceSummonOnMaxCountProperty);
            EditorGUILayout.PropertyField(m_MaxSummonDistanceFromOwnerProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}