using B4Games.CharacterSystem.Configs.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(ConfigTemplate), true)]
    public class ConfigTemplateEditor : UnityEditor.Editor
    {
        protected virtual void OnEnable()
        {

        }

        protected virtual void OnDisable()
        {

        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            serializedObject.ApplyModifiedProperties();
        }
    }
}