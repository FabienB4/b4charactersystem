using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors.Templates;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(CharacterTemplate), true)]
    public class CharacterTemplateEditor : UnityEditor.Editor
    {
        SerializedProperty m_CharacterNameProperty;

        // Base Stats
        SerializedProperty m_BaseMaxHealthProperty;
        SerializedProperty m_BaseMaxEnergyProperty;
        SerializedProperty m_BaseHealthRegenerationProperty;
        SerializedProperty m_BaseEnergyRegenerationProperty;
        SerializedProperty m_BaseAttackProperty;
        SerializedProperty m_BaseDefenseProperty;
        SerializedProperty m_BaseSpeedProperty;
        SerializedProperty m_BaseHealPowerProperty;
        SerializedProperty m_BaseRegenerationPowerProperty;

        // Limits
        SerializedProperty m_MaxLevelProperty;
        SerializedProperty m_MaxSummonCountProperty;

        // Animations
        SerializedProperty m_RunCycleLegOffsetProperty;

        // Starter Abilities
        SerializedProperty m_AbilitiesProperty;

        // Starter Equipment
        SerializedProperty m_PrimaryWeaponProperty;
        SerializedProperty m_SecondaryWeaponProperty;

        SerializedProperty m_HeadArmorPartProperty;
        SerializedProperty m_TorsoArmorPartProperty;
        SerializedProperty m_HandsArmorPartProperty;
        SerializedProperty m_LegsArmorPartProperty;
        SerializedProperty m_FeetArmorPartProperty;

        protected virtual void OnEnable()
        {
            m_CharacterNameProperty = serializedObject.FindProperty("characterName");

            // Base Stats
            m_BaseMaxHealthProperty = serializedObject.FindProperty("baseMaxHealth");
            m_BaseMaxEnergyProperty = serializedObject.FindProperty("baseMaxEnergy");
            m_BaseHealthRegenerationProperty = serializedObject.FindProperty("baseHealthRegeneration");
            m_BaseEnergyRegenerationProperty = serializedObject.FindProperty("baseEnergyRegeneration");
            m_BaseAttackProperty = serializedObject.FindProperty("baseAttack");
            m_BaseDefenseProperty = serializedObject.FindProperty("baseDefense");
            m_BaseSpeedProperty = serializedObject.FindProperty("baseSpeed");
            m_BaseHealPowerProperty = serializedObject.FindProperty("baseHealPower");
            m_BaseRegenerationPowerProperty = serializedObject.FindProperty("baseRegenerationPower");

            // Limits
            m_MaxLevelProperty = serializedObject.FindProperty("maxLevel");
            m_MaxSummonCountProperty = serializedObject.FindProperty("maxSummonCount");

            // Animations
            m_RunCycleLegOffsetProperty = serializedObject.FindProperty("runCycleLegOffset");

            // Starter Abilities
            m_AbilitiesProperty = serializedObject.FindProperty("abilities");

            // Starter Equipment
            m_PrimaryWeaponProperty = serializedObject.FindProperty("primaryWeapon");
            m_SecondaryWeaponProperty = serializedObject.FindProperty("secondaryWeapon");

            m_HeadArmorPartProperty = serializedObject.FindProperty("headArmorPart");
            m_TorsoArmorPartProperty = serializedObject.FindProperty("torsoArmorPart");
            m_HandsArmorPartProperty = serializedObject.FindProperty("handsArmorPart");
            m_LegsArmorPartProperty = serializedObject.FindProperty("legsArmorPart");
            m_FeetArmorPartProperty = serializedObject.FindProperty("feetArmorPart");
        }

        protected virtual void OnDisable()
        {
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(m_CharacterNameProperty);

            // Base Stats
            EditorGUILayout.PropertyField(m_BaseMaxHealthProperty);
            EditorGUILayout.PropertyField(m_BaseMaxEnergyProperty);
            EditorGUILayout.PropertyField(m_BaseHealthRegenerationProperty);
            EditorGUILayout.PropertyField(m_BaseEnergyRegenerationProperty);
            EditorGUILayout.PropertyField(m_BaseAttackProperty);
            EditorGUILayout.PropertyField(m_BaseDefenseProperty);
            EditorGUILayout.PropertyField(m_BaseSpeedProperty);
            EditorGUILayout.PropertyField(m_BaseHealPowerProperty);
            EditorGUILayout.PropertyField(m_BaseRegenerationPowerProperty);

            // Limits
            EditorGUILayout.PropertyField(m_MaxLevelProperty);
            EditorGUILayout.PropertyField(m_MaxSummonCountProperty);

            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseMaxHealthProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseMaxEnergyProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseHealthRegenerationProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseEnergyRegenerationProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseAttackProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseDefenseProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseSpeedProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseHealPowerProperty, 1);
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_BaseRegenerationPowerProperty, 1);

            // Animations
            EditorGUILayout.PropertyField(m_RunCycleLegOffsetProperty);

            // Starter Abilities
            B4CharacterSystemEditorGUILayout.LevelTemplateListPropertyField(m_AbilitiesProperty, null, "Ability");

            // Starter Equipment
            EditorGUILayout.PropertyField(m_PrimaryWeaponProperty);

            WeaponTemplate primaryWeapon = (WeaponTemplate)m_PrimaryWeaponProperty.objectReferenceValue;

            if (primaryWeapon != null && primaryWeapon.slot != WeaponSlot.Primary)
            {
                m_PrimaryWeaponProperty.objectReferenceValue = null;
            }

            EditorGUILayout.PropertyField(m_SecondaryWeaponProperty);

            WeaponTemplate secondaryWeapon = (WeaponTemplate)m_SecondaryWeaponProperty.objectReferenceValue;

            if (secondaryWeapon != null && secondaryWeapon.slot != WeaponSlot.Secondary)
            {
                m_SecondaryWeaponProperty.objectReferenceValue = null;
            }

            EditorGUILayout.PropertyField(m_HeadArmorPartProperty);

            ArmorPartTemplate headArmorPart = (ArmorPartTemplate)m_HeadArmorPartProperty.objectReferenceValue;

            if (headArmorPart != null && headArmorPart.slot != ArmorPartSlot.Head)
            {
                m_HeadArmorPartProperty.objectReferenceValue = null;
            }

            EditorGUILayout.PropertyField(m_TorsoArmorPartProperty);

            ArmorPartTemplate torsoArmorPart = (ArmorPartTemplate)m_TorsoArmorPartProperty.objectReferenceValue;

            if (torsoArmorPart != null && torsoArmorPart.slot != ArmorPartSlot.Torso)
            {
                m_TorsoArmorPartProperty.objectReferenceValue = null;
            }

            EditorGUILayout.PropertyField(m_HandsArmorPartProperty);

            ArmorPartTemplate handsArmorPart = (ArmorPartTemplate)m_HandsArmorPartProperty.objectReferenceValue;

            if (handsArmorPart != null && handsArmorPart.slot != ArmorPartSlot.Hands)
            {
                m_HandsArmorPartProperty.objectReferenceValue = null;
            }

            EditorGUILayout.PropertyField(m_LegsArmorPartProperty);

            ArmorPartTemplate legsArmorPart = (ArmorPartTemplate)m_LegsArmorPartProperty.objectReferenceValue;

            if (legsArmorPart != null && legsArmorPart.slot != ArmorPartSlot.Legs)
            {
                m_LegsArmorPartProperty.objectReferenceValue = null;
            }

            EditorGUILayout.PropertyField(m_FeetArmorPartProperty);

            ArmorPartTemplate feetArmorPart = (ArmorPartTemplate)m_FeetArmorPartProperty.objectReferenceValue;

            if (feetArmorPart != null && feetArmorPart.slot != ArmorPartSlot.Feet)
            {
                m_FeetArmorPartProperty.objectReferenceValue = null;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}