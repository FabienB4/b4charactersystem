using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(ItemTemplate), true)]
    public class ItemTemplateEditor : UnityEditor.Editor
    {
        // Display
        SerializedProperty m_ItemNameProperty;
        SerializedProperty m_DescriptionProperty;
        SerializedProperty m_IconProperty;

        // Statuses
        SerializedProperty m_StorableProperty;
        SerializedProperty m_DestroyableProperty;
        SerializedProperty m_StackableProperty;
        SerializedProperty m_NpcOnlyProperty;
        SerializedProperty m_ConditionsProperty;

        protected virtual void OnEnable()
        {
            // Display
            m_ItemNameProperty = serializedObject.FindProperty("itemName");
            m_DescriptionProperty = serializedObject.FindProperty("description");
            m_IconProperty = serializedObject.FindProperty("icon");

            // Statuses
            m_StorableProperty = serializedObject.FindProperty("storable");
            m_DestroyableProperty = serializedObject.FindProperty("destroyable");
            m_StackableProperty = serializedObject.FindProperty("stackable");
            m_NpcOnlyProperty = serializedObject.FindProperty("npcOnly");
            m_ConditionsProperty = serializedObject.FindProperty("conditions");
        }

        protected virtual void OnDisable()
        {
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            // Display
            EditorGUILayout.PropertyField(m_ItemNameProperty);
            EditorGUILayout.PropertyField(m_DescriptionProperty);
            EditorGUILayout.PropertyField(m_IconProperty);

            // Statuses
            EditorGUILayout.PropertyField(m_StorableProperty);
            EditorGUILayout.PropertyField(m_DestroyableProperty);

            EditorGUILayout.PropertyField(m_StackableProperty);

            if (serializedObject.targetObject is EquippableTemplate)
            {
                m_StackableProperty.boolValue = false;
            }

            EditorGUILayout.PropertyField(m_NpcOnlyProperty);

            if (serializedObject.targetObject is EquippableTemplate)
            {
                B4CharacterSystemEditorGUILayout.ConditionListPropertyField(m_ConditionsProperty, "Equip Conditions");
            }
            else if (serializedObject.targetObject is ConsumableTemplate)
            {
                B4CharacterSystemEditorGUILayout.ConditionListPropertyField(m_ConditionsProperty, "Use Conditions");
            }
            else
            {
                B4CharacterSystemEditorGUILayout.ConditionListPropertyField(m_ConditionsProperty);
            }

            // stackable are always destroyable
            if (m_StackableProperty.boolValue)
            {
                m_DestroyableProperty.boolValue = true;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}