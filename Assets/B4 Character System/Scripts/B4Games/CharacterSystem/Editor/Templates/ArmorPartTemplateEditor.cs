using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(ArmorPartTemplate), true)]
    public class ArmorPartTemplateEditor : ItemTemplateEditor
    {
        // Armor Specific
        SerializedProperty m_TypeProperty;
        SerializedProperty m_SlotProperty;
        SerializedProperty m_StatsProperty;
        SerializedProperty m_AbilitiesProperty;

        protected override void OnEnable()
        {
            base.OnEnable();

            // Armor Specific
            m_TypeProperty = serializedObject.FindProperty("type");
            m_SlotProperty = serializedObject.FindProperty("slot");
            m_StatsProperty = serializedObject.FindProperty("stats");
            m_AbilitiesProperty = serializedObject.FindProperty("abilities");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            // Armor Specific
            EditorGUILayout.PropertyField(m_TypeProperty);
            EditorGUILayout.PropertyField(m_SlotProperty);
            B4CharacterSystemEditorGUILayout.ItemStatListPropertyField(m_StatsProperty);
            B4CharacterSystemEditorGUILayout.LevelTemplateListPropertyField(m_AbilitiesProperty, null, "Ability");

            serializedObject.ApplyModifiedProperties();
        }
    }
}