using B4Games.CharacterSystem.Configs.Templates;
using B4Games.CharacterSystem.Editor.Helpers;
using UnityEditor;
using UnityEngine;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(AbilityConfigTemplate), true)]
    public class AbilityConfigTemplateEditor : ConfigTemplateEditor
    {
        // Position Indicator
        SerializedProperty m_DirectionalIndicatorProperty;
        SerializedProperty m_DirectionalIndicatorColorProperty;
        SerializedProperty m_PositionalIndicatorProperty;
        SerializedProperty m_PositionalIndicatorInRangeColorProperty;
        SerializedProperty m_PositionalIndicatorOutOfRangeColorProperty;
        SerializedProperty m_IgnoreIndicatorCollisionLayerNameProperty;

        // Physics
        SerializedProperty m_IgnoreCollisionLayerNameProperty;

        // Disablers
        SerializedProperty m_AggressionAnimationProperty;
        SerializedProperty m_AggressionAnimationPositionProperty;
        SerializedProperty m_FearAnimationProperty;
        SerializedProperty m_FearAnimationPositionProperty;
        SerializedProperty m_ParalyzeAnimationProperty;
        SerializedProperty m_ParalyzeAnimationPositionProperty;
        SerializedProperty m_SleepAnimationProperty;
        SerializedProperty m_SleepAnimationPositionProperty;
        SerializedProperty m_SnareAnimationProperty;
        SerializedProperty m_SnareAnimationPositionProperty;
        SerializedProperty m_StunAnimationProperty;
        SerializedProperty m_StunAnimationPositionProperty;

        protected override void OnEnable()
        {
            base.OnEnable();

            // Position Indicator
            m_DirectionalIndicatorProperty = serializedObject.FindProperty("directionalIndicator");
            m_DirectionalIndicatorColorProperty = serializedObject.FindProperty("directionalIndicatorColor");
            m_PositionalIndicatorProperty = serializedObject.FindProperty("positionalIndicator");
            m_PositionalIndicatorInRangeColorProperty = serializedObject.FindProperty("positionalIndicatorInRangeColor");
            m_PositionalIndicatorOutOfRangeColorProperty = serializedObject.FindProperty("positionalIndicatorOutOfRangeColor");
            m_IgnoreIndicatorCollisionLayerNameProperty = serializedObject.FindProperty("ignoreIndicatorCollisionLayerName");

            // Physics
            m_IgnoreCollisionLayerNameProperty = serializedObject.FindProperty("ignoreCollisionLayerName");

            // Disablers
            m_AggressionAnimationProperty = serializedObject.FindProperty("aggressionAnimation");
            m_AggressionAnimationPositionProperty = serializedObject.FindProperty("aggressionAnimationPosition");
            m_FearAnimationProperty = serializedObject.FindProperty("fearAnimation");
            m_FearAnimationPositionProperty = serializedObject.FindProperty("fearAnimationPosition");
            m_ParalyzeAnimationProperty = serializedObject.FindProperty("paralyzeAnimation");
            m_ParalyzeAnimationPositionProperty = serializedObject.FindProperty("paralyzeAnimationPosition");
            m_SleepAnimationProperty = serializedObject.FindProperty("sleepAnimation");
            m_SleepAnimationPositionProperty = serializedObject.FindProperty("sleepAnimationPosition");
            m_SnareAnimationProperty = serializedObject.FindProperty("snareAnimation");
            m_SnareAnimationPositionProperty = serializedObject.FindProperty("snareAnimationPosition");
            m_StunAnimationProperty = serializedObject.FindProperty("stunAnimation");
            m_StunAnimationPositionProperty = serializedObject.FindProperty("stunAnimationPosition");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            // Positioning Indicators
            EditorGUILayout.PropertyField(m_DirectionalIndicatorProperty);

            EditorGUI.indentLevel++;
            {
                EditorGUILayout.PropertyField(m_DirectionalIndicatorColorProperty, new GUIContent("Indicator Color"));
            }
            EditorGUI.indentLevel--;

            EditorGUILayout.PropertyField(m_PositionalIndicatorProperty);

            EditorGUI.indentLevel++;
            {
                EditorGUILayout.PropertyField(m_PositionalIndicatorInRangeColorProperty, new GUIContent("In Range Color"));
                EditorGUILayout.PropertyField(m_PositionalIndicatorOutOfRangeColorProperty, new GUIContent("Out Of Range Color"));
            }
            EditorGUI.indentLevel--;

            EditorGUILayout.PropertyField(m_IgnoreIndicatorCollisionLayerNameProperty);

            B4CharacterSystemEditorGUILayout.NotEmptyHelpBox(m_IgnoreIndicatorCollisionLayerNameProperty);

            // Physics
            EditorGUILayout.PropertyField(m_IgnoreCollisionLayerNameProperty);

            B4CharacterSystemEditorGUILayout.NotEmptyHelpBox(m_IgnoreCollisionLayerNameProperty);

            // Disablers
            EditorGUILayout.PropertyField(m_AggressionAnimationProperty);
            EditorGUILayout.PropertyField(m_AggressionAnimationPositionProperty);
            EditorGUILayout.PropertyField(m_FearAnimationProperty);
            EditorGUILayout.PropertyField(m_FearAnimationPositionProperty);
            EditorGUILayout.PropertyField(m_ParalyzeAnimationProperty);
            EditorGUILayout.PropertyField(m_ParalyzeAnimationPositionProperty);
            EditorGUILayout.PropertyField(m_SleepAnimationProperty);
            EditorGUILayout.PropertyField(m_SleepAnimationPositionProperty);
            EditorGUILayout.PropertyField(m_SnareAnimationProperty);
            EditorGUILayout.PropertyField(m_SnareAnimationPositionProperty);
            EditorGUILayout.PropertyField(m_StunAnimationProperty);
            EditorGUILayout.PropertyField(m_StunAnimationPositionProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}