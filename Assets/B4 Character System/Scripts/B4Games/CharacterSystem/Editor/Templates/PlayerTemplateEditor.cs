using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(PlayerTemplate), true)]
    public class PlayerTemplateEditor : CharacterTemplateEditor
    {
        //SerializedProperty m_MaxLevelProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            //m_MaxLevelProperty = serializedObject.FindProperty("maxLevel");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            //EditorGUILayout.PropertyField(m_MaxLevelProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}