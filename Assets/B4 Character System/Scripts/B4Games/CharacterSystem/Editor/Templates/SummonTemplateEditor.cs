using B4Games.CharacterSystem.Models.Actors.Templates;
using UnityEditor;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(SummonTemplate), true)]
    public class SummonTemplateEditor : NpcTemplateEditor
    {
        //SerializedProperty m_MaxLevelProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            //m_MaxLevelProperty = serializedObject.FindProperty("maxLevel");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            //EditorGUILayout.PropertyField(m_MaxLevelProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}