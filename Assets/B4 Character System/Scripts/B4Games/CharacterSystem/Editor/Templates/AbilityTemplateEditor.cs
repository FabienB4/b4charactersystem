using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Abilities.Templates;
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(AbilityTemplate), true)]
    public class AbilityTemplateEditor : UnityEditor.Editor
    {
        // Display
        SerializedProperty m_AbilityNameProperty;
        SerializedProperty m_DescriptionProperty;
        SerializedProperty m_IconProperty;

        // Spawns
        SerializedProperty m_CastAnimationProperty;
        SerializedProperty m_TriggerProperty;

        // Behaviour
        SerializedProperty m_TypeProperty;
        SerializedProperty m_BehaviourProperty;
        SerializedProperty m_InstantBehaviourProperty;

        // Values
        SerializedProperty m_LevelsProperty;
        SerializedProperty m_HealthConsumptionProperty;
        SerializedProperty m_EnergyConsumptionProperty;
        SerializedProperty m_CastDurationProperty;
        SerializedProperty m_CastRangeProperty;
        SerializedProperty m_ReuseDurationProperty;
        SerializedProperty m_MaxTargetsProperty;
        SerializedProperty m_EffectTemplatesProperty;
        SerializedProperty m_ConditionsProperty;

        // Projectile Specific
        SerializedProperty m_ProjectileInitialPositionProperty;
        SerializedProperty m_ProjectileForceProperty;
        SerializedProperty m_ProjectileUpwardForceProperty;
        SerializedProperty m_ProjectileForceModeProperty;

        // Projectile Aoe Specific
        SerializedProperty m_ProjectileCountProperty;
        SerializedProperty m_ProjectileMaxAngleProperty;

        AnimBool m_ShowBehaviourPropsTransition = new AnimBool();
        AnimBool m_ShowActivePropsTransition = new AnimBool();

        protected virtual void OnEnable()
        {
            // Display
            m_AbilityNameProperty = serializedObject.FindProperty("abilityName");
            m_DescriptionProperty = serializedObject.FindProperty("description");
            m_IconProperty = serializedObject.FindProperty("icon");

            // Animations
            m_CastAnimationProperty = serializedObject.FindProperty("castAnimation");
            m_TriggerProperty = serializedObject.FindProperty("trigger");

            // Behaviour
            m_TypeProperty = serializedObject.FindProperty("type");
            m_BehaviourProperty = serializedObject.FindProperty("behaviour");
            m_InstantBehaviourProperty = serializedObject.FindProperty("instantBehaviour");

            // Values
            m_LevelsProperty = serializedObject.FindProperty("levels");
            m_HealthConsumptionProperty = serializedObject.FindProperty("healthConsumption");
            m_EnergyConsumptionProperty = serializedObject.FindProperty("energyConsumption");
            m_CastDurationProperty = serializedObject.FindProperty("castDuration");
            m_CastRangeProperty = serializedObject.FindProperty("castRange");
            m_ReuseDurationProperty = serializedObject.FindProperty("reuseDuration");
            m_MaxTargetsProperty = serializedObject.FindProperty("maxTargets");
            m_EffectTemplatesProperty = serializedObject.FindProperty("effectTemplates");
            m_ConditionsProperty = serializedObject.FindProperty("conditions");

            // Projectile Specific
            m_ProjectileInitialPositionProperty = serializedObject.FindProperty("projectileInitialPosition");
            m_ProjectileForceProperty = serializedObject.FindProperty("projectileForce");
            m_ProjectileUpwardForceProperty = serializedObject.FindProperty("projectileUpwardForce");
            m_ProjectileForceModeProperty = serializedObject.FindProperty("projectileForceMode");

            // Projectile Aoe Specific
            m_ProjectileCountProperty = serializedObject.FindProperty("projectileCount");
            m_ProjectileMaxAngleProperty = serializedObject.FindProperty("projectileMaxAngle");

            AbilityBehaviour behaviour = GetBehaviour(m_BehaviourProperty);
            m_ShowBehaviourPropsTransition.value = ShowBehaviourProps(behaviour);

            m_ShowBehaviourPropsTransition.valueChanged.AddListener(Repaint);

            AbilityType type = GetType(m_TypeProperty);
            m_ShowActivePropsTransition.value = ShowActiveProps(type);

            m_ShowActivePropsTransition.valueChanged.AddListener(Repaint);
        }

        protected virtual void OnDisable()
        {
            m_ShowBehaviourPropsTransition.valueChanged.RemoveListener(Repaint);
            m_ShowActivePropsTransition.valueChanged.RemoveListener(Repaint);
        }

        static AbilityType GetType(SerializedProperty abilityType)
        {
            return (AbilityType)abilityType.enumValueIndex;
        }

        static AbilityBehaviour GetBehaviour(SerializedProperty behaviour)
        {
            return (AbilityBehaviour)behaviour.enumValueIndex;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            AbilityType type = GetType(m_TypeProperty);
            AbilityBehaviour behaviour = GetBehaviour(m_BehaviourProperty);
            int levels = m_LevelsProperty.intValue;

            // Display
            EditorGUILayout.PropertyField(m_AbilityNameProperty);
            EditorGUILayout.PropertyField(m_DescriptionProperty);
            EditorGUILayout.PropertyField(m_IconProperty);

            if (type == AbilityType.Active)
            {
                // Animations
                B4CharacterSystemEditorGUILayout.CastAnimationPropertyField(m_CastAnimationProperty);
                B4CharacterSystemEditorGUILayout.TriggerPropertyField(m_TriggerProperty, levels);
            }

            // Behaviour
            EditorGUILayout.PropertyField(m_TypeProperty);
            EditorGUILayout.PropertyField(m_BehaviourProperty);

            if (type == AbilityType.Passive)
            {
                m_BehaviourProperty.enumValueIndex = (int)AbilityBehaviour.Self;
            }

            if (behaviour != AbilityBehaviour.Self)
            {
                EditorGUILayout.PropertyField(m_InstantBehaviourProperty);
            }

            m_ShowBehaviourPropsTransition.target = (!m_BehaviourProperty.hasMultipleDifferentValues && ShowBehaviourProps(behaviour));

            if (EditorGUILayout.BeginFadeGroup(m_ShowBehaviourPropsTransition.faded))
            {
                EditorGUI.indentLevel++;

                switch (behaviour)
                {
                    case AbilityBehaviour.Projectile:
                        // Projectile Specific
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ProjectileInitialPositionProperty, levels);
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ProjectileForceProperty, levels);
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ProjectileUpwardForceProperty, levels);
                        EditorGUILayout.PropertyField(m_ProjectileForceModeProperty);
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ProjectileCountProperty, levels);
                        B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ProjectileMaxAngleProperty, levels);
                        break;
                }

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndFadeGroup();

            // Values
            EditorGUILayout.PropertyField(m_LevelsProperty);

            // make sure at least one level is present for level dependent values
            B4CharacterSystemEditorGUILayout.EnsureMinIntValue(m_LevelsProperty, 1);

            m_ShowActivePropsTransition.target = (!m_TypeProperty.hasMultipleDifferentValues && ShowActiveProps(type));

            if (EditorGUILayout.BeginFadeGroup(m_ShowActivePropsTransition.faded))
            {
                B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_HealthConsumptionProperty, levels);
                B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_EnergyConsumptionProperty, levels);
                B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_CastDurationProperty, levels);

                if (behaviour == AbilityBehaviour.Positional)
                {
                    B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_CastRangeProperty, levels);
                }

                B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_ReuseDurationProperty, levels);
                EditorGUILayout.HelpBox(
                    "If one of the previous level related lists has a size that doesn't match the number of levels, the last value of the list is used for the remaining levels.",
                    MessageType.Info
                );

                if (behaviour != AbilityBehaviour.Self && behaviour != AbilityBehaviour.Directional)
                {
                    B4CharacterSystemEditorGUILayout.LevelListPropertyField(m_MaxTargetsProperty, levels);
                }
            }

            EditorGUILayout.EndFadeGroup();

            B4CharacterSystemEditorGUILayout.AbilityEffectTemplateListPropertyField(m_EffectTemplatesProperty, (AbilityTemplate)this.target);
            B4CharacterSystemEditorGUILayout.ConditionListPropertyField(m_ConditionsProperty);

            // ability can only use actor for condition target.
            for (int i = 0; i < m_ConditionsProperty.arraySize; i++)
            {
                SerializedProperty conditionProperty = m_ConditionsProperty.GetArrayElementAtIndex(i);

                SerializedProperty conditionTargetProperty = conditionProperty.FindPropertyRelative("target");
                conditionTargetProperty.enumValueIndex = (int)ConditionTarget.Actor;

                SerializedProperty conditionTypeProperty = conditionProperty.FindPropertyRelative("type");

                // can't use some condition types with ability
                switch ((ConditionType)conditionTypeProperty.enumValueIndex)
                {
                    case ConditionType.Distance:
                    case ConditionType.DistanceMax:
                    case ConditionType.DistanceMin:
                    case ConditionType.DistanceRange:
                    case ConditionType.LevelDifference:
                    case ConditionType.LevelDifferenceMin:
                    case ConditionType.LevelDifferenceMax:
                    case ConditionType.LevelDifferenceRange:
                    case ConditionType.Enemy:
                    case ConditionType.Friendly:
                    case ConditionType.Neutral:
                        conditionTypeProperty.enumValueIndex = 0;
                        break;
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private bool ShowActiveProps(AbilityType type)
        {
            return type == AbilityType.Active;
        }

        private bool ShowBehaviourProps(AbilityBehaviour behaviour)
        {
            return behaviour == AbilityBehaviour.Projectile || behaviour == AbilityBehaviour.Positional;
        }
    }
}