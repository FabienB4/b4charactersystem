using B4Games.CharacterSystem.Editor.Helpers;
using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace B4Games.CharacterSystem.Editor.Templates
{
    [CustomEditor(typeof(ConsumableTemplate), true)]
    public class ConsumableTemplateEditor : ItemTemplateEditor
    {
        // Usable Item Specific
        SerializedProperty m_ConsumableTypeProperty;
        SerializedProperty m_ReuseDurationProperty;

        // Regeneration Specific
        SerializedProperty m_RegenerationValueProperty;

        // Ability Specific
        SerializedProperty m_AbilityTemplateLevelProperty;

        // Effect Specific
        SerializedProperty m_EffectTemplateLevelProperty;

        AnimBool m_ShowTypePropsTransition = new AnimBool();

        protected override void OnEnable()
        {
            base.OnEnable();

            // Usable Item Specific
            m_ConsumableTypeProperty = serializedObject.FindProperty("consumableType");
            m_ReuseDurationProperty = serializedObject.FindProperty("reuseDuration");

            // Regeneration Specific
            m_RegenerationValueProperty = serializedObject.FindProperty("regenerationValue");

            // Ability Specific
            m_AbilityTemplateLevelProperty = serializedObject.FindProperty("abilityTemplateLevel");

            // Effect Specific
            m_EffectTemplateLevelProperty = serializedObject.FindProperty("effectTemplateLevel");

            ConsumableType type = GetType(m_ConsumableTypeProperty);
            m_ShowTypePropsTransition.value = ShowTypeProps(type);

            m_ShowTypePropsTransition.valueChanged.AddListener(Repaint);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            m_ShowTypePropsTransition.valueChanged.RemoveListener(Repaint);
        }

        static ConsumableType GetType(SerializedProperty type)
        {
            return (ConsumableType)type.enumValueIndex;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            // Usable Item Specific
            EditorGUILayout.PropertyField(m_ConsumableTypeProperty);

            ConsumableType type = GetType(m_ConsumableTypeProperty);

            m_ShowTypePropsTransition.target = (!m_ConsumableTypeProperty.hasMultipleDifferentValues && ShowTypeProps(type));

            if (EditorGUILayout.BeginFadeGroup(m_ShowTypePropsTransition.faded))
            {
                EditorGUI.indentLevel++;

                switch (type)
                {
                    case ConsumableType.RegenerateHealth:
                    case ConsumableType.RegenerateEnergy:
                        EditorGUILayout.PropertyField(m_RegenerationValueProperty);
                        break;
                    case ConsumableType.AbilityTemplate:
                        B4CharacterSystemEditorGUILayout.LevelTemplatePropertyField(m_AbilityTemplateLevelProperty);
                        break;
                    case ConsumableType.EffectTemplate:
                        B4CharacterSystemEditorGUILayout.LevelTemplatePropertyField(m_EffectTemplateLevelProperty);
                        break;
                }

                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndFadeGroup();

            EditorGUILayout.PropertyField(m_ReuseDurationProperty);

            serializedObject.ApplyModifiedProperties();
        }

        private bool ShowTypeProps(ConsumableType type)
        {
            return (type == ConsumableType.RegenerateHealth || type == ConsumableType.RegenerateEnergy ||
                    type == ConsumableType.AbilityTemplate || type == ConsumableType.EffectTemplate);
        }
    }
}