using UnityEngine;

namespace B4Games.UnityEditorExtended
{
    static public class MonoBehaviourExtended
    {
        /// <summary>
        /// Get or add a component. Usage example:
        /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
        /// </summary>
        static public T GetOrAddComponent<T>(this Component child) where T : Component
        {
            T result = child.GetComponent<T>();

            if (result == null)
            {
                result = child.gameObject.AddComponent<T>();
            }

            return result;
        }

        /// <summary>
        /// Get or add a component. Usage example:
        /// BoxCollider boxCollider = gameObject.GetOrAddComponent<BoxCollider>();
        /// </summary>
        static public T GetOrAddComponent<T>(this GameObject child) where T : Component
        {
            return child.transform.GetOrAddComponent<T>();
        }
    }
}