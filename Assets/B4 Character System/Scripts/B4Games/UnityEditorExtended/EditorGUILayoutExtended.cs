using UnityEditor;
using UnityEngine;

namespace B4Games.UnityEditorExtended
{
    public static class EditorGUILayoutExtended
    {
        /// <summary>
        /// Make a field for a list of SerializedProperty with add/remove buttons.
        /// </summary>
        /// <param name="property">The SerializedProperty to make a field for.</param>
        /// <param name="options">An optional list of layout options that specify extra layouting properties. Any values passed in here will override settings defined by the style. See Also: GUILayout.Width, GUILayout.Height, GUILayout.MinWidth, GUILayout.MaxWidth, GUILayout.MinHeight, GUILayout.MaxHeight, GUILayout.ExpandWidth, GUILayout.ExpandHeight.</param>
		public static void ListPropertyField(SerializedProperty property, params GUILayoutOption[] options)
        {
            EditorGUILayoutExtended.ListPropertyField(property, null, false, options);
        }

        /// <summary>
        /// Make a field for a list of SerializedProperty with add/remove buttons.
        /// </summary>
        /// <param name="property">The SerializedProperty to make a field for.</param>
        /// <param name="label">Optional label to use. If not specified the label of the property itself is used. Use GUIContent.none to not display a label at all.</param>
        /// <param name="options">An optional list of layout options that specify extra layouting properties. Any values passed in here will override settings defined by the style. See Also: GUILayout.Width, GUILayout.Height, GUILayout.MinWidth, GUILayout.MaxWidth, GUILayout.MinHeight, GUILayout.MaxHeight, GUILayout.ExpandWidth, GUILayout.ExpandHeight.</param>
		public static void ListPropertyField(SerializedProperty property, GUIContent label, params GUILayoutOption[] options)
        {
            EditorGUILayoutExtended.ListPropertyField(property, label, false, options);
        }

        /// <summary>
        /// Make a field for a list of SerializedProperty with add/remove buttons.
        /// </summary>
        /// <param name="property">The SerializedProperty to make a field for.</param>
        /// <param name="includeChildren">If true the property including children is drawn; otherwise only the control itself (such as only a foldout but nothing below it).</param>
        /// <param name="options">An optional list of layout options that specify extra layouting properties. Any values passed in here will override settings defined by the style. See Also: GUILayout.Width, GUILayout.Height, GUILayout.MinWidth, GUILayout.MaxWidth, GUILayout.MinHeight, GUILayout.MaxHeight, GUILayout.ExpandWidth, GUILayout.ExpandHeight.</param>
		public static void ListPropertyField(SerializedProperty property, bool includeChildren, params GUILayoutOption[] options)
        {
            EditorGUILayoutExtended.ListPropertyField(property, null, includeChildren, options);
        }

        /// <summary>
        /// Make a field for a list of SerializedProperty with add/remove buttons.
        /// </summary>
        /// <param name="property">The SerializedProperty to make a field for.</param>
        /// <param name="label">Optional label to use. If not specified the label of the property itself is used. Use GUIContent.none to not display a label at all.</param>
        /// <param name="includeChildren">If true the property including children is drawn; otherwise only the control itself (such as only a foldout but nothing below it).</param>
        /// <param name="options">An optional list of layout options that specify extra layouting properties. Any values passed in here will override settings defined by the style. See Also: GUILayout.Width, GUILayout.Height, GUILayout.MinWidth, GUILayout.MaxWidth, GUILayout.MinHeight, GUILayout.MaxHeight, GUILayout.ExpandWidth, GUILayout.ExpandHeight.</param>
        public static void ListPropertyField(SerializedProperty property, GUIContent label, bool includeChildren, params GUILayoutOption[] options)
        {
            EditorGUILayout.PropertyField(property, label, includeChildren, options);

            if (property.isExpanded)
            {
                EditorGUI.indentLevel++;

                // List controls
                GUILayout.BeginHorizontal();
                // Align buttons to the right
                GUILayout.FlexibleSpace();

                if (GUILayout.Button("+", GUILayout.Width(20), GUILayout.Height(20)))
                {
                    property.arraySize++;
                }

                if (GUILayout.Button("-", GUILayout.Width(20), GUILayout.Height(20)))
                {
                    property.arraySize--;
                }

                GUILayout.EndHorizontal();

                EditorGUI.indentLevel--;
            }
        }
    }
}