# Boilerplate for a Character & Combat System

Quickly implement your character system with abilities, items... and be ready for combat in no time!

B4 Character System removes the burden of creating all the "standard" scripts for your characters.
Instead you can concentrate on the specific features you want for your characters.
Stats, Abilities, Weapons, Armors, Consumables and much more are supported out of the box.

* Works for both RPG and FPS type games.
* Built on ScriptableObjects (templates) to allow easy editing even during play mode, and better version control support!
* Multiplayer-Ready (requires you to implement the necessary features according to your infrastructure).
* Extensive customization.
* Zero-To-Minimal coding requirements if you don't require more specific features.
* Fully documented code.
* Extensive Wiki to help you start out.
* Issue tracker to quickly identify and crush the bugs!

See it in action: [https://www.youtube.com/watch?v=ncSvr6q_nxU](https://www.youtube.com/watch?v=ncSvr6q_nxU)


# Built-In Features

* 9 Stats
* 5 Armor parts (Head, Torso, Hands, Legs, Feet)
* 2 Weapons (primary, secondary)
* 4 Ability behaviours (Self, Directional, Positional, Projectile)
* 2 Ability types (Active, Passive)
* 41 Conditions for Abilities, Effects, Equippables and Consumables (using almost every aspects of B4 Character System)
* 28 Effects (Damage, Heal, Teleport, Summon, Buff, Cleanse, Resist, Resurrect and more)
* 4 Consumable types (Health regen, Energy regen, Ability, Effect)
* Unlimited Character templates possibilities (Player/NPC/Summon)
* Unlimited Item templates possibilities (Weapon/Armor Part/Consumable)
* Unlimited Ability templates possibilities
* Unlimited Effect templates possibilities
* Player/NPC/Summon support (with basic behaviour for NPC/Summon)
* Character movements (based on Ethan), casting, and status.
* Attackable, respawnable NPCs
* Ability casting (with animation support)
* Ability positioning (Guild Wars 2 like)
* Item Container support (Equipment/Inventory/Storage, just link them to your GUI and make your changes)
* Storable, destroyable, stackable or NPC-restricted items
* Can attach stats and abilities to Equippables
* Set, add, subtract, divide, multiply Stat Modifiers (with order support)
* GameManager & SpawnManager to handle automatic on-start spawns and respawns
* Extensive configuration (Characters, Abilities, Items and more)
* Notification system (just link it to your GUI)

# Samples

To enable the use in a project without the Samples, some of the code required for the Samples has been commented out in the asset's scripts.

Please check the following files if you want to try the Samples fully:

* B4 Character System\Scripts\B4Games\CharacterSystem\Models\ItemContainers\Inventory.cs
* B4 Character System\Scripts\B4Games\CharacterSystem\Models\ItemContainers\Storage.cs

You can search for the string `/* UNCOMMENT TO TEST SAMPLES` to find the relevant parts quickly.

Please note the Samples use the following Standard Assets and will not work properly without these:

* Cameras
* Characters
* CrossPlatformInput
* PhysicsMaterials
* Utility

# Setup

Start by adding the package to your project.
Once the package is compiled, you should have access to a new sub-menu in the "Asset/Create" menu named "B4 Character System". This menu contains all the templates you can create for your game.

Templates are an essential part of B4 Character System. They are the base of every objects in your game that require values to be stored. Many games use XML, or JSON for this purpose, but Unity's ScriptableObjects are perfect for this. The ScriptableObject values are not changing (should not change!) at runtime, they are just used to instantiate your objects, they are "base values". Think of it as databases of static values that you set before releasing/updating your game.
You can create as many templates as you need. For example, with Player Templates, you could create a template for Elves, one for Orcs, and one for Humans, or one for a foot soldier, one for a demolition expert, and one for a sniper. The template will then affect the base values of the player's stats, his starting abilities, weapons, armors, and more.

Available templates:

* Configs
    + Ability
    + Character
    + Database
    + Item
    + Player
* Characters
    + Npc
    + Player
    + Summon
* Items
    + Armor Part
    + Consumable
    + Item
    + Weapon
* Ability
* Effect
* Notification

You can start creating your templates, and link them appropriately, depending on your needs.
If required, you can easily subclass these templates and their respective classes or modify the existing ones to create custom behaviours.
The Samples use an inspector value to assign a template to the player. This implementation is entirely up to you, but the player template has to be assigned before the player is instantiated. _A typical implementation would be a class selection screen that then takes the player to the character creation screen._

# Conditions

Conditions are the base of every game, and B4 Character System is using them for almost everything. They are used to restrict certain behaviours, like equipping a weapon, using an ability, or applying an effect.
Right out of the box, there are **41 conditions** that can be used, and by adding multiple conditions, you can create an infinite number of combinations.
For example, you could have a weapon that requires the player to be under level 5, an ability that requires the caster to be wearing a specific armor, or an effect that requires the caster to be at a certain distance to be applied to the victims.

# Stats

Stats are an essential part of every games. B4 Character System comes with **9 stats** built-in. These can easily be removed or modified, and more can also be added.
The process of adding stats is pretty straightforward and you don't have to worry about creating a behaviour for each, B4 Character System is smart enough to handle them on its own.
The easiest way to remove, modify or add a stat is to search for an existing one in your project (for example `Stat.Attack`) and simply remove, modify or add the desired stat wherever the existing one appears (you should be looking for 6 occurrences at the moment - including one in the Samples). _Note: When searching for occurrences, avoid searching for `Stat.MaxHealth` or `Stat.MaxEnergy` as these have specific behaviours with deeper ties._

# Notifications

Notifications are used to notify players when something happens. Like every parts of B4 Character System, you have **full control over how they will be displayed**, B4 Character System is only here to provide the "behind the scene" behaviour.
B4 Character System comes with a bunch of Notifications already, that are used for internal behaviours and conditions. You can easily re-use these and add more whenever you need.


# Tips

* (Ability|Effect) Create level 1 and test it, then create level 2, and test it, then create level 3, and test it, and so on... This make debugging much easier. It will also copy values from the previous level when you add new list items, making modifying it easier.
* (Effect) Max Targets value allows for multiple configurations, and can also dictate the life of the prefab.

# Remember, Remember

* (Config) Remember to specify where you are storing your resources, if you are not using the default folders.
* (Config) Remember to specify and define the different layers used by B4 Character System.
* (Input) Remember to define the different inputs used by B4 Character System (See `PlayerController.cs`).


[Click here for the online version of this documentation](https://bitbucket.org/FabienB4/b4charactersystem/wiki)