using B4Games.CharacterSystem.Enums;
using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Effects;
using B4Games.CharacterSystem.Models.Items;
using UnityEngine;
using UnityEngine.UI;

namespace B4CharacterSystem.Samples
{
    /// <summary>
    /// Test UI displaying information about a Character
    /// ABSOLUTELY NOT OPTIMIZED, SHOULD NOT BE USED IN PRODUCTION
    /// </summary>
    public class CombatUI : MonoBehaviour
    {
        public Character character;

        [Header("Stats")]
        public Text health;
        public Text maxHealth;
        public Text energy;
        public Text maxEnergy;
        public Text healthRegeneration;
        public Text energyRegeneration;
        public Text attack;
        public Text defense;
        public Text speed;
        public Text healPower;
        public Text regenerationPower;

        [Header("Equipment")]
        public Button primaryWeaponButton;
        public Button secondaryWeaponButton;
        public Button headArmorPartButton;
        public Button torsoArmorPartButton;
        public Button handsArmorPartButton;
        public Button legsArmorPartButton;
        public Button feetArmorPartButton;

        [Header("Inventory")]
        public RectTransform inventoryPanel;

        [Header("Effects")]
        public RectTransform effectsPanel;

        [Header("Abilities")]
        public RectTransform abilityBarPanel;


        private Button[] m_AbilityBarButtons;
        private int latestEffectCount = -1;

        void Start()
        {
            if (character == null)
            {
                this.gameObject.SetActive(false);
            }

            if (abilityBarPanel != null)
            {
                m_AbilityBarButtons = abilityBarPanel.GetComponentsInChildren<Button>();
            }

            if (primaryWeaponButton != null)
            {
                primaryWeaponButton.onClick.AddListener(delegate ()
                {
                    Equippable primaryWeaponItem = character.equipment.GetCurrentWeapon(WeaponSlot.Primary);

                    if (primaryWeaponItem != null)
                    {
                        primaryWeaponItem.UnEquip(character);
                    }
                });
            }

            if (secondaryWeaponButton != null)
            {
                secondaryWeaponButton.onClick.AddListener(delegate ()
                {
                    Equippable secondaryWeaponItem = character.equipment.GetCurrentWeapon(WeaponSlot.Secondary);

                    if (secondaryWeaponItem != null)
                    {
                        secondaryWeaponItem.UnEquip(character);
                    }
                });
            }

            if (headArmorPartButton != null)
            {
                headArmorPartButton.onClick.AddListener(delegate ()
                {
                    Equippable headArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Head);

                    if (headArmorPartItem != null)
                    {
                        headArmorPartItem.UnEquip(character);
                    }
                });
            }

            if (torsoArmorPartButton != null)
            {
                torsoArmorPartButton.onClick.AddListener(delegate ()
                {
                    Equippable torsoArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Torso);

                    if (torsoArmorPartItem != null)
                    {
                        torsoArmorPartItem.UnEquip(character);
                    }
                });
            }

            if (handsArmorPartButton != null)
            {
                handsArmorPartButton.onClick.AddListener(delegate ()
                {
                    Equippable handsArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Hands);

                    if (handsArmorPartItem != null)
                    {
                        handsArmorPartItem.UnEquip(character);
                    }
                });
            }

            if (legsArmorPartButton != null)
            {
                legsArmorPartButton.onClick.AddListener(delegate ()
                {
                    Equippable legsArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Legs);

                    if (legsArmorPartItem != null)
                    {
                        legsArmorPartItem.UnEquip(character);
                    }
                });
            }

            if (feetArmorPartButton != null)
            {
                feetArmorPartButton.onClick.AddListener(delegate ()
                {
                    Equippable feetArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Feet);

                    if (feetArmorPartItem != null)
                    {
                        feetArmorPartItem.UnEquip(character);
                    }
                });
            }
        }

        void Update()
        {
            if (health != null)
            {
                health.text = character.health.ToString();
            }

            if (maxHealth != null)
            {
                maxHealth.text = character.GetStat(Stat.MaxHealth).ToString();
            }

            if (energy != null)
            {
                energy.text = character.energy.ToString();
            }

            if (maxEnergy != null)
            {
                maxEnergy.text = character.GetStat(Stat.MaxEnergy).ToString();
            }

            if (healthRegeneration != null)
            {
                healthRegeneration.text = character.GetStat(Stat.HealthRegeneration).ToString();
            }

            if (energyRegeneration != null)
            {
                energyRegeneration.text = character.GetStat(Stat.EnergyRegeneration).ToString();
            }

            if (attack != null)
            {
                attack.text = character.GetStat(Stat.Attack).ToString();
            }

            if (defense != null)
            {
                defense.text = character.GetStat(Stat.Defense).ToString();
            }

            if (speed != null)
            {
                speed.text = character.GetStat(Stat.Speed).ToString();
            }

            if (healPower != null)
            {
                healPower.text = character.GetStat(Stat.HealPower).ToString();
            }

            if (regenerationPower != null)
            {
                regenerationPower.text = character.GetStat(Stat.RegenerationPower).ToString();
            }

            if (primaryWeaponButton != null)
            {
                Equippable primaryWeaponItem = character.equipment.GetCurrentWeapon(WeaponSlot.Primary);

                if (primaryWeaponItem != null)
                {
                    primaryWeaponButton.GetComponentInChildren<Text>().text = primaryWeaponItem.template.itemName + DisplayEquippableStats(primaryWeaponItem);
                }
                else
                {
                    primaryWeaponButton.GetComponentInChildren<Text>().text = "Primary Weapon";
                }
            }

            if (secondaryWeaponButton != null)
            {
                Equippable secondaryWeaponItem = character.equipment.GetCurrentWeapon(WeaponSlot.Secondary);

                if (secondaryWeaponItem != null)
                {
                    secondaryWeaponButton.GetComponentInChildren<Text>().text = secondaryWeaponItem.template.itemName + DisplayEquippableStats(secondaryWeaponItem);
                }
                else
                {
                    secondaryWeaponButton.GetComponentInChildren<Text>().text = "Secondary Weapon";
                }
            }

            if (headArmorPartButton != null)
            {
                Equippable headArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Head);

                if (headArmorPartItem != null)
                {
                    headArmorPartButton.GetComponentInChildren<Text>().text = headArmorPartItem.template.itemName + DisplayEquippableStats(headArmorPartItem);
                }
                else
                {
                    headArmorPartButton.GetComponentInChildren<Text>().text = "Head Armor Part";
                }
            }

            if (torsoArmorPartButton != null)
            {
                Equippable torsoArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Torso);

                if (torsoArmorPartItem != null)
                {
                    torsoArmorPartButton.GetComponentInChildren<Text>().text = torsoArmorPartItem.template.itemName + DisplayEquippableStats(torsoArmorPartItem);
                }
                else
                {
                    torsoArmorPartButton.GetComponentInChildren<Text>().text = "Torso Armor Part";
                }
            }

            if (handsArmorPartButton != null)
            {
                Equippable handsArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Hands);

                if (handsArmorPartItem != null)
                {
                    handsArmorPartButton.GetComponentInChildren<Text>().text = handsArmorPartItem.template.itemName + DisplayEquippableStats(handsArmorPartItem);
                }
                else
                {
                    handsArmorPartButton.GetComponentInChildren<Text>().text = "Hands Armor Part";
                }
            }

            if (legsArmorPartButton != null)
            {
                Equippable legsArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Legs);

                if (legsArmorPartItem != null)
                {
                    legsArmorPartButton.GetComponentInChildren<Text>().text = legsArmorPartItem.template.itemName + DisplayEquippableStats(legsArmorPartItem);
                }
                else
                {
                    legsArmorPartButton.GetComponentInChildren<Text>().text = "Legs Armor Part";
                }
            }

            if (feetArmorPartButton != null)
            {
                Equippable feetArmorPartItem = character.equipment.GetCurrentArmorPart(ArmorPartSlot.Feet);

                if (feetArmorPartItem != null)
                {
                    feetArmorPartButton.GetComponentInChildren<Text>().text = feetArmorPartItem.template.itemName + DisplayEquippableStats(feetArmorPartItem);
                }
                else
                {
                    feetArmorPartButton.GetComponentInChildren<Text>().text = "Feet Armor Part";
                }
            }

            if (m_AbilityBarButtons != null && m_AbilityBarButtons.Length > 0)
            {
                for (int i = 0; i < m_AbilityBarButtons.Length; i++)
                {
                    Button button = m_AbilityBarButtons[i];

                    button.GetComponentInChildren<Text>().text = TrySetAbilityBarSlot(i);
                }
            }

            if (latestEffectCount != character.effects.Count)
            {
                foreach (Transform child in effectsPanel)
                {
                    if (child.gameObject.name != "Header")
                    {
                        GameObject.Destroy(child.gameObject);
                    }
                }

                for (int i = 0; i < character.effects.Count; i++)
                {
                    AddEffectToEffectsPanel(character.effects[i]);
                }

                latestEffectCount = character.effects.Count;
            }
        }

        public void AddItemToInventory(Item item)
        {
            if (inventoryPanel == null)
            {
                return;
            }

            Button itemButton = CreateButton("Item " + item.template.itemName, item.template.itemName, inventoryPanel);

            itemButton.onClick.RemoveAllListeners();

            if (item is Equippable)
            {
                itemButton.onClick.AddListener(delegate ()
                {
                    Equippable equippableItem = item.GetEquippable();

                    if (equippableItem != null)
                    {
                        equippableItem.Equip(character);
                    }
                });
            }
            else if (item is Consumable)
            {
                itemButton.onClick.AddListener(delegate ()
                {
                    Consumable consumableItem = item.GetConsumable();

                    if (consumableItem != null)
                    {
                        consumableItem.Use();
                    }
                });
            }
        }

        public void RemoveItemFromInventory(Item item)
        {
            if (inventoryPanel == null)
            {
                return;
            }

            Transform transform = inventoryPanel.FindChild("Item " + item.template.itemName);

            if (transform == null)
            {
                return;
            }

            Destroy(transform.gameObject);
        }

        private string TrySetAbilityBarSlot(int index)
        {
            if (!(character is Player))
            {
                return null;
            }

            Player player = (Player)character;

            if (player.controller == null || !player.controller.HasAbilityInSlot(index))
            {
                return "Empty";
            }

            return player.controller.abilityBar[index].template.abilityName;
        }

        private void AddEffectToEffectsPanel(Effect effect)
        {
            string effectName = effect.template.effectName;
            Text text = new GameObject("Effect " + effectName).AddComponent<Text>();
            text.text = effectName + " (Level:" + effect.level + ", Power:" + effect.power + ", Duration:" + effect.duration + ")";
            text.alignment = TextAnchor.UpperCenter;
            text.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            text.color = Color.black;

            text.transform.SetParent(effectsPanel, false);
        }

        private string DisplayEquippableStats(Equippable item)
        {
            string stats = " \n";

            foreach (var stat in item.template.stats)
            {
                if (stats != " \n")
                {
                    stats += "\n";
                }

                stats += "- " + stat.stat + " " + stat.modifierType + " " + stat.power + " " + stat.order;
            }

            return stats;
        }

        private Button CreateButton(string name, string text, Transform parent)
        {
            GameObject itemGo = new GameObject(name);
            Image itemImage = itemGo.AddComponent<Image>();
            Button itemButton = itemGo.AddComponent<Button>();
            itemButton.targetGraphic = itemImage;
            itemButton.transform.SetParent(parent, false);

            Text itemButtonText = new GameObject("Text").AddComponent<Text>();
            itemButtonText.transform.SetParent(itemButton.transform, false);
            itemButtonText.alignment = TextAnchor.MiddleCenter;
            itemButtonText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            itemButtonText.color = Color.black;

            itemButtonText.text = text;

            return itemButton;
        }
    }
}