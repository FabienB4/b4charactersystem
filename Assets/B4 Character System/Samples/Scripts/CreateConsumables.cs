using B4Games.CharacterSystem.Models.Actors;
using B4Games.CharacterSystem.Models.Items;
using B4Games.CharacterSystem.Models.Items.Templates;
using UnityEngine;

namespace B4CharacterSystem.Samples
{
    public class CreateConsumables : MonoBehaviour
    {
        public string key = "`";
        public Character character;
        public ConsumableTemplate consumableTemplate;
        public int consumableCount = 1;

        void Update()
        {
            if (Input.GetKeyDown(key))
            {
                if (consumableCount > 0)
                {
                    Item.Create(consumableTemplate, consumableCount, character);
                }
            }
        }
    }
}